﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Velocimetro : MonoBehaviour 
{
	void Update () 
	{
		if(Kinect.shotDone == true)
		{
			if(Kinect.pForce.z > 0 && !enterTimer)//velocimeter.text = "vel: " + (System.Math.Round(Kinect.pForce.z / 122,2)).ToString () + "m/s";
			{
				enterTimer = true;

				floatVelocity = (float)System.Math.Round(11 / AnimatorController2.time);
				floatVelocity = floatVelocity * (0.00062f / 0.00028f); //= 2.2143
				intVelocity = (int)Mathf.Abs( floatVelocity);

                int power = (int)(intVelocity * 100 / MAX_VELOCITY);

				if(intVelocity > MAX_VELOCITY)
				{
					intVelocity = MAX_VELOCITY;
				}
                powerText.text = power + "";
                velocityText.text = intVelocity + "";

                // Calculate degress in MPH
                /*int degress = (intVelocity * NEEDLE_MAX_DEGREE) / MAX_VELOCITY;
				RotateNeedle(degress);
				StartCoroutine(TimerPlus(degress));*/
			}
		}
	}

	/*public void RotateNeedle(float degrees)
	{
		if(speedNeedle != null)
		{
			Debug.Log("Angle= " + degrees * -1);
			iTween.RotateAdd (speedNeedle, iTween.Hash ("z", degrees * -1.0f, "time", 1.0f, "delay", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeOutSine));
		}
	}

	public IEnumerator TimerPlus(float degreess)
	{
		yield return new WaitForSeconds(2.5f);

		if(speedNeedle != null)
		{
			iTween.RotateAdd (speedNeedle, iTween.Hash ("z", degreess, "time", 1.0f, "delay", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeOutSine));
		}

		yield return null;
	}*/

	public static int intVelocity;

    //public GameObject speedNeedle;
    public Text velocityText;
	private float floatVelocity;
	
	private bool enterTimer = false;

    public Text powerText;

	private static int NEEDLE_MAX_DEGREE = 270;
	private static int MAX_VELOCITY = 60;
}
