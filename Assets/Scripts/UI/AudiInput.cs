﻿using UnityEngine;
using System.Collections;

public class AudiInput : MonoBehaviour
{
	void Start ()
	{
		for(int i = 0; i < textInGroup.Length; i++)
		{
			textInGroup[i].fooArgs = new object[] { (object) textInGroup[i] };
			textInGroup[i].fooEvent = UpdateGroup;
		}
		
		current = null;

		mCurrentField = 0;
		mCountButton = buttonsGroup.Length;

		textInGroup[mCurrentField].OnMouseUp();
		textInGroup[mCurrentField].OnMouseUpAsButton();
	}

	void Update()
	{
		/*if(Input.GetKeyDown(KeyCode.Tab))
		{
			int prevField = mCurrentField;
			mCurrentField++;
			if(mCurrentField >= textInGroup.Length)
			{
				int total = textInGroup.Length + buttonsGroup.Length;
				if(mCurrentField < total)
				{
					if(prevField < textInGroup.Length)
					{
						textInGroup[prevField].ResetToNormal();
						current = null;
					}

					mCurrentButton = total - textInGroup.Length - mCountButton;
					mCountButton--;

					if(mCurrentButton > 0)
					{
						buttonsGroup[mCurrentButton - 1].Unhighlight();
					}

					buttonsGroup[mCurrentButton].Highlight();
				}
				else
				{
					buttonsGroup[mCurrentButton].Unhighlight();

					mCurrentField = 0;
					mCountButton = buttonsGroup.Length;
				}
			}

			if(mCurrentField < textInGroup.Length)
			{
				if(prevField < textInGroup.Length)
				{
					textInGroup[prevField].ResetToNormal();
				}

				current = null;
				
				textInGroup[mCurrentField].OnMouseUp();
				textInGroup[mCurrentField].OnMouseUpAsButton();
			}
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			int total = textInGroup.Length + buttonsGroup.Length;
			if(mCurrentField < total && mCurrentField >= textInGroup.Length)
			{
				buttonsGroup[mCurrentButton].Next();
			}
		}*/

		if(Input.GetKeyDown(KeyCode.KeypadEnter))
		{
			if(buttonSend != null)
			{
				buttonSend.OnMouseUpAsButton();
			}
			else
			{
				Debug.Log("Warning: missing button send reference.");
			}
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(buttonRestart != null)
			{
				buttonRestart.OnMouseUpAsButton();
			}
			else
			{
				Debug.Log("Warning: missing button restart reference.");
			}
		}
	}

	void OnMouseUpAsButton()
	{
		if(current != null)
		{
			current.ResetToNormal();
			current = null;
		}

		/*for(int i = 0; i < buttonsGroup.Length; i++)
		{
			buttonsGroup[i].Unhighlight();
		}*/
	}
	
	public UIText Current
	{
		set { current = value; }
		get { return current; }
	}
	
	public void UpdateGroup(object[] args)
	{
		UIText next = (UIText) args[0];
		if(next != current)
		{
			for(int i = 0; i < textInGroup.Length; i++)
			{
				if(next == textInGroup[i])
				{
					mCurrentField = i;
					current = textInGroup[i];
				}
				else
				{
					textInGroup[i].ResetToNormal();
				}
			}
		}

		/*mCountButton = buttonsGroup.Length;
		for(int i = 0; i < buttonsGroup.Length; i++)
		{
			buttonsGroup[i].Unhighlight();
		}*/
	}
	
	public UIText[] textInGroup;

	public UIButtonGroup[] buttonsGroup;

	public UIButton buttonSend;
	public UIButton buttonRestart;
	
	private UIText current;

	private int mCurrentField;
	private int mCurrentButton;
	private int mCountButton;
}
