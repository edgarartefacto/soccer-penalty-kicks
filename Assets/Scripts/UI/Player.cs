﻿using UnityEngine;
using System.Collections;

public class Player
{
	public Player()
	{
		mEmail = "";
		mName = "";
		mZip = "";
		
		mAgeRange = 0;
		mIsMarket = 0;
		mIsOwned = 0;
		mIsMore = 0;
	}

	public string Email {
		get {
			return this.mEmail;
		}
		set {
			mEmail = value;
		}
	}

	public string Name {
		get {
			return this.mName;
		}
		set {
			mName = value;
		}
	}

	public string LastName
	{
		get {
			return this.mLastName;
		}
		set {
			mLastName = value;
		}
	}

	public string Zip {
		get {
			return this.mZip;
		}
		set {
			mZip = value;
		}
	}

	public int AgeRange {
		get {
			return this.mAgeRange;
		}
		set {
			mAgeRange = value;
		}
	}

	public int IsMarket {
		get {
			return this.mIsMarket;
		}
		set {
			mIsMarket = value;
		}
	}

	public int IsOwned {
		get {
			return this.mIsOwned;
		}
		set {
			mIsOwned = value;
		}
	}

	public int IsMore {
		get {
			return this.mIsMore;
		}
		set {
			mIsMore = value;
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Player: mEmail={0}, mName={1}, mLastName={2}, mZip={3}, mAgeRange={4}, mIsMarket={5}, mIsOwned={6}, mIsMore={7}]", mEmail, mName, mLastName, mZip, mAgeRange, mIsMarket, mIsOwned, mIsMore);
	}

	private string mEmail;
	private string mName;
	private string mLastName;
	private string mZip;
	
	private int mAgeRange;
	private int mIsMarket;
	private int mIsOwned;
	private int mIsMore;
}
