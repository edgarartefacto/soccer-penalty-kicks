﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour 
{
	public static int bonus = 1;
	public static bool textBonusChanged = false;

	public GameObject powerNeedle;

	public TMPro.TextMeshPro bonusmeter;
	public TMPro.TextMeshPro extraMeter;

	private static int currentDegree;
	private static int prevBonus = 0;

	void Start()
	{
		if(AnimatorController2.nTiro > 1)
		{
			powerNeedle.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -67.0f * bonus); // 270 / 4 =~ 67.5
		}
		else if(AnimatorController2.nTiro == 1)
		{
			currentDegree = 0;
			prevBonus = 0;
		}

		extraMeter.GetComponent<TextAnimation>().Reset();
		extraMeter.gameObject.transform.localScale = Vector3.one;
		extraMeter.text = "x" + bonus.ToString();
		bonusmeter.text = "x" + bonus.ToString();

	}

	void Update () 
	{
		if(bonus != prevBonus)
		{
			if(bonus >= 1 && bonus <=5)
			{
				prevBonus = bonus;
				if(bonus == 1)
				{
					iTween.RotateAdd (powerNeedle, iTween.Hash ("z", currentDegree, "time", 1.0f, "delay", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeOutSine));
					currentDegree = 0;

					if(prevTiro != 3 && !Timer.sceneTraining)
					{
						StartCoroutine(TimerNeedle());
					}
					else if(Timer.sceneTraining)
					{
						StartCoroutine(TimerNeedle());
					}
				}
				else
				{
					currentDegree += 67;
					iTween.RotateAdd (powerNeedle, iTween.Hash ("z", -67.0f, "time", 1.0f, "delay", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeOutSine));
				}

				prevTiro = AnimatorController2.nTiro;
			}
		}

		if(textBonusChanged)
		{
			textBonusChanged = false;
			bonusmeter.text = "x" + bonus.ToString();
			extraMeter.text = "x" + bonus.ToString();
			iTween.ScaleTo(extraMeter.gameObject, iTween.Hash("scale", new Vector3(1.5f, 1.5f, 1.5f), "time", 1.0f));
			iTween.ValueTo (extraMeter.gameObject, iTween.Hash ("from", 1.0f, "to", 0.0f, "time", 1.0f,
			                                                    "onupdate", "ChangeAlpha", "easetype", iTween.EaseType.easeOutSine));
		}
	}

	public IEnumerator TimerNeedle()
	{
		yield return new WaitForSeconds(1.5f);
		
		if(powerNeedle != null && AnimatorController2.nTiro == 1)
		{
			iTween.RotateAdd (powerNeedle, iTween.Hash ("z", -67.0f, "time", 1.0f, "delay", 0.2f, "islocal", true, "easetype", iTween.EaseType.easeOutSine));
		}
		
		yield return null;
	}

	private static int prevTiro;
}
