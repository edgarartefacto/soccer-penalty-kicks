﻿using UnityEngine;
using System.Collections;
using TMPro;

public delegate void FooEvent(object[] args);

public class UIButton : MonoBehaviour
{
	void Awake()
	{
		currenStates = new bool[3];
	}

	void Start () {}
	
	void Update () {}

	public void OnMouseDown()
	{
		currenStates[(int) State.NORMAL] = false;
		currenStates[(int) State.OVER] = true;
		currenStates[(int) State.ACTIVE] = false;
		UpdateState();
	}

	public void OnMouseUp()
	{
		currenStates[(int) State.OVER] = false;
		if(isActive)
		{
			currenStates[(int) State.NORMAL] = false;
			currenStates[(int) State.ACTIVE] = true;
		}
		else
		{
			currenStates[(int) State.NORMAL] = true;
			currenStates[(int) State.ACTIVE] = false;
		}
		
		UpdateState();

		if(isActive)
		{// For RadioButtons behaviour
			if(fooEvent != null)
			{
				fooEvent(fooArgs);
			}
		}
	}

	public void OnMouseUpAsButton()
	{
		if(!isActive)
		{// For RadioButtons behaviour
			if(fooEvent != null)
			{
				fooEvent(fooArgs);
			}
		}
	}

	public bool IsEnabled
	{
		get { return isEnabled; }
		set {
			isEnabled = value;
			UpdateCollider();
		}
	}

	public void EnableActiveState()
	{
		currenStates[(int) State.NORMAL] = false;
		currenStates[(int) State.OVER] = false;
		currenStates[(int) State.ACTIVE] = true;
		UpdateState();
	}

	public void Reset()
	{
		currenStates[(int) State.NORMAL] = true;
		currenStates[(int) State.OVER] = false;
		currenStates[(int) State.ACTIVE] = false;
		UpdateState();
	}

	public void UpdateState()
	{
		if(normalState != null)
		{
			normalState.SetActive(currenStates[(int) State.NORMAL]);
		}
		
		if(overState != null)
		{
			overState.SetActive(currenStates[(int) State.OVER]);
		}
		
		if(activeState != null)
		{
			activeState.SetActive(currenStates[(int) State.ACTIVE]);
		}
	}

	private void UpdateCollider()
	{
		gameObject.collider2D.enabled = isEnabled;
	}

	enum State
	{
		NORMAL,
		OVER,
		ACTIVE
	}

	public bool isActive = false;
	public bool isEnabled = true;

	public GameObject normalState;
	public GameObject overState;
	public GameObject activeState;

	public Color textColorOver;
	public Color textColorNormal;
	public Color textColorActive;
	public Color textColorDisabled;

	public string value;

	public FooEvent fooEvent;

	public object[] fooArgs;

	private bool[] currenStates;
}
