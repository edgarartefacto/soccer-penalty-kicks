﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;

public class Puntaje : MonoBehaviour 
{
	public static int puntaje = 0;
    public static int tiros = 1;
    //TextMesh puntajeTex;
    //public TMPro.TextMeshPro puntajeTex;
    public Text puntajeTex;
	void Start () {}
	
	// Update is called once per frame
	void Update () 
	{
		if(!Timer.sceneTraining)
		{
			if(puntaje.ToString("#,#", CultureInfo.InvariantCulture) != "")
				puntajeTex.text = puntaje.ToString("#,#", CultureInfo.InvariantCulture);
			else
				puntajeTex.text = "0";
		}
		else
			puntajeTex.text = "";
		if(puntaje > 0)
			AnimatorController2.score = puntaje;
	}
}
