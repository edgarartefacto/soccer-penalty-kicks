﻿using UnityEngine;
using System.Collections;

public class UIButtonGroup : MonoBehaviour
{
	void Start()
	{
		for(int i = 0; i < buttonsInGroup.Length; i++)
		{
			buttonsInGroup[i].fooArgs = new object[] { (object) buttonsInGroup[i] };
			buttonsInGroup[i].fooEvent = UpdateGroup;
		}

		if(buttonsInGroup.Length > 0)
		{
			current = buttonsInGroup[0];
		}

		mCurrentRadio = 0;
	}

	public UIButton Current
	{
		get { return current; }
	}

	public void Next()
	{
		mCurrentRadio++;
		if(mCurrentRadio >= buttonsInGroup.Length)
		{
			mCurrentRadio = 0;
		}

		buttonsInGroup[mCurrentRadio].OnMouseUp();
		buttonsInGroup[mCurrentRadio].OnMouseUpAsButton();
	}

	public void Unhighlight()
	{
		if(normalState != null && overState != null)
		{
			normalState.SetActive(true);
			overState.SetActive(false);
		}
	}

	public void Highlight()
	{
		if(normalState != null && overState != null)
		{
			normalState.SetActive(false);
			overState.SetActive(true);
		}
	}

	public void UpdateGroup(object[] args)
	{
		UIButton next = (UIButton) args[0];
		if(next != current)
		{
			for(int i = 0; i < buttonsInGroup.Length; i++)
			{
				if(next == buttonsInGroup[i])
				{
					current = buttonsInGroup[i];
				}
				else
				{
					buttonsInGroup[i].Reset();
				}
			}
		}
	}

	public void Reset()
	{
		for(int i = 0; i < buttonsInGroup.Length; i++)
		{
			buttonsInGroup[i].Reset();
		}

		current = buttonsInGroup[0];
		buttonsInGroup[0].OnMouseUp();
	}

	public GameObject normalState;
	public GameObject overState;

	public UIButton[] buttonsInGroup;

	private UIButton current;

	private int mCurrentRadio;
}
