﻿using UnityEngine;
using System.Collections;
using TMPro;

public class UIText : MonoBehaviour
{
	void Start ()
	{
		currenStates = new bool[3];
	}
	
	void Update () {}

	public void OnMouseDown()
	{
		currenStates[(int) State.NORMAL] = false;
		currenStates[(int) State.OVER] = true;
		currenStates[(int) State.ACTIVE] = false;
		UpdateState();
	}
	
	public void OnMouseUp()
	{
		currenStates[(int) State.OVER] = false;
		if(isActive)
		{
			currenStates[(int) State.NORMAL] = false;
			currenStates[(int) State.ACTIVE] = true;
		}
		else
		{
			currenStates[(int) State.NORMAL] = true;
			currenStates[(int) State.ACTIVE] = false;
		}

		if(isActive)
		{
			if(fooEvent != null)
			{
				fooEvent(fooArgs);
			}
		}

		UpdateState();
	}

	public void OnMouseUpAsButton()
	{
		if(!isActive)
		{
			if(fooEvent != null)
			{
				fooEvent(fooArgs);
			}
		}
	}

	public void ResetToNormal()
	{
		currenStates[(int) State.NORMAL] = true;
		currenStates[(int) State.OVER] = false;
		currenStates[(int) State.ACTIVE] = false;
		UpdateState();
	}

	public void Reset()
	{
		input.text = "";
	}

	public void UpdateState()
	{
		if(normalState != null)
		{
			normalState.SetActive(currenStates[(int) State.NORMAL]);
		}
		
		if(overState != null)
		{
			overState.SetActive(currenStates[(int) State.OVER]);
		}
		
		if(activeState != null)
		{
			activeState.SetActive(currenStates[(int) State.ACTIVE]);
		}
	}

	enum State
	{
		NORMAL,
		OVER,
		ACTIVE
	}

	public int characterLimit;

	public bool isActive;

	public GameObject normalState;
	public GameObject overState;
	public GameObject activeState;

	public TextMeshPro input;

	public FooEvent fooEvent;

	public object[] fooArgs;

	private bool[] currenStates;
}
