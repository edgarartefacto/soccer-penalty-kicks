﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Timer : MonoBehaviour 
{
	public static float myTimer;
	public static bool sceneTraining = true;
	public static float progress = -0.2f;
	public static float timeToValueTo;
	public static string timerWait;
	public static float quantityToUp;
//	public static bool takedPhoto = false;

	public bool alreadyRestarted = false;
	public GameObject practiceSprite;
//	public GameObject hexagono;
	public GameObject timeText;
	public GameObject fiveShots;
	public GameObject messageAlert1;
	public GameObject messageAlert2;

	float progresstimeText = -1.0f;
	float progresstimeMessage = -1.0f;
	int myIntTimer;
	int tempTimer = 0;
    //TextMesh timer;
    //TMPro.TextMeshPro timer;
    public Text timer;
	private Vector4 color;

	Launcher mLauncher;
	
	void Awake()
	{
		mLauncher = GameObject.FindObjectOfType<Launcher>();
	}

	// Use this for initialization
	void Start () 
	{
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		fiveShots.renderer.material.color = color;
		messageAlert1.renderer.material.color = color;
		messageAlert2.renderer.material.color = color;
        //		hexagono = GameObject.Find ("Hexagono");
        //		timeText = GameObject.Find ("Time-Text");
        if(AnimatorController2.nTiro == 1 && !sceneTraining)
		{
			progress = -0.2f;
            //myTimer = 71;
            myTimer = XMLConfiguration.GetSetting<float>("GameTime", 60);
            timerWait = XMLConfiguration.GetSetting<string>("GameTime", "60");
            //timeToValueTo = 61.0f;
            timeToValueTo = XMLConfiguration.GetSetting<float>("GameTime", 60);

            iTween.MoveBy(fiveShots, iTween.Hash("EaseType", "easeOutSine", "y", 1.1, "time", 0.5f));
			iTween.MoveBy(fiveShots, iTween.Hash("EaseType", "easeInSine", "y", -1.1, "time", 0.5f, "Delay", 2));
			iTween.FadeTo (fiveShots, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
			iTween.FadeTo (fiveShots, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 2));
			Invoke ("StartTimer", 10);
		}
		else if(AnimatorController2.nTiro == 1 && sceneTraining)
		{
			progress = -0.2f;
			myTimer = 41;
			timerWait = "0";
			timeToValueTo = 31.0f;
			iTween.MoveBy(practiceSprite, iTween.Hash("EaseType", "easeOutSine", "x", -3.9, "time", 1.0f));
			iTween.MoveBy(practiceSprite, iTween.Hash("EaseType", "easeInSine", "x", 3.9, "time", 1.0f, "Delay", 1.5f));
			iTween.ValueTo(gameObject, iTween.Hash("from", progresstimeMessage, "to", 2.0f, "time", 1.0f, "Delay", 0.7f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeMessageValue"));
			Invoke ("StartTimer", 10);
		}
		if(AnimatorController2.nTiro > 1)
			iTween.ValueTo(gameObject, iTween.Hash("from", progress, "to", 1.0f, "time", timeToValueTo, "onupdatetarget", gameObject, "onupdate", "TweenedValue"));
		if(AnimatorController2.nTiro == 1)
		{
			
			iTween.MoveBy (messageAlert2, iTween.Hash("EaseType", "easeOutSine", "y", 1.15f, "time", 0.5f, "Delay", 1.5));
			iTween.MoveBy (messageAlert2, iTween.Hash("EaseType", "easeInSine", "y", -1.15f, "time", 0.5f, "Delay", 4f));
			iTween.FadeTo (messageAlert2, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "Delay", 1.5));
			iTween.FadeTo (messageAlert2, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 4f));
			
		}

		//Get the 3D text called "timer"
		//timer = gameObject.GetComponent<TMPro.TextMeshPro>();
		if(sceneTraining)
		{
			float timing = (AnimatorController2.nTiro > 1) ? 11.5f : 19.0f;
			StartCoroutine(PrepareRestart(timing));
		}

//		if(sceneTraining)<------------ CHECK THIS
//		{
//			if(AnimatorController2.nTiro > 1)
//				Invoke ("ForceZeroTraining", 11.5f);
//			else
//				Invoke ("ForceZeroTraining", 19);
//		}
			
	}

	void StartTimer()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", progress, "to", 1.0f, "time", timeToValueTo, "onupdatetarget", gameObject, "onupdate", "TweenedValue"));
	}

	void TweenedtimeMessageValue(float val)
	{
		progresstimeMessage = val;
	}

	void TweenedValue(float val)
	{
		progress = val;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!sceneTraining)
		{
			if(myTimer > Convert.ToInt32(timerWait))
				timer.text = timerWait;
			else
				timer.text = myIntTimer.ToString();
//			hexagono.renderer.material.SetFloat("_Progress", progress);
			//timeText.renderer.material.SetFloat("_Progress", progresstimeText);
			//practiceSprite.renderer.material.SetFloat("_Progress", progresstimeMessage);
			//progress += quantityToUp;
			/*progresstimeText += 0.08f;
			if(progresstimeText >= 1)
				progresstimeText = -1.0f;
                */
			if(myTimer >= 1)
			{
				myTimer -= Time.deltaTime;
				myIntTimer = Convert.ToInt32(myTimer);
				myIntTimer -= 1;
				timeToValueTo = myIntTimer;
				if(tempTimer != myIntTimer)
				{
					//print(tempTimer);
					tempTimer = myIntTimer;
					//progress += quantityToUp;
				}
			}
			else if(myIntTimer == 3 && AnimatorController2.canShot == true)
			{
				AnimatorController2.canShot = false;
				AnimatorController2.activeAtaj = -1;
				if(mLauncher != null)
				{
					StartCoroutine(mLauncher.Execute(3.0f));
				}
				else
				{
					Debug.Log("Timer: launcher is NULL");
				}
				//				ForceZero();
			}
			else
			{// (' - ')
				if(mLauncher != null)
				{
					StartCoroutine(mLauncher.Execute());
				}
				else
				{
					Debug.Log("Timer: launcher is NULL");
				}
//				Restart();//<---- CHANGE WITH LAUNCHER			
			}
		}
		else
		{
			timer.text = "0";
//			hexagono.renderer.material.SetFloat("_Progress", -2.0f);
		}
	}

	private IEnumerator PrepareRestart(float time)
	{
		yield return new WaitForSeconds(time);
		AnimatorController2.canShot = false;
		AnimatorController2.activeAtaj = -1;
		AnimatorController2.nTiro = Launcher.MAX_TRAINING_SHOTS;

		if(mLauncher != null)
		{
			StartCoroutine(mLauncher.Execute(5.0f));
		}
		else
		{
			Debug.Log("Timer: launcher is NULL");
		}

		yield return null;
	}

//	void ForceZero()
//	{
//		AnimatorController2.canShot = false;
//		AnimatorController2.activeAtaj = -1;
//		Invoke ("Restart", 3);
//	}

//	void ForceZeroTraining()
//	{
//		if(AnimatorController2.canShot == true)
//		{
//			AnimatorController2.canShot = false;
//			AnimatorController2.nTiro++;
//			AnimatorController2.activeAtaj = -1;
//			Invoke ("RestartTraining", 5);
//		}
//	}

//	void RestartTraining()
//	{
//		//SampleScript.video.Stop();
//		AnimatorController2.time = 0;
//		AnimatorController2.activeAtaj = 0;
//		AnimatorController2.isWin = 1;
//		AnimatorController2.nTiroModified = false;
//		AnimatorController2.alreadyInverse = false;
//		AnimatorController2.canShot = false;
//		Kinect.shotDone = false;
//		if(AnimatorController2.nTiro == 2)
//		{
//			AnimatorController2.nTiro = 1;
//			Timer.progress = -0.2f;
//			Puntaje.puntaje = 0;
//			Bonus.bonus = 1;		
//			Kinect.numberCometShot = 1;
//			Kinect.numberCometOuterShot = 1;
//			Timer.sceneTraining = false;
//			AnimatorController2.score = 0;
//			Application.LoadLevel (Application.loadedLevel);
//		}
//		else
//			Application.LoadLevel (Application.loadedLevel);
//	}

//	void Restart()
//	{
//		//ES AQUI
//		if(alreadyRestarted)
//			return;
//		alreadyRestarted = true;
//		//print ("Entra al restart de tiempo");
//		progress = -0.2f;
//		Puntaje.puntaje = 0;
//		Bonus.bonus = 1;
//		//SampleScript.video.Stop();
//		AnimatorController2.time = 0;
//		AnimatorController2.activeAtaj = 0;
//		AnimatorController2.isWin = 1;
//		AnimatorController2.nTiroModified = false;
//		AnimatorController2.alreadyInverse = false;
//		AnimatorController2.canShot = false;
//		Kinect.shotDone = false;				
//		Kinect.numberCometShot = 1;
//		Kinect.numberCometOuterShot = 1;
//		if(sceneTraining)
//		{
//			//print ("Entra al termina entrenamiento");
//			sceneTraining = false;
//			Application.LoadLevel (Application.loadedLevel);
//		}
//		else
//		{
//			//print ("Entra al terminan penaltis");
//			sceneTraining = true;
//			if(!Configuration.isAssistedMode)
//			{
//				//print ("Entra al modo no asistido del restart del tiempo");
//				//print ("el tiro fue: " + AnimatorController2.nTiro);
//				if(takedPhoto)
//				{// (' - ')
//					takedPhoto = false;
//					Application.LoadLevel ("KeyBoard");
//				}
//				else
//				{
//					//VideoIntroScript.video.setPosition(0);
//					//VideoEndScript.video.Stop();
//					AnimatorController2.idPlayer = "";
//					AnimatorController2.email = "";
//					//	AnimatorController2.isAdult = "";
//					//	AnimatorController2.acceptPromos = "";
//					//	AnimatorController2.acceptDisplay = "";
//					AnimatorController2.mphAverage = 0;
//					AnimatorController2.score = 0;
//					//AnimatorController2.thumb = "";
//					AnimatorController2.photo1 = "";
//					AnimatorController2.photo2 = "";
//					AnimatorController2.photo3 = "";
//					AnimatorController2.photo4 = "";
//					Application.LoadLevel ("IntroVideo");
//				}
//			}
//			else
//			{
//				//print ("Entra al modo asistido del restart del tiempo");
//				if(takedPhoto)
//				{
//					takedPhoto = false;
//					//print ("Entra al modo ya casi escribe archivo del restart del tiempo");
//					AnimatorController2.mphAverage = AnimatorController2.mphAverage / 5;
//					//print ("Calculo promedio");
//					UploadDataAssisted();
//				}
//				else
//				{
//					//VideoIntroScript.video.setPosition(0);
//					//VideoEndScript.video.Stop();
//					AnimatorController2.idPlayer = "";
//					AnimatorController2.email = "";
//					//	AnimatorController2.isAdult = "";
//					//	AnimatorController2.acceptPromos = "";
//					//	AnimatorController2.acceptDisplay = "";
//					AnimatorController2.mphAverage = 0;
//					AnimatorController2.score = 0;
//					//AnimatorController2.thumb = "";
//					AnimatorController2.photo1 = "";
//					AnimatorController2.photo2 = "";
//					AnimatorController2.photo3 = "";
//					AnimatorController2.photo4 = "";
//					Application.LoadLevel ("IntroVideo");
//				}
//			}
//		}
//		AnimatorController2.nTiro = 1;
//	}

//	private void UploadDataAssisted()
//	{
//		//print ("Escribe archivo por tiempo");
//		string text = "";
//		if(AnimatorController2.photo1 == "" && AnimatorController2.photo2 == "" && AnimatorController2.photo3 == "" && AnimatorController2.photo4 == "")
//		{
//			//VideoIntroScript.video.setPosition(0);
//			//VideoEndScript.video.Stop();
//			AnimatorController2.idPlayer = "";
//			AnimatorController2.email = "";
//			//	AnimatorController2.isAdult = "";
//			//	AnimatorController2.acceptPromos = "";
//			//	AnimatorController2.acceptDisplay = "";
//			AnimatorController2.mphAverage = 0;
//			AnimatorController2.score = 0;
//			//AnimatorController2.thumb = "";
//			AnimatorController2.photo1 = "";
//			AnimatorController2.photo2 = "";
//			AnimatorController2.photo3 = "";
//			AnimatorController2.photo4 = "";
//			Application.LoadLevel ("IntroVideo");
//		}
//		else
//		{
//			//WWWForm form = new WWWForm();
//			if(AnimatorController2.mphAverage == 0)
//			{
//				//form.AddField("mph","0");
//				text += "0|";
//			}
//			else
//			{
//				//form.AddField("mph",AnimatorController2.mphAverage.ToString());
//				text += AnimatorController2.mphAverage.ToString() + "|";
//			}
//			if(AnimatorController2.score == 0)
//			{
//				//form.AddField("score","0");
//				text += "0|";
//			}
//			else
//			{
//				//form.AddField("score",AnimatorController2.score.ToString());
//				text += AnimatorController2.score.ToString() + "|";
//			}
//			//form.AddField("thumb","");
//
//			//form.AddField("photo1",AnimatorController2.photo1 + ".png");
//			text += AnimatorController2.photo1 + ".png|";
//			//form.AddField("photo2",AnimatorController2.photo2 + ".png");
//			if(AnimatorController2.photo2 == "")
//				text += AnimatorController2.photo1 + ".png|";
//			else
//				text += AnimatorController2.photo2 + ".png|";
//			//form.AddField("photo3",AnimatorController2.photo3 + ".png");
//			if(AnimatorController2.photo3 == "")
//				text += AnimatorController2.photo1 + ".png|";
//			else
//				text += AnimatorController2.photo3 + ".png|";
//			//form.AddField("photo4",AnimatorController2.photo4 + ".png");
//			if(AnimatorController2.photo4 == "")
//			{
//				if(AnimatorController2.photo2 == "")
//					text += AnimatorController2.photo1 + ".png|";
//				else
//				{
//					if(AnimatorController2.photo3 == "")
//						text += AnimatorController2.photo2 + ".png|";
//					else
//						text += AnimatorController2.photo1 + ".png|";
//				}
//			}
//			else
//				text += AnimatorController2.photo4 + ".png|";
//			//		WWW preg = new WWW("http://newsite.mvp-interactive.com/index.php?/hall/uploadDataAssisted",form);
//			//		StartCoroutine(WaitUploadDataAssisted(preg));
//			int consecutive = 0;
//			if(PlayerPrefs.HasKey("Data Consecutive"))
//			{
//				int oldConsecutive = PlayerPrefs.GetInt("Data Consecutive");
//				consecutive = oldConsecutive + 1;
//				PlayerPrefs.SetInt("Data Consecutive", consecutive);
//			}
//			else 
//				PlayerPrefs.SetInt("Data Consecutive", consecutive);
//			string paddedConsecutivo = consecutive.ToString().PadLeft(8, '0');
//			string path = Application.dataPath;
//			#if UNITY_EDITOR
//			path = path.Substring(0,Application.dataPath.Length - 7);
//			#else
//			path = path.Substring(0,Application.dataPath.Length - 16);
//			#endif
//			path += "/data/data" + paddedConsecutivo + ".txt";
//			System.IO.File.WriteAllText(path, text);
//			//print ("Termina scribe archivo por tiempo");
//			Application.LoadLevel ("EndScreen");
//		}
//	}

//	private void UploadDataAssisted()
//	{
//		WWWForm form = new WWWForm();
//		if(AnimatorController2.mphAverage == 0)
//			form.AddField("mph","0");
//		else
//			form.AddField("mph",AnimatorController2.mphAverage.ToString());
//		if(AnimatorController2.score == 0)
//			form.AddField("score","0");
//		else
//			form.AddField("score",AnimatorController2.score.ToString());
//		form.AddField("thumb","");
//		form.AddField("photo1",AnimatorController2.photo1 + ".png");
//		form.AddField("photo2",AnimatorController2.photo2 + ".png");
//		form.AddField("photo3",AnimatorController2.photo3 + ".png");
//		form.AddField("photo4",AnimatorController2.photo4 + ".png");
//		WWW preg = new WWW("http://newsite.mvp-interactive.com/index.php?/hall/uploadDataAssisted",form);
//		StartCoroutine(WaitUploadDataAssisted(preg));
//	}
	
//	IEnumerator WaitUploadDataAssisted(WWW www)
//	{
//		yield return www;		
//		// check for errors
//		if (www.error == null)
//		{
//			//print("Texto: " + www.text);
//			Application.LoadLevel ("IntroVideo");
//		}
//		else 
//		{
//			Debug.Log("WWW Error: "+ www.error);
//		}
//	}
}
