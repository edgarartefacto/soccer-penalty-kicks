﻿using UnityEngine;
using System.Collections;
using System.Xml;
using Microsoft.Win32;
using System.Text.RegularExpressions;

public class SignUp : MonoBehaviour
{
	void Start ()
	{
		inputs = gameObject.GetComponent<Keyboard>().mGroup;

		if(mButtonSend != null)
		{
			mButtonSend.fooEvent = Send;
		}

		if(mButtonRestart)
		{
			mButtonRestart.fooEvent = Restart;
		}

		textMessage.text = "";
	}
	
	void Update ()
	{
		if (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
		{
			Send(null);
		}
	}

	public void Send(object[] args)
	{
		mButtonSend.collider2D.enabled = false;
		mButtonRestart.collider2D.enabled = false;

		Player player = new Player();

		player.Name = "";//inputs.textInGroup[0].input.text;
		player.LastName = "";//inputs.textInGroup[1].input.text;
		player.Email = inputs.textInGroup[0].input.text;
		player.Zip = "";//inputs.textInGroup[3].input.text;

		int age = 0;
		int owned = 0;
		int market = 0;
		int info = 0;

		player.AgeRange = 0; //int.TryParse(inputs.buttonsGroup[0].Current.value, out age) ? age : 0;
		player.IsOwned = 0; //int.TryParse(inputs.buttonsGroup[1].Current.value, out owned) ? owned : 0;
		player.IsMarket = 0; //int.TryParse(inputs.buttonsGroup[2].Current.value, out market) ? market : 0;
		player.IsMore = 0; //int.TryParse(inputs.buttonsGroup[3].Current.value, out info) ? info : 0;

		/*if(!string.IsNullOrEmpty(player.Name) && !string.IsNullOrEmpty(player.Email) && !string.IsNullOrEmpty(player.Zip)
		   && !player.Name.Trim().Equals("") && !player.Email.Trim().Equals("") && !player.Zip.Trim().Equals(""))
		{*/
			bool isEmail = Regex.IsMatch(player.Email, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", RegexOptions.IgnoreCase);
			if(isEmail)
			{
				if(player.Email.Substring(player.Email.Length - 7) != "COM.COM")
				{
					/*if(player.Zip.Length == 5)
					{*/
						textMessage.text = "";
						AnimatorController2.mphAverage = AnimatorController2.mphAverage / 5;
						CreatePlayerXML(player, "0");
						
						mButtonSend.collider2D.enabled = true;
						mButtonRestart.collider2D.enabled = true;
						Application.LoadLevel ("EndScreen");
					/*}
					else
					{
						textMessage.text = "Incorrect Zip. Zip must be 5 digits.";
					}*/
				}
				else
				{
					textMessage.text = "Incorrect Email";
				}
			}
			else
			{
				textMessage.text = "Incorrect Email";
			}
		/*}
		else
		{
			textMessage.text = "All fields are required!";
		}**/

		mButtonSend.collider2D.enabled = true;
		mButtonRestart.collider2D.enabled = true;
	}

	public void Restart(object[] args)
	{
		AnimatorController2.idPlayer = "";
		AnimatorController2.email = "";
		AnimatorController2.mphAverage = 0;
		AnimatorController2.score = 0;

		for(int i = 0; i < PhotosKeyBoard.photosTaked.Length; i++)
		{
			PhotosKeyBoard.photosTaked[i] = "";
		}

		Application.LoadLevel ("IntroVideo");
	}

	public static void CreatePlayerXML(Player player, string assisted)
	{
		int position = PlayerPrefs.GetInt(Configuration.PREFS_XML_PLAYER, 0);
		position++;

		PlayerPrefs.SetInt(Configuration.PREFS_XML_PLAYER, position);

		using(XmlWriter writer = XmlWriter.Create("Data/" + Configuration.PREFS_XML_NAME + position + ".xml"))
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("player");
			writer.WriteElementString("assisted", assisted);
			writer.WriteElementString("name", player.Name);
			writer.WriteElementString("lastname", player.LastName);
			writer.WriteElementString("email", player.Email);
			writer.WriteElementString("zip", player.Zip);
			writer.WriteElementString("score", AnimatorController2.score.ToString());

			string[] photos = PhotosKeyBoard.GetPhotosName();
			writer.WriteElementString("photo1", photos[0] + ".png");
			writer.WriteElementString("photo2", photos[1] + ".png");
			writer.WriteElementString("photo3", photos[2] + ".png");
			writer.WriteElementString("photo4", photos[3] + ".png");

			writer.WriteElementString("mph", AnimatorController2.mphAverage.ToString());
			writer.WriteElementString("age", player.AgeRange.ToString());
			writer.WriteElementString("owned", player.IsOwned.ToString());
			writer.WriteElementString("market", player.IsMarket.ToString());
			writer.WriteElementString("info", player.IsMore.ToString());

			writer.WriteEndElement();
			writer.WriteEndDocument();
		}

		const string userRoot = "HKEY_CURRENT_USER";
		const string subkey = "RegistryValueID";
		const string keyName = userRoot + "\\" + subkey;
		
		string testID = (string) Registry.GetValue(keyName, "ValueID", "-1");
		AnimatorController2.idPlayer = testID;
	}

	public TextMesh textMessage; 

	public UIButton mButtonSend;
	public UIButton mButtonRestart;

	private AudiInput inputs;
}
