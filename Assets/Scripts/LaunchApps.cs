﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class LaunchApps : MonoBehaviour {

	[DllImport ("LaunchApplication")]
	private static extern void LaunchAppIfNotOpened (string appName);

	[DllImport("user32.dll")]
	static extern uint GetActiveWindow();

	[DllImport("user32.dll")]
	static extern bool SetForegroundWindow(IntPtr hWnd);

	[DllImport("user32.dll")]
	static extern bool ShowWindow(IntPtr hWnd,int show);
	
	public string[] apps;
	private IntPtr w;

	// Use this for initialization
	void Start () {

		//w = (IntPtr)GetActiveWindow ();
		//print (w);

		foreach (string appName in apps) {
			LaunchAppIfNotOpened (appName);
		}

		Invoke ("NextScene",1.5f);
	}

	/*void OnApplicationFocus(bool focusStatus) {

		if (!focusStatus) {
			//bool r = SetForegroundWindow (w);
			ShowWindow(w,3);

		}
	}*/

	void NextScene()
	{
		Application.LoadLevel ("Init");
	}
}
