﻿using UnityEngine;
using System.Collections;

using System;
using System.Runtime.InteropServices;	
using System.Diagnostics;

/*[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class tTouchData{
	public int m_x;
	public int m_y;
	public int m_ID;
	public int m_Time;
};
[StructLayout(LayoutKind.Sequential, Pack = 1)]
*/

public class PrintMouse : MonoBehaviour {
	
	public bool m_Initialised;
	
    [DllImport("DLLTo", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern int Initialise(string Str);
    
	[DllImport("DLLTo")]
	public static extern int GetTouchPointCount();
	
	[DllImport ("DLLTo")]
	public static extern void GetTouchPoint(int i, tTouchData n);
	
	[DllImport ("DLLTo")]
	public static extern void Test(int i);
	
	public TextMesh inputX;
	public TextMesh inputY;
	Vector3 point;

	// Use this for initialization
	void Start () {
		m_Initialised = false;
	}
	
	// Update is called once per frame
	void Update () {
		

		
		/*Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		inputX.text = point.x.ToString();
		inputY.text = point.y.ToString();*/
	}
	
	void OnGUI () {
		
		string Str;
		int NumTouch = 0;
		int res;
		if (!m_Initialised)
		{
			Str = "Raycast Reflection";
			//Str = "Win 32 Ejemplo";
			res = Initialise(Str);
			print(res);
			if (res < 0)
			{
				// ERROR STATE
			}
			//Test(5);
			m_Initialised = true;
		}
		
		NumTouch = GetTouchPointCount ();
		Str = "Number of Touch Points: " + NumTouch.ToString();
		print(Str);
		GUI.Label (new Rect (10,10,150,40), Str);
		for (int p=0; p<NumTouch; p++)
		{
			tTouchData TouchData = new tTouchData();
			GetTouchPoint (p, TouchData);
			
			point = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_x/100f,Screen.height-(TouchData.m_y/100f),0));
			GUI.Label (new Rect (10,10 + (p+1) * 40, 400, 40), 
				"ID:" + TouchData.m_ID + 
				" Time:" + TouchData.m_Time.ToString() + 
				" (" + TouchData.m_x.ToString() + "," + TouchData.m_y.ToString() + ") , (" + point.x + "," + point.y + ")");
			
		}
		//Test(5);
	}	
}
