﻿using UnityEngine;
using System.Collections;

using System;
using System.Runtime.InteropServices;	
using System.Diagnostics;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class tTouchData{
	public int m_x;
	public int m_y;
	public int m_ID;
	public int m_Time;
};
[StructLayout(LayoutKind.Sequential, Pack = 1)]


public class TouchMouse : MonoBehaviour {
	
	private float timeToReset;
	
	public bool m_Initialised;
	
    [DllImport("TouchOverlay", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern int Initialise(string Str);
    
	[DllImport("TouchOverlay")]
	public static extern int GetTouchPointCount();
	
	[DllImport ("TouchOverlay")]
	public static extern void GetTouchPoint(int i, tTouchData n);
	
	[DllImport ("TouchOverlay")]
	public static extern void Test(int i);
	
	public string nameWindow;
	
	public ArrayList touches;
	private ArrayList tempTouches;
	
	private Ray ray;
	private RaycastHit hit;
	
	public Camera cameraMain;
	
	void Awake () {
		DontDestroyOnLoad(gameObject);
	}
	// Use this for initialization
	void Start () {
		timeToReset = 300;
		
		if(cameraMain == null)
			cameraMain = Camera.main;
		
		touches = new ArrayList();
		tempTouches = new ArrayList();
		m_Initialised = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		int NumTouch = 0;
		int res;
		bool wasActivated;
		if (!m_Initialised)
		{
			res = Initialise(nameWindow);
			if (res < 0)
			{
				// ERROR STATE
			}
			m_Initialised = true;
		}
		
		NumTouch = GetTouchPointCount ();
		tempTouches.Clear();
		foreach(tTouchData touch in touches){
			tempTouches.Add(touch);
		}
		touches.Clear();
		
		for (int p=0; p<NumTouch; p++)
		{
			wasActivated = false;
			tTouchData TouchData = new tTouchData();
			GetTouchPoint (p, TouchData);
			
			touches.Add(TouchData);
			
			foreach(tTouchData touch in tempTouches){
				if(touch.m_ID == TouchData.m_ID){
					//suavisado
					TouchData.m_x = Mathf.RoundToInt(touch.m_x * 0.7f) + Mathf.RoundToInt(TouchData.m_x * 0.3f);
					TouchData.m_y = Mathf.RoundToInt(touch.m_y * 0.7f) + Mathf.RoundToInt(TouchData.m_y * 0.3f);
					//tempTouches.Remove(touch);
					wasActivated = true;
				}
			}
			
			if(!wasActivated){
				ray = cameraMain.ScreenPointToRay(new Vector3(TouchData.m_x/100f,Screen.height-(TouchData.m_y/100f),0));
				
				if(Physics.Raycast(ray.origin,ray.direction, out hit, 2500)){
					hit.transform.SendMessage("OnTouchDown",TouchData.m_ID,SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		
		
		//Reset App
		if(NumTouch > 0){
			timeToReset = 300;
		}
		else{
			if(Application.loadedLevelName != "Init"){
				timeToReset -= Time.deltaTime;
				if(timeToReset < 0)
					Application.LoadLevel("Init");
			}
		}
	}
	
	void OnLevelWasLoaded(int level) {
		cameraMain = Camera.main;
	}
	
	/// <summary>
    /// Si z es negativo significa que no existe el touch con ese id;
    /// </summary>
	public Vector3 GetScreenPositionTouch(int id){
		Vector3 sPosition = new Vector3(0,0,-1f);
		foreach(tTouchData touch in touches){
			if(touch.m_ID == id){
				sPosition = cameraMain.ScreenToWorldPoint(new Vector3(touch.m_x/100f,touch.m_y/100f,0));
			}
		}
		
		return sPosition;
	}
	
	public tTouchData GetTouch(int id){
		tTouchData sTouch = null;
		foreach(tTouchData touch in touches){
			if(touch.m_ID == id){
				sTouch = touch;
			}
		}
		
		return sTouch;
	}
}
