﻿using UnityEngine;
using System.Collections;

public class SetLights : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{

	}

	void Update () 
	{
		//Number of lights
		int NLights = 0;
		//Array for all the lights
		object[] lights = FindObjectsOfType (typeof(Light));
		Light l;
		//Our arrays lights and colors
		Vector4[] luces = new Vector4[4];
		Color[] colors = new Color[4];
		for(int i = 0; i < lights.Length && i < 4; i++) //finish when laghts are 4 or less
		{
			NLights++;
			l = (Light)lights[i]; //Asign current light
			if( l.type == LightType.Point ) //PointLight?
			{
				//Give values
				luces[i].x = l.transform.position.x; 
				luces[i].y = l.transform.position.y; 
				luces[i].z = l.transform.position.z;
				luces[i].w = 1.0f;
				colors[i] = l.color;
				//Send the values to our shader
				renderer.material.SetVector("_VectorLight" + NLights, luces[i]);
				renderer.material.SetColor("_ColorLight" + NLights, colors[i]);
			}
			if( l.type == LightType.Directional ) //DirectionalLight
			{
				luces[i].x = -l.transform.forward.x;
				luces[i].y = -l.transform.forward.y; 
				luces[i].z = -l.transform.forward.z;
				luces[i].w = 0.0f;
				colors[i] = l.color;
				renderer.material.SetVector("_VectorLight" + NLights, luces[i]);
				renderer.material.SetColor("_ColorLight" + NLights, colors[i]);
			}
		}
		//Send to the shader the number of active lights in the scene
		renderer.material.SetInt ("_nLights", NLights);
	}
}
