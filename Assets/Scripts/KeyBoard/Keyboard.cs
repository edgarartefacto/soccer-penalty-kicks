﻿using UnityEngine;
using System.Collections;
using Microsoft.Win32;
using System.Text.RegularExpressions;

public class Keyboard : MonoBehaviour
{//UploadDataAssisted - TriggerWin
	void Start () {}
	
	void Update ()
	{
		string key = Input.inputString;
//		print(key);
		if(!string.IsNullOrEmpty(key))
		{
			for(int i = 0; i < key.Length; i++)
			{
				string ch = key[i].ToString();
				if(Regex.IsMatch(ch, audiPattern))
				{
					AddInputKey(ch);
				}
				else if(ch.Equals("\b"))
				{
					AddInputKey(KEY_RETURN);
				}
				else if(ch.Equals(" "))
				{
					AddInputKey(KEY_SPACE);
				}
			}
		}

		if(Input.GetKeyDown(KeyCode.Asterisk))
		{
			Screen.showCursor = true;
		}
	}

	public void AddInputKey(string key)
	{
		if(mGroup.Current != null)
		{
			string value = mGroup.Current.input.text;
			if(value.Length + 1 <= mGroup.Current.characterLimit || key.Equals(KEY_RETURN))
			{
				if(key.Equals(KEY_RETURN))
				{
					if(value.Length > 1)
					{
						value = value.Substring(0, value.Length - 1);
					}
					else
					{
						value = "";
					}
				}
				else if(key.Equals(KEY_SPACE))
				{
					value += " ";
				}
				else
				{
					value += key;
				}
				
				mGroup.Current.input.text = value.ToUpper();
				mGroup.Current.input.ForceMeshUpdate();
			}
		}
	}

	public AudiInput mGroup;

	private static string KEY_RETURN = "return";
	private static string KEY_SPACE = "space";

	private string audiPattern = @"^[a-zA-Z0-9,.@\_-]+$";
}
