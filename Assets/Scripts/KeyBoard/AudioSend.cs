﻿using UnityEngine;
using System.Collections;

public class AudioSend : MonoBehaviour 
{
	private AudioSource[] sounds;
	private AudioSource send;

	// Use this for initialization
	void Start () 
	{
		sounds = gameObject.GetComponents<AudioSource> ();
		send = sounds [0];
	}

	void OnMouseUp()
	{
		send.Play();
	}
}
