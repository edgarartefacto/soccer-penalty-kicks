﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Globalization;

public class PhotosKeyBoard : MonoBehaviour 
{
	void Start () 
	{
		string path = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/"));
		url = "file:/" + path + "/" + Configuration.photoDir;
		Debug.Log("URL_Base=" + url);

		mPhotos = new PhotoHandler[N_PHOTOS];
		for(int i = 0; i < mPhotos.Length; i++)
		{
			mPhotos[i] = new PhotoHandler(5000);
		}

		int indexPhoto = GetInitialPhoto();
		// Fill up from initial photo (not empty) to back
		for(int i = 0; i < indexPhoto; i++)
		{
			mPhotos[i].Name = photosTaked[i] + ".png";
		}

		// Check for empty photos and fill it
		for(int i = indexPhoto; i < photosTaked.Length; i++)
		{
			mPhotos[i].Name = (string.IsNullOrEmpty(photosTaked[i]) ? 
			                photosTaked[i - 1] : photosTaked[i]) + ".png";
		}

		if(AnimatorController2.score == 0)
		{
			textScorePhotos.text = "0";
			textScoreHead.text = "0";
		}
		else
		{
			textScorePhotos.text = AnimatorController2.score.ToString ("#,#", CultureInfo.InvariantCulture);
			textScoreHead.text = AnimatorController2.score.ToString ("#,#", CultureInfo.InvariantCulture);
		}

		for(int i = 0; i < mPhotos.Length; i++)
		{
			mPhotos[i].StartTimer();
			StartCoroutine(ChargePhoto(new WWW(url + "/" + mPhotos[i].Name), mPhotos[i]));
		}

		StartCoroutine(ProcessPhotos());
	}

	private IEnumerator ChargePhoto(WWW wwwPhoto, PhotoHandler photo)
	{
		yield return wwwPhoto;

		if(wwwPhoto.error == null)
		{
			photo.Texture = wwwPhoto.texture;
		}
		else
		{
			if(photo.IsWorking)
			{
				string path = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/"));
				
				if(!photo.IsInBackup)
				{
					url = "file:/" + path + "/" + Configuration.photoDir + "/backup";
					photo.IsInBackup = true;
				}
				else
				{
					url = "file:/" + path + "/" + Configuration.photoDir;
					photo.IsInBackup = false;
				}
				
				WWW wwwLocation = new WWW(url + "/" + photo.Name);
				StartCoroutine(ChargePhoto(wwwLocation, photo));
			}
		}
	}

	private bool PhotosCharged()
	{
		for(int i = 0; i < mPhotos.Length; i++)
		{
			if(mPhotos[i].Texture == null && mPhotos[i].IsWorking)
			{
				return false;
			}
		}

		return true;
	}

	private IEnumerator ProcessPhotos()
	{
		yield return new WaitForSeconds(1.0f);
		if(PhotosCharged())
		{
			int index = GetInitialTexture();

			for(int i = 0; i < index; i++)
			{
				mPhotos[i].Texture = mPhotos[index].Texture;
			}

			for(int i = index; i < mPhotos.Length; i++)
			{
				mPhotos[i].Texture = mPhotos[i].Texture == null ? 
				                   mPhotos[i - 1].Texture : mPhotos[i].Texture;
			}

			StartCoroutine(MakeGif());

			yield break;
		}
		else
		{
			StartCoroutine(ProcessPhotos());
		}
		yield return null;
	}

	private IEnumerator MakeGif()
	{
		yield return new WaitForSeconds(0.3f);
		photo.renderer.material.mainTexture = mPhotos[1].Texture;
		yield return new WaitForSeconds(0.3f);
		photo.renderer.material.mainTexture = mPhotos[0].Texture;
		yield return new WaitForSeconds(0.3f);
		photo.renderer.material.mainTexture = mPhotos[2].Texture;

		StartCoroutine(MakeGif());
	}

	public static string[] GetPhotosName()
	{
		string[] photosName = new string[mPhotos.Length];
		for(int i = 0; i < photosName.Length; i++)
		{
			photosName[i] = mPhotos[i].Name.Replace(".png", "");
		}

		return photosName;
	}

	private int GetInitialPhoto()
	{
		for(int i = 0; i < photosTaked.Length; i++)
		{
			if(!string.IsNullOrEmpty(photosTaked[i]))
			{
				return i;
			}
		}

		return -1;
	}

	private int GetInitialTexture()
	{
		for(int i = 0; i < mPhotos.Length; i++)
		{
			if(mPhotos[i].Texture != null)
			{
				return i;
			}
		}
		
		return -1;
	}

	public const int N_PHOTOS = 4;
	
	public static string[] photosTaked = new string[N_PHOTOS];
	public static PhotoHandler[] mPhotos;

	public GameObject photo;

	public TextMesh textScorePhotos;
	public TextMesh textScoreHead;

	private string url;

	private int nPhoto = 0;
}
