﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour
{
	void Awake()
	{
		mKeyboard = GameObject.FindObjectOfType<Keyboard>();

		mButton = gameObject.GetComponent<UIButton>();
		mButton.fooArgs = new object[] { (object) mButton.value };
		mButton.fooEvent = InputKeyButton;
	}

	void Start () {}
	
	void Update () {}

	private void InputKeyButton(object[] args)
	{
		string key = (string) args[0];
		mKeyboard.AddInputKey(key);
	}

	private Keyboard mKeyboard;

	private UIButton mButton;
}
