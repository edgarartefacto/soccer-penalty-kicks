﻿using UnityEngine;
using System.Collections;

public class ScaleFade : MonoBehaviour 
{
	private Vector3 size0;
	private Vector4 color;

	void Start()
	{
		//Wait 7.5 seconds
		//Invoke("StartCounting", 7.5f);
		if(AnimatorController2.nTiro > 1)
			Invoke("CanShot", 1);
		else
			Invoke("CanShot", 10);
	}

	void CanShot()
	{
		AnimatorController2.canShot = true;
	}
}
