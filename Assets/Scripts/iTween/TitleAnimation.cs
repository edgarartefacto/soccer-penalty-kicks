﻿using UnityEngine;
using System.Collections;

public class TitleAnimation : MonoBehaviour 
{
	private Vector3 sizeBig;

	void Start()
	{
		sizeBig.x = 0.16f;
		sizeBig.y = 0.16f;
		sizeBig.z = 0.16f;
		iTween.ScaleTo (gameObject, iTween.Hash ("scale", sizeBig, "time", 1.0f, "looptype", "pingPong"));
	}
}
