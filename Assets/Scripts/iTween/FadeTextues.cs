﻿using UnityEngine;
using System.Collections;

public class FadeTextues : MonoBehaviour 
{
	private Vector4 color;

	// Use this for initialization
	void Start ()
	{
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		if(gameObject.name == "3r" || gameObject.name == "2r" || gameObject.name == "1r" || gameObject.name == "ReadyActive")
			gameObject.renderer.material.color = color;
		//Wait 11 seconds
		Invoke("StartCounting", 7.5f);
	}
	
	//Called when want to start counting
	void StartCounting()
	{
		if (gameObject.name == "3") 
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 1.4));
		}
		if (gameObject.name == "2")
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 1.4));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 2.6));
		}
		if (gameObject.name == "1") 
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 2.6));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 3.8));
		}
		if (gameObject.name == "Ready")
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 3.8));
		if (gameObject.name == "3r") 
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 1.4));
		}
		if (gameObject.name == "2r") 
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 1.4));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 2.6));
		}
		if (gameObject.name == "1r") 
		{
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 2.6));
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", 3.8));
		}
		if (gameObject.name == "ReadyActive")
			iTween.FadeTo (gameObject, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", 3.8));
	}
}
