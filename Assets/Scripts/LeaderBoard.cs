﻿using UnityEngine;
using System.Collections;
using System;
using System.Globalization;
using Microsoft.Win32;
using System.Xml;
using System.IO;

public class LeaderBoard : MonoBehaviour 
{
	private string RString = "";

	public GameObject photo1;
	public GameObject photo2;
	public GameObject photo3;
	public GameObject photo4;
	public GameObject photo5;
	public GameObject photo6;

	//Dates
	TMPro.TextMeshPro date1;
	TMPro.TextMeshPro date2;
	TMPro.TextMeshPro date3;
	TMPro.TextMeshPro date4;
	TMPro.TextMeshPro date5;
	TMPro.TextMeshPro date6;
	//Ranks
	TMPro.TextMeshPro rank1;
	TMPro.TextMeshPro rank2;
	TMPro.TextMeshPro rank3;
	TMPro.TextMeshPro rank4;
	TMPro.TextMeshPro rank5;
	TMPro.TextMeshPro rank6;
	//Players
	TMPro.TextMeshPro player1;
	TMPro.TextMeshPro player2;
	TMPro.TextMeshPro player3;
	TMPro.TextMeshPro player4;
	TMPro.TextMeshPro player5;
	TMPro.TextMeshPro player6;
	//MPH
	TMPro.TextMeshPro mph1;
	TMPro.TextMeshPro mph2;
	TMPro.TextMeshPro mph3;
	TMPro.TextMeshPro mph4;
	TMPro.TextMeshPro mph5;
	TMPro.TextMeshPro mph6;
	//Scores
	TMPro.TextMeshPro score1;
	TMPro.TextMeshPro score2;
	TMPro.TextMeshPro score3;
	TMPro.TextMeshPro score4;
	TMPro.TextMeshPro score5;
	TMPro.TextMeshPro score6;

	// Use this for initialization
	void Start () 
	{
		//Dates
		date1 = GameObject.Find("Date1").GetComponent<TMPro.TextMeshPro>();
		date2 = GameObject.Find("Date2").GetComponent<TMPro.TextMeshPro>();
		date3 = GameObject.Find("Date3").GetComponent<TMPro.TextMeshPro>();
		date4 = GameObject.Find("Date4").GetComponent<TMPro.TextMeshPro>();
		date5 = GameObject.Find("Date5").GetComponent<TMPro.TextMeshPro>();
		date6 = GameObject.Find("Date6").GetComponent<TMPro.TextMeshPro>();
		//Ranks
		rank1 = GameObject.Find("Rank1").GetComponent<TMPro.TextMeshPro>();
		rank2 = GameObject.Find("Rank2").GetComponent<TMPro.TextMeshPro>();
		rank3 = GameObject.Find("Rank3").GetComponent<TMPro.TextMeshPro>();
		rank4 = GameObject.Find("Rank4").GetComponent<TMPro.TextMeshPro>();
		rank5 = GameObject.Find("Rank5").GetComponent<TMPro.TextMeshPro>();
		rank6 = GameObject.Find("Rank6").GetComponent<TMPro.TextMeshPro>();
		//Players
		player1 = GameObject.Find("Player1").GetComponent<TMPro.TextMeshPro>();
		player2 = GameObject.Find("Player2").GetComponent<TMPro.TextMeshPro>();
		player3 = GameObject.Find("Player3").GetComponent<TMPro.TextMeshPro>();
		player4 = GameObject.Find("Player4").GetComponent<TMPro.TextMeshPro>();
		player5 = GameObject.Find("Player5").GetComponent<TMPro.TextMeshPro>();
		player6 = GameObject.Find("Player6").GetComponent<TMPro.TextMeshPro>();
		//MPH
		mph1 = GameObject.Find("mph1").GetComponent<TMPro.TextMeshPro>();
		mph2 = GameObject.Find("mph2").GetComponent<TMPro.TextMeshPro>();
		mph3 = GameObject.Find("mph3").GetComponent<TMPro.TextMeshPro>();
		mph4 = GameObject.Find("mph4").GetComponent<TMPro.TextMeshPro>();
		mph5 = GameObject.Find("mph5").GetComponent<TMPro.TextMeshPro>();
		mph6 = GameObject.Find("mph6").GetComponent<TMPro.TextMeshPro>();
		//Scores
		score1 = GameObject.Find("score1").GetComponent<TMPro.TextMeshPro>();
		score2 = GameObject.Find("score2").GetComponent<TMPro.TextMeshPro>();
		score3 = GameObject.Find("score3").GetComponent<TMPro.TextMeshPro>();
		score4 = GameObject.Find("score4").GetComponent<TMPro.TextMeshPro>();
		score5 = GameObject.Find("score5").GetComponent<TMPro.TextMeshPro>();
		score6 = GameObject.Find("score6").GetComponent<TMPro.TextMeshPro>();
		//End texts
		InvokeRepeating ("CargaData", 1, 2);
		Invoke ("ResetID", 9.5f);
		date1.text = PlayerPrefs.GetString("LBDate1", "");
		date2.text = PlayerPrefs.GetString("LBDate2", "");
		date3.text = PlayerPrefs.GetString("LBDate3", "");
		date4.text = PlayerPrefs.GetString("LBDate4", "");
		date5.text = PlayerPrefs.GetString("LBDate5", "");
		date6.text = PlayerPrefs.GetString("LBDate6", "");
		rank1.text = PlayerPrefs.GetString("LBRank1", "");
		rank2.text = PlayerPrefs.GetString("LBRank2", "");
		rank3.text = PlayerPrefs.GetString("LBRank3", "");
		rank4.text = PlayerPrefs.GetString("LBRank4", "");
		rank5.text = PlayerPrefs.GetString("LBRank5", "");
		rank6.text = PlayerPrefs.GetString("LBRank6", "");
		player1.text = PlayerPrefs.GetString("LBPlayer1", "");
		player2.text = PlayerPrefs.GetString("LBPlayer2", "");
		player3.text = PlayerPrefs.GetString("LBPlayer3", "");
		player4.text = PlayerPrefs.GetString("LBPlayer4", "");
		player5.text = PlayerPrefs.GetString("LBPlayer5", "");
		player6.text = PlayerPrefs.GetString("LBPlayer6", "");
		mph1.text = PlayerPrefs.GetString("LBMPH1", "");
		mph2.text = PlayerPrefs.GetString("LBMPH2", "");
		mph3.text = PlayerPrefs.GetString("LBMPH3", "");
		mph4.text = PlayerPrefs.GetString("LBMPH4", "");
		mph5.text = PlayerPrefs.GetString("LBMPH5", "");
		mph6.text = PlayerPrefs.GetString("LBMPH6", "");
		score1.text = PlayerPrefs.GetString("LBScore1", "");
		score2.text = PlayerPrefs.GetString("LBScore2", "");
		score3.text = PlayerPrefs.GetString("LBScore3", "");
		score4.text = PlayerPrefs.GetString("LBScore4", "");
		score5.text = PlayerPrefs.GetString("LBScore5", "");
		score6.text = PlayerPrefs.GetString("LBScore6", "");

		string temp=PlayerPrefs.GetString("Photo1" ,"");		
		int width=PlayerPrefs.GetInt("Photo1_w", 0);
		int height=PlayerPrefs.GetInt("Photo1_h", 0);		
		byte[] byteArray= Convert.FromBase64String(temp);		
		Texture2D tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo1.renderer.material.mainTexture = tex;
		
		temp=PlayerPrefs.GetString("Photo2" ,"");		
		width=PlayerPrefs.GetInt("Photo2_w", 0);
		height=PlayerPrefs.GetInt("Photo2_h", 0);		
		byteArray= Convert.FromBase64String(temp);		
		tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo2.renderer.material.mainTexture = tex;
		
		temp=PlayerPrefs.GetString("Photo3" ,"");		
		width=PlayerPrefs.GetInt("Photo3_w", 0);
		height=PlayerPrefs.GetInt("Photo3_h", 0);		
		byteArray= Convert.FromBase64String(temp);		
		tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo3.renderer.material.mainTexture = tex;
		
		temp=PlayerPrefs.GetString("Photo4" ,"");		
		width=PlayerPrefs.GetInt("Photo4_w", 0);
		height=PlayerPrefs.GetInt("Photo4_h", 0);		
		byteArray= Convert.FromBase64String(temp);		
		tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo4.renderer.material.mainTexture = tex;
		
		temp=PlayerPrefs.GetString("Photo5" ,"");		
		width=PlayerPrefs.GetInt("Photo5_w", 0);
		height=PlayerPrefs.GetInt("Photo5_h", 0);		
		byteArray= Convert.FromBase64String(temp);		
		tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo5.renderer.material.mainTexture = tex;
		
		temp=PlayerPrefs.GetString("Photo6" ,"");		
		width=PlayerPrefs.GetInt("Photo6_w", 0);
		height=PlayerPrefs.GetInt("Photo6_h", 0);		
		byteArray= Convert.FromBase64String(temp);		
		tex = new Texture2D(width,height);		
		tex.LoadImage(byteArray);
		photo6.renderer.material.mainTexture = tex;
	}

	void ResetID()
	{
		// The name of the key must include a valid root. 
		const string userRoot = "HKEY_CURRENT_USER";
		const string subkey = "RegistryValueID";
		const string keyName = userRoot + "\\" + subkey;
		
		// An int value can be stored without specifying the 
		// registry data type, but long values will be stored 
		// as strings unless you specify the type. Note that 
		// the int is stored in the default name/value 
		// pair.
		Registry.SetValue(keyName, "ValueID", "-1");
	}

	void CargaData()
	{
		WWWForm form = new WWWForm();
		const string userRoot = "HKEY_CURRENT_USER";
		const string subkey = "RegistryValueID";
		const string keyName = userRoot + "\\" + subkey;

		string testID = (string)Registry.GetValue(keyName, "ValueID", "-1");
		AnimatorController2.idPlayer = testID;

		if(!Configuration.isAssistedMode)
		{
			form.AddField("id",AnimatorController2.idPlayer);
		}
		else
		{
			form.AddField("id", -1);
		}

		WWW preg = new WWW(Configuration.currentServer + "/adm/getRanksById", form);
		StartCoroutine(WaitGetRankById(preg));
	}

	IEnumerator WaitGetRankById(WWW www)
	{
		yield return www;		
		// check for errors
		if (www.error == null)
		{
			RString = www.text;
			string [] split = RString.Split('|');
			int nVuelta = 1;
			for (int i = 1; i < split.Length; i++) 
			{
				string [] subSplit = split[i].Split('~');
				if(nVuelta == 1)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date1.text = date;
					rank1.text = subSplit[1];
					player1.text = subSplit[2];
					mph1.text = subSplit[3];
                    print("Aqui pongo score");
					int score = Convert.ToInt32(subSplit[4]);
					if(score == 0)
						score1.text = "0";
					else
						score1.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo1));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo1, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo1.renderer.material.mainTexture = tex;
					}

					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate1", date1.text);
						PlayerPrefs.SetString("LBRank1", rank1.text);
						PlayerPrefs.SetString("LBPlayer1", player1.text);
						PlayerPrefs.SetString("LBMPH1", mph1.text);
						PlayerPrefs.SetString("LBScore1", score1.text);
					}

					nVuelta++;
				}
				else if(nVuelta == 2)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date2.text = date;
					rank2.text = subSplit[1];
					player2.text = subSplit[2];
					mph2.text = subSplit[3];
					int score = Convert.ToInt32(subSplit[4]);
					if(score == 0)
						score2.text = "0";
					else
						score2.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo2));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo2, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo2.renderer.material.mainTexture = tex;
					}

					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate2", date2.text);
						PlayerPrefs.SetString("LBRank2", rank2.text);
						PlayerPrefs.SetString("LBPlayer2", player2.text);
						PlayerPrefs.SetString("LBMPH2", mph2.text);
						PlayerPrefs.SetString("LBScore2", score2.text);
					}

					nVuelta++;
				}
				else if(nVuelta == 3)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date3.text = date;
					rank3.text = subSplit[1];
					player3.text = subSplit[2];
					mph3.text = subSplit[3];
					int score = Convert.ToInt32(subSplit[4]);
					if(score == 0)
						score3.text = "0";
					else
						score3.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo3));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo3, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo3.renderer.material.mainTexture = tex;
					}
					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate3", date3.text);
						PlayerPrefs.SetString("LBRank3", rank3.text);
						PlayerPrefs.SetString("LBPlayer3", player3.text);
						PlayerPrefs.SetString("LBMPH3", mph3.text);
						PlayerPrefs.SetString("LBScore3", score3.text);
					}

					nVuelta++;
				}
				else if(nVuelta == 4)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date4.text = date;
					rank4.text = subSplit[1];
					player4.text = subSplit[2];
					mph4.text = subSplit[3];
					int score = Convert.ToInt32(subSplit[4]);
                    print("Otro score");
					if(score == 0)
						score4.text = "0";
					else
						score4.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo4));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo4, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo4.renderer.material.mainTexture = tex;
					}
					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate4", date4.text);
						PlayerPrefs.SetString("LBRank4", rank4.text);
						PlayerPrefs.SetString("LBPlayer4", player4.text);
						PlayerPrefs.SetString("LBMPH4", mph4.text);
						PlayerPrefs.SetString("LBScore4", score4.text);
					}

					nVuelta++;
				}
				else if(nVuelta == 5)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date5.text = date;
					rank5.text = subSplit[1];
					player5.text = subSplit[2];
					mph5.text = subSplit[3];
                    print("Score 5");
					int score = Convert.ToInt32(subSplit[4]);
					if(score == 0)
						score5.text = "0";
					else
						score5.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo5));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo5, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo5.renderer.material.mainTexture = tex;
					}

					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate5", date5.text);
						PlayerPrefs.SetString("LBRank5", rank5.text);
						PlayerPrefs.SetString("LBPlayer5", player5.text);
						PlayerPrefs.SetString("LBMPH5", mph5.text);
						PlayerPrefs.SetString("LBScore5", score5.text);
					}

					nVuelta++;
				}
				else if(nVuelta == 6)
				{
					DateTime dt = Convert.ToDateTime(subSplit[0]);
					String.Format("{0:MM/dd/yyyy}", dt);
					string date = dt.ToString().Split(' ')[0];
					date6.text = date;
					rank6.text = subSplit[1];
					player6.text = subSplit[2];
					mph6.text = subSplit[3];
					int score = Convert.ToInt32(subSplit[4]);
					if(score == 0)
						score6.text = "0";
					else
						score6.text = score.ToString("#,#", CultureInfo.InvariantCulture);
					if(subSplit[5] != "")
					{
						if(!Configuration.isAssistedMode)
						{
							StartCoroutine(Photos(subSplit[5], photo6));
						}
						else
						{
							StartCoroutine(Photos(subSplit[5], photo6, nVuelta));
						}
					}
					else
					{
						Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
						photo6.renderer.material.mainTexture = tex;
					}

					if(Configuration.isAssistedMode)
					{
						PlayerPrefs.SetString("LBDate6", date6.text);
						PlayerPrefs.SetString("LBRank6", rank6.text);
						PlayerPrefs.SetString("LBPlayer6", player6.text);
						PlayerPrefs.SetString("LBMPH6", mph6.text);
						PlayerPrefs.SetString("LBScore6", score6.text);
					}

					nVuelta++;
				}
			}
		}
		else 
		{
			RString = www.error;
		}
	}

	IEnumerator Photos(string name, GameObject obj) 
	{
		WWW www = new WWW(Configuration.currentServer + "/upload/" + name);
		yield return www;
		if (www.error == null)
			obj.renderer.material.mainTexture = www.texture;
		else
		{
			Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
			obj.renderer.material.mainTexture = tex;
		}
	}

	IEnumerator Photos(string name, GameObject obj, int n) 
	{
		WWW www = new WWW(Configuration.currentServer + "/upload/" + name);
		print (www.url);
		yield return www;

		if (www.error == null)
		{
			obj.renderer.material.mainTexture = www.texture;
			byte[] byteArray = www.texture.EncodeToPNG();
			string temp = Convert.ToBase64String(byteArray);
			PlayerPrefs.SetString("Photo" + n.ToString(),temp);      /// save it to file if u want.
			PlayerPrefs.SetInt("Photo" + n.ToString() + "_w",www.texture.width);
			PlayerPrefs.SetInt("Photo" + n.ToString() + "_h",www.texture.height);
		}
		else
		{
			Texture2D tex = (Texture2D)Resources.Load("AvatarPhoto", typeof(Texture2D));
			obj.renderer.material.mainTexture = tex;
		}
	}
}
