﻿using UnityEngine;
using System.Collections;

public class ForceBallon : MonoBehaviour
{
	//Select Animation
	public int whichAnim = 0;

	public Timer myTimer;

	public GameObject objectToCast;
	public GameObject loseSprite1;
	public GameObject loseSprite2;
	//Semaforo
	public GameObject shineShot1;
	public GameObject shineShot2;
	public GameObject shineShot3;
//	public GameObject shineShot4;
//	public GameObject shineShot5;
	public GameObject falloShot1;
	public GameObject falloShot2;
	public GameObject falloShot3;
//	public GameObject falloShot4;
//	public GameObject falloShot5;
	public GameObject goalShot1;
	public GameObject goalShot2;
	public GameObject goalShot3;
//	public GameObject goalShot4;
//	public GameObject goalShot5;
	public GameObject bgScorePractice;
	public GameObject bgScoreGameTime;

	private float finalX;
	private float finalY;
	private float finalZ;
	private AudioSource[] sounds;
	private AudioSource kick;
	private AudioSource fail;
	private AudioSource indicator;
	private AudioSource messageAppear;
	private AudioSource messageDisappear;
	private GameObject notGoal;
	private Vector4 color;
	private Vector4 colorGoal1;
	private Vector4 colorGoal2;
	private Vector4 colorGoal3;
//	private Vector4 colorGoal4;
//	private Vector4 colorGoal5;

	float progresstimeMessage = -1.0f;
	float progressfallo1 = 0.0f;
	float progressfallo2 = 0.0f;
	float progressfallo3 = 0.0f;
//	float progressfallo4 = 0.0f;
//	float progressfallo5 = 0.0f;

	Launcher mLauncher;

	void Awake()
	{
		mLauncher = GameObject.FindObjectOfType<Launcher>();
	}

	void Start ()
	{
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		shineShot1.renderer.material.color = color;
		shineShot2.renderer.material.color = color;
		shineShot3.renderer.material.color = color;
//		shineShot4.renderer.material.color = color;
//		shineShot5.renderer.material.color = color;
		if(AnimatorController2.nTiro == 1)
		{
			falloShot1.renderer.material.color = color;
			falloShot2.renderer.material.color = color;
			falloShot3.renderer.material.color = color;
//			falloShot4.renderer.material.color = color;
//			falloShot5.renderer.material.color = color;
			colorGoal1.x = 0;
			colorGoal1.y = 1;
			colorGoal1.z = 1;
			colorGoal1.w = 1;
			//goalShot1.renderer.material.color = colorGoal;
			colorGoal2.x = 0;
			colorGoal2.y = 1;
			colorGoal2.z = 1;
			colorGoal2.w = 1;
			//goalShot2.renderer.material.color = colorGoal;
			colorGoal3.x = 0;
			colorGoal3.y = 1;
			colorGoal3.z = 1;
			colorGoal3.w = 1;
			//goalShot3.renderer.material.color = colorGoal;
//			colorGoal4.x = 0;
//			colorGoal4.y = 1;
//			colorGoal4.z = 1;
//			colorGoal4.w = 1;
			//goalShot4.renderer.material.color = colorGoal;
//			colorGoal5.x = 0;
//			colorGoal5.y = 1;
//			colorGoal5.z = 1;
//			colorGoal5.w = 1;
			//goalShot5.renderer.material.color = colorGoal;
		}
		else if(AnimatorController2.nTiro == 2)
		{
			if(AnimatorController2.resultTiro1 == 1)
			{
				falloShot1.renderer.material.color = color;
				colorGoal1.x = 1;
				colorGoal1.y = 1;
				colorGoal1.z = 1;
				colorGoal1.w = 1;
				//normalShot1.renderer.material.color = color;
			}
			else
			{
				progressfallo1 = 1.0f;
				//normalShot1.renderer.material.color = color;
				colorGoal1.x = 0;
				colorGoal1.y = 1;
				colorGoal1.z = 1;
				colorGoal1.w = 1;
				//goalShot1.renderer.material.color = colorGoal;
			}
			falloShot2.renderer.material.color = color;
			falloShot3.renderer.material.color = color;
//			falloShot4.renderer.material.color = color;
//			falloShot5.renderer.material.color = color;
			colorGoal2.x = 0;
			colorGoal2.y = 1;
			colorGoal2.z = 1;
			colorGoal2.w = 1;
			//goalShot2.renderer.material.color = colorGoal;
			colorGoal3.x = 0;
			colorGoal3.y = 1;
			colorGoal3.z = 1;
			colorGoal3.w = 1;
			//goalShot3.renderer.material.color = colorGoal;
//			colorGoal4.x = 0;
//			colorGoal4.y = 1;
//			colorGoal4.z = 1;
//			colorGoal4.w = 1;
			//goalShot4.renderer.material.color = colorGoal;
//			colorGoal5.x = 0;
//			colorGoal5.y = 1;
//			colorGoal5.z = 1;
//			colorGoal5.w = 1;
			//goalShot5.renderer.material.color = colorGoal;
		}
		else if(AnimatorController2.nTiro == 3)
		{
			if(AnimatorController2.resultTiro1 == 1)
			{
				falloShot1.renderer.material.color = color;
				colorGoal1.x = 1;
				colorGoal1.y = 1;
				colorGoal1.z = 1;
				colorGoal1.w = 1;
				//normalShot1.renderer.material.color = color;
			}
			else
			{
				progressfallo1 = 1.0f;
				//normalShot1.renderer.material.color = color;
				colorGoal1.x = 0;
				colorGoal1.y = 1;
				colorGoal1.z = 1;
				colorGoal1.w = 1;
				//goalShot1.renderer.material.color = colorGoal;
			}
			if(AnimatorController2.resultTiro2 == 1)
			{
				falloShot2.renderer.material.color = color;
				colorGoal2.x = 1;
				colorGoal2.y = 1;
				colorGoal2.z = 1;
				colorGoal2.w = 1;
				//normalShot2.renderer.material.color = color;
			}
			else
			{
				progressfallo2 = 1.0f;
				//normalShot2.renderer.material.color = color;
				colorGoal2.x = 0;
				colorGoal2.y = 1;
				colorGoal2.z = 1;
				colorGoal2.w = 1;
				//goalShot2.renderer.material.color = colorGoal;
			}
			falloShot3.renderer.material.color = color;
//			falloShot4.renderer.material.color = color;
//			falloShot5.renderer.material.color = color;
			colorGoal3.x = 0;
			colorGoal3.y = 1;
			colorGoal3.z = 1;
			colorGoal3.w = 1;
			//goalShot3.renderer.material.color = colorGoal;
//			colorGoal4.x = 0;
//			colorGoal4.y = 1;
//			colorGoal4.z = 1;
//			colorGoal4.w = 1;
			//goalShot4.renderer.material.color = colorGoal;
//			colorGoal5.x = 0;
//			colorGoal5.y = 1;
//			colorGoal5.z = 1;
//			colorGoal5.w = 1;
			//goalShot5.renderer.material.color = colorGoal;
		}

		GameObject.Find ("Kinect");
		notGoal = GameObject.Find ("NotGoal");
		notGoal.SetActive(false);
		Kinect kinect = GameObject.Find ("Kinect").GetComponent<Kinect>();
		kinect.soccerBall = gameObject;
		sounds = gameObject.GetComponents<AudioSource> ();
		kick = sounds[0];
		fail = sounds[2];
		indicator = sounds[5];
		messageAppear = sounds[6];
		messageDisappear = sounds[7];

		if(Timer.sceneTraining)
		{
			bgScorePractice.SetActive(true);
			bgScoreGameTime.SetActive(false);
		}
		else
		{
			bgScorePractice.SetActive(false);
			bgScoreGameTime.SetActive(true);
		}
	}

	// Update is called once per frame
	void Update ()
	{
		//print (Time.time);
		if (Kinect.shot) 
		{
//			if(AnimatorController2.nTiro == 3)
//			{
//				int consecutive = 0;
//				if(PlayerPrefs.HasKey("Picture Consecutive"))
//				{
//					int oldConsecutive = PlayerPrefs.GetInt("Picture Consecutive");
//					consecutive = oldConsecutive + 1;
//					PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				}
//				else 
//					PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				AnimatorController2.photo2 = System.Environment.MachineName + "_" + consecutive;
//				Kinect.TakePicture (consecutive);
//			}
			//print ("empieza tiro: " + Time.time);
			Kinect.shot = false;
			kick.Play();
			//Calculate the time
			/*Para calcular la cantidad por la que se dividira la fuerza en Z se hace un ejemplo debugueando y se calcula el tiempo en ese ejemplo,
			  usando ese tiempo se calcula la velocidad que llevaba el balon: la fuerza sobre un numero dara como resultado esa velocidad*/
			AnimatorController2.time = 11/(Kinect.pForce.z/52);
            //Obtain the values when the ballo pass the goal line
            //finalX = gameObject.transform.position.x + ((Kinect.pForce.x/52) * AnimatorController2.time);
            finalX = Random.Range(-3.52f, 3.52f);

			finalY = gameObject.transform.position.y + ((Kinect.pForce.y/52) * AnimatorController2.time) + (0.5f * Physics.gravity.y * AnimatorController2.time * AnimatorController2.time);
			finalZ = gameObject.transform.position.z + ((Kinect.pForce.z/52) * AnimatorController2.time);
			//Asign that position to a empty object
			Vector3 positionObjectToCast;
			positionObjectToCast.x = finalX;
			if(finalY > 0)
				positionObjectToCast.y = finalY;
			else
				positionObjectToCast.y = 0;
			positionObjectToCast.z = finalZ;
			objectToCast.transform.position = positionObjectToCast;
			//Coroutine for the animation texture of win or lose
			Invoke("LoseOrWin", 2);
			//Coroutine for give the ballon force 0
			Invoke("ForceZero", 2);
		}

		//loseSprite1.renderer.material.SetFloat("_Progress", progresstimeMessage);
		loseSprite2.renderer.material.SetFloat("_Progress", progresstimeMessage);
		falloShot1.renderer.material.SetFloat("_Progress", progressfallo1);
		falloShot2.renderer.material.SetFloat("_Progress", progressfallo2);
		falloShot3.renderer.material.SetFloat("_Progress", progressfallo3);
//		falloShot4.renderer.material.SetFloat("_Progress", progressfallo4);
//		falloShot5.renderer.material.SetFloat("_Progress", progressfallo5);
		goalShot1.renderer.material.SetColor ("_Color", colorGoal1);
		goalShot2.renderer.material.SetColor ("_Color", colorGoal2);
		goalShot3.renderer.material.SetColor ("_Color", colorGoal3);
//		goalShot4.renderer.material.SetColor ("_Color", colorGoal4);
//		goalShot5.renderer.material.SetColor ("_Color", colorGoal5);
	}

	void TweenedtimeColorGoal1(float val)
	{
		colorGoal1.x = val;
	}
	
	void TweenedtimeColorGoal2(float val)
	{
		colorGoal2.x = val;
	}
	
	void TweenedtimeColorGoal3(float val)
	{
		colorGoal3.x = val;
	}
	
//	void TweenedtimeColorGoal4(float val)
//	{
//		colorGoal4.x = val;
//	}
	
//	void TweenedtimeColorGoal5(float val)
//	{
//		colorGoal5.x = val;
//	}

	void LoseOrWin()
	{
		//If player goal
		if (AnimatorController2.isWin == 0)
		{
			indicator.Play();
            //Puntaje.puntaje += 100 * Bonus.bonus * Velocimetro.intVelocity;print();
            Puntaje.puntaje += 1;
			if(!Timer.sceneTraining)
			{
				AnimatorController2.mphAverage += Velocimetro.intVelocity;
				//print ("avg = " + AnimatorController2.mphAverage);
			}

			if(Bonus.bonus < 5)
			{
				Bonus.bonus++;
				Bonus.textBonusChanged = true;
			}

//			if (AnimatorController2.nTiro == 5) 				
//			{
//				AnimatorController2.resultTiro5 = 1;
//				//iTween.FadeTo (normalShot5, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
//				iTween.ValueTo(gameObject, iTween.Hash("from", colorGoal5.x, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeColorGoal5"));
//				//iTween.FadeTo (goalShot5, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
//				iTween.FadeTo (shineShot5, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "delay", 1.2f));
//				iTween.FadeTo (shineShot5, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.7f));
//			}
//			else if (AnimatorController2.nTiro == 4) 	
//			{
//				AnimatorController2.resultTiro4 = 1;
//				//iTween.FadeTo (normalShot4, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
//				iTween.ValueTo(gameObject, iTween.Hash("from", colorGoal4.x, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeColorGoal4"));
//				//iTween.FadeTo (goalShot4, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
//				iTween.FadeTo (shineShot4, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "delay", 1.2f));
//				iTween.FadeTo (shineShot4, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.7f));
//			}
//			else
				if (AnimatorController2.nTiro >= 3)
			{
				AnimatorController2.resultTiro3 = 1;
				//iTween.FadeTo (normalShot3, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", colorGoal3.x, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeColorGoal3"));
				//iTween.FadeTo (goalShot3, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.FadeTo (shineShot3, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "delay", 1.2f));
				iTween.FadeTo (shineShot3, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.7f));
			}
			else if (AnimatorController2.nTiro == 2)
			{
				AnimatorController2.resultTiro2 = 1;
				//iTween.FadeTo (normalShot2, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				//iTween.FadeTo (goalShot2, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", colorGoal2.x, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeColorGoal2"));
				iTween.FadeTo (shineShot2, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "delay", 1.2f));
				iTween.FadeTo (shineShot2, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.7f));
			}
			else if (AnimatorController2.nTiro == 1)
			{
				AnimatorController2.resultTiro1 = 1;
				//iTween.FadeTo (normalShot1, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				//iTween.FadeTo (goalShot1, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", colorGoal1.x, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeColorGoal1"));
				iTween.FadeTo (shineShot1, iTween.Hash ("alpha", 1.0f, "time", 0.5f, "delay", 1.2f));
				iTween.FadeTo (shineShot1, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.7f));
			}
//			if(AnimatorController2.nTiro == 3)
//			{
//				int consecutive = 0;
//				if(PlayerPrefs.HasKey("Picture Consecutive"))
//				{
//					int oldConsecutive = PlayerPrefs.GetInt("Picture Consecutive");
//					consecutive = oldConsecutive + 1;
//					PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				}
//				else 
//					PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				AnimatorController2.photo3 = System.Environment.MachineName + "_" + consecutive;
//				Kinect.TakePicture (consecutive);
//			}
		} 
		//If fails
		else 
		{
			fail.Play();
			if(!Timer.sceneTraining)
			{
				AnimatorController2.mphAverage += Velocimetro.intVelocity;
				//print ("avg = " + AnimatorController2.mphAverage);
			}
			AnimatorController2.publicLose = true;
			messageAppear.Play();
			Invoke("Dissapear", 1.5f);
			
			iTween.MoveBy(loseSprite1, iTween.Hash("EaseType", "easeOutSine", "x", -3.9, "time", 1.0f));
			iTween.MoveBy(loseSprite1, iTween.Hash("EaseType", "easeInSine", "x", 3.9, "time", 1.0f, "Delay", 1.5f));
			iTween.ValueTo(gameObject, iTween.Hash("from", progresstimeMessage, "to", 2.0f, "time", 1.0f, "delay", 0.7f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeMessageValue"));
			

			Kinect.numberCometShot = 1;
			Kinect.numberCometOuterShot = 1;

			if(Bonus.bonus > 1)
				Bonus.textBonusChanged = true;

			Bonus.bonus = 1;

			if(mLauncher != null)
			{
				StartCoroutine(mLauncher.Execute(5.0f));
			}
			else
			{
				Debug.Log("ForceBallon: Launcher is NULL");
			}


//			Invoke ("Restart", 5);//<---- CHANGE WITH LAUNCHER
//			if (AnimatorController2.nTiro == 5) 	
//			{
//				AnimatorController2.resultTiro5 = 0;
//				//iTween.FadeTo (normalShot5, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
//				iTween.ValueTo(gameObject, iTween.Hash("from", progressfallo5, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeFalloGoal5"));
//				//iTween.FadeTo (falloShot5, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
//			}
//			else if (AnimatorController2.nTiro == 4) 	
//			{
//				AnimatorController2.resultTiro4 = 0;
//				//iTween.FadeTo (normalShot4, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
//				//iTween.FadeTo (falloShot4, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
//				iTween.ValueTo(gameObject, iTween.Hash("from", progressfallo4, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeFalloGoal4"));
//			}
//			else
			if (AnimatorController2.nTiro >= 3)
			{
				AnimatorController2.resultTiro3 = 0;
				//iTween.FadeTo (normalShot3, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				//iTween.FadeTo (falloShot3, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", progressfallo3, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeFalloGoal3"));
			}
			else if (AnimatorController2.nTiro == 2)
			{
				AnimatorController2.resultTiro2 = 0;
				//iTween.FadeTo (normalShot2, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				//iTween.FadeTo (falloShot2, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", progressfallo2, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeFalloGoal2"));
			}
			else if (AnimatorController2.nTiro == 1)
			{
				AnimatorController2.resultTiro1 = 0;
				//iTween.FadeTo (normalShot1, iTween.Hash ("alpha", 0.0f, "time", 1.0f, "delay", .2));
				//iTween.FadeTo (falloShot1, iTween.Hash ("alpha", 1.0f, "time", 1.0f, "delay", .2));
				iTween.ValueTo(gameObject, iTween.Hash("from", progressfallo1, "to", 1.0f, "time", 1.0f, "delay", 0.2f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeFalloGoal1"));
			}
//			if(AnimatorController2.nTiro == 3)
//			{
//				int consecutive = 0;
//				if(PlayerPrefs.HasKey("Picture Consecutive"))
//				{
//					int oldConsecutive = PlayerPrefs.GetInt("Picture Consecutive");
//					consecutive = oldConsecutive + 1;
//					PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				}
//				else PlayerPrefs.SetInt("Picture Consecutive", consecutive);
//				AnimatorController2.photo3 = System.Environment.MachineName + "_" + consecutive;
//				Kinect.TakePicture (consecutive);
//			}
		}

        if (AnimatorController2.nTiro < XMLConfiguration.GetSetting<int>("KicksPerRound", 3)) // End at third turn
        {

            AnimatorController2.nTiro++;
            Puntaje.tiros++;
        }
        else
        {
            AnimatorController2.nTiro = 1;
        }
	}

	void Dissapear()
	{
		messageDisappear.Play ();
	}

	void TweenedtimeMessageValue(float val)
	{
		progresstimeMessage = val;
	}

	void TweenedtimeFalloGoal1(float val)
	{
		progressfallo1 = val;
	}

	void TweenedtimeFalloGoal2(float val)
	{
		progressfallo2 = val;
	}

	void TweenedtimeFalloGoal3(float val)
	{
		progressfallo3 = val;
	}


	void ForceZero()
	{
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
		notGoal.SetActive(true);
		AnimatorController2.activeAtaj = -1;
	}

	void OnApplicationQuit()
	{
		//SampleScript.video.Stop();
	}
}