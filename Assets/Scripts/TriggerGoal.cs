﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerGoal : MonoBehaviour 
{
	List<int> choices = new List<int>();
	int numberAnimation;
	void OnTriggerEnter(Collider other) 
	{
		if(System.Int32.TryParse(other.gameObject.name, out numberAnimation))
			choices.Add (numberAnimation);
        //		if(other.gameObject.name == "TriggerDerechaAbajo")
        //			choices.Add(1);
        //		if(other.gameObject.name == "TriggerContrapieDerecha")
        //			choices.Add(2);
        //		if(other.gameObject.name == "TriggerEnMedioCercaDerecha")
        //			choices.Add(3);
        //		if(other.gameObject.name == "TriggerEnMedioFintaDerecha")
        //			choices.Add(4);
        //		if(other.gameObject.name == "TriggerUnaManoDerecha")
        //			choices.Add(1);
        //		if(other.gameObject.name == "TriggerDerechaArriba")
        //			choices.Add(2);
        //		if(other.gameObject.name == "TriggerManoArribaDerecha")
        //			choices.Add(3);
        //		if(other.gameObject.name == "TriggerCentroArribaEquivocadoDerecha")
        //			choices.Add(4);
        //		if(other.gameObject.name == "TriggerDerechaEquivocadoDerecha")
        //			choices.Add(1);
        //		if(other.gameObject.name == "TriggerEnMedioEquivocadoDerecha")
        //			choices.Add(2);
        //		if(other.gameObject.name == "TriggerDerechaAbajoEquivocado")
        //			choices.Add(2);
        //		if(other.gameObject.name == "TriggerCentroAbajoD")
        //			choices.Add(4);
        //		if(other.gameObject.name == "TriggerCentroAbajoI")
        //			choices.Add(1);
        //		if(other.gameObject.name == "TriggerCentroArriba")
        //			choices.Add(2);
        //		if(other.gameObject.name == "TriggerIzquierdaEquivocadoCentro")
        //			choices.Add(3);
        //		if(other.gameObject.name == "TriggerDerechaEquivocadoCentro")
        //			choices.Add(4);
        //		if(other.gameObject.name == "TriggerCentroTunelAbajoD")
        //			choices.Add(1);
        //		if(other.gameObject.name == "TriggerCentroTunelAbajoI")
        //			choices.Add(2);
        //		if(other.gameObject.name != "ColliderFloor")
        //print("Atajada antes " + AnimatorController2.activeAtaj);
		if(choices.Count>0)
			AnimatorController2.activeAtaj = choices[Random.Range(0,choices.Count)];
        //print("Activo atajada " + AnimatorController2.activeAtaj);
    }
}