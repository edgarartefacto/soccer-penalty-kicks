﻿using UnityEngine;
using System.Collections;

public class FPSSinGUI : MonoBehaviour {

	public  float updateInterval = 0.5F;
	
	private float accum = 0;
	private int frames = 0;
	private float timeleft;
	
	int fps;
	string format;
	
	
	public int smallestFrame;
	public int counterFrames60mas;
	public int counterFrames60;	
	public int counterFrames50;	
	public int counterFrames40;	
	public int counterFrames30;
	public int counterFrames20;	
	public int counterFrames10;	

	public int counterTotalFrames;
	
	void Start()
	{
		smallestFrame = 60;
		counterTotalFrames = 0;

		DontDestroyOnLoad(gameObject);

		timeleft = updateInterval;  
	}
	
	void Update()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale/Time.deltaTime;
		++frames;
		
		if( timeleft <= 0.0 ){
			fps = (int)(accum/frames)+1;
			//format = System.String.Format("{0:F2} FPS",fps);

			if(fps < 10)
				counterFrames10++;
			else if(fps < 20)
				counterFrames20++;
			else if(fps < 30)
				counterFrames30++;
			else if(fps < 40)
				counterFrames40++;
			else if(fps < 50)
				counterFrames50++;
			else if(fps < 60)
				counterFrames60++;
			else
				counterFrames60mas++;

			counterTotalFrames++;

			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
			
			if(fps < smallestFrame)
				smallestFrame = fps;


		}
		/*
		try {
			GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().BSM.TextMessageFinal.text = activeFinalFPS();
				} catch (System.Exception ex) {
			
				}
				*/

	}
	
	public string activeFinalFPS()
	{
		/*
		return "|>60:"+counterFrames60mas.ToString()
				+"\n|<60:"+counterFrames60.ToString()
				+"\n|<50:"+counterFrames50.ToString()
				+"\n|<40:"+counterFrames40.ToString()
				+"\n|<30:"+counterFrames30.ToString()
				+"\n|<20:"+counterFrames20.ToString()
				+"\n|<10:"+counterFrames10.ToString()
				+"\n|min"+smallestFrame.ToString();
				*/
		float f1 = (100f*counterFrames60mas)/counterTotalFrames;
		float f2 = (100f*counterFrames60)/counterTotalFrames;
		float f3 = (100f*counterFrames50)/counterTotalFrames;
		float f4 = (100f*counterFrames40)/counterTotalFrames;
		float f5 = (100f*counterFrames30)/counterTotalFrames;
		float f6 = (100f*counterFrames20)/counterTotalFrames;
		float f7 = (100f*counterFrames10)/counterTotalFrames;

		//return "";


		return  "|>60: "+f1.ToString("0.00")+"%"
				+"\n|<60: "+f2.ToString("0.00")+"%"
				+"\n|<50: "+f3.ToString("0.00")+"%"
				+"\n|<40: "+f4.ToString("0.00")+"%"
				+"\n|<30: "+f5.ToString("0.00")+"%"
				+"\n|<20: "+f6.ToString("0.00")+"%"
				+"\n|<10: "+f7.ToString("0.00")+"%"
				+"\n|min: "+smallestFrame.ToString();
				
	}
	
	public void resetFPS()
	{
		//GeneralHelper.Log("Se resetean el FPS");
		counterFrames10=0;
		counterFrames20=0;
		counterFrames30=0;
		counterFrames40=0;
		counterFrames50=0;
		counterFrames60=0;
		counterFrames60mas=0;
		smallestFrame = 60;

		counterTotalFrames = 0;
	}
}
