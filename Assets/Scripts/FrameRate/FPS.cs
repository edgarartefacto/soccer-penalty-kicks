using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {

    public static FPS singleton;

	public  float updateInterval = 0.5F;
 
	private float accum = 0;
	private int frames = 0;
	private float timeleft;

	int fps;
	string format;
	 
	bool setVisible = false;
	int smallestFrame;
	int counterFrames30;
	int counterFrames20;	

	void Start()
	{

        if (FPS.singleton == null)
        {
            FPS.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        smallestFrame = 60;
	    if( !guiText )
	    {
	        Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
	        enabled = false;
	        return;
	    }
		else{
			guiText.fontSize = 30;
		}
	    timeleft = updateInterval;  
	}
	 
	void Update()
	{
		if(Application.loadedLevelName == "KeyBoard")
			setVisible = false;
		if(Input.GetKeyUp(KeyCode.Alpha1) && Application.loadedLevelName != "KeyBoard")
			setVisible = !setVisible;
	    timeleft -= Time.deltaTime;
	    accum += Time.timeScale/Time.deltaTime;
	    ++frames;
	 
	    if( timeleft <= 0.0 ){
			fps = (int)(accum/frames)+1;
			//format = System.String.Format("{0:F2} FPS",fps);
			//guiText.text = fps.ToStringLookup(); //format;
		 
			if(fps < 20)
				counterFrames20++;
			else if(fps < 30)
				counterFrames30++;

			if(fps < 30)
			{
				guiText.material.color = Color.yellow;
			}
			else if(fps < 10)
			{
				guiText.material.color = Color.red;
			}
			else
				guiText.material.color = Color.green;

		        timeleft = updateInterval;
		        accum = 0.0F;
		        frames = 0;

//			if(fps < smallestFrame)
//				smallestFrame = fps;
			if(setVisible)
				guiText.text = fps.ToString();
			else
				guiText.text = "";
	    }
	}

	public string activeFinalFPS()
	{
	//	guiText.text = counterFrames30.ToString()+"|"+counterFrames20.ToString()+"|"+smallestFrame.ToString();

		return counterFrames30.ToString()+"|"+counterFrames20.ToString()+"|"+smallestFrame.ToString();
	}

	public void resetFPS()
	{
		counterFrames20=0;
		counterFrames30=0;
		smallestFrame = 60;
	}
}
