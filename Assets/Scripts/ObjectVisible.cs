﻿using UnityEngine;
using System.Collections;

public class ObjectVisible : MonoBehaviour 
{
	void Start ()
	{
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		kickNowSprite.renderer.material.color = color;
		practiceRoundSprite.renderer.material.color = color;
		sounds = gameObject.GetComponents<AudioSource> ();
		silb = sounds [1];
		if(AnimatorController2.nTiro > 1)
			Invoke ("StartSilbato", 0f);
		else
			Invoke ("StartSilbato", 9);

		if(Timer.sceneTraining)
		{
			shooterStatus = Status.NO_CALLED;
			MessagePractice();
		}
		else
		{
			GameTimeMessage();
		}
    }

	void MessagePractice()
	{
		if(AnimatorController2.nTiro > 1)
		{
			iTween.MoveBy(practiceRoundSprite, iTween.Hash("EaseType", "easeOutSine", "y", -0.7, "time", 0.5f));
			iTween.MoveBy(practiceRoundSprite, iTween.Hash("EaseType", "easeInSine", "y", 0.7, "time", 0.5f, "Delay", 11.5));
			iTween.FadeTo (practiceRoundSprite, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
			iTween.FadeTo (practiceRoundSprite, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 11.5));
		}
		else
		{
			iTween.MoveBy(practiceRoundSprite, iTween.Hash("EaseType", "easeOutSine", "y", -0.7, "time", 0.5f));
			iTween.MoveBy(practiceRoundSprite, iTween.Hash("EaseType", "easeInSine", "y", 0.7, "time", 0.5f, "Delay", 19));
			iTween.FadeTo (practiceRoundSprite, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
			iTween.FadeTo (practiceRoundSprite, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 19));
		}
	}

	void GameTimeMessage()
	{
		if(AnimatorController2.nTiro > 1)
		{
			iTween.MoveBy(gameTimeSprite, iTween.Hash("EaseType", "easeOutSine", "y", -0.7, "time", 0.5f));
			iTween.MoveBy(gameTimeSprite, iTween.Hash("EaseType", "easeInSine", "y", 0.7, "time", 0.5f, "Delay", 11.5));
			iTween.FadeTo (gameTimeSprite, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
			iTween.FadeTo (gameTimeSprite, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 11.5));
		}
		else
		{
			iTween.MoveBy(gameTimeSprite, iTween.Hash("EaseType", "easeOutSine", "y", -0.7, "time", 0.5f));
			iTween.MoveBy(gameTimeSprite, iTween.Hash("EaseType", "easeInSine", "y", 0.7, "time", 0.5f, "Delay", 19));
			iTween.FadeTo (gameTimeSprite, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
			iTween.FadeTo (gameTimeSprite, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 19));
		}
	}

	void StartSilbato()
	{
		silb.Play ();
		iTween.MoveBy(kickNowSprite, iTween.Hash("EaseType", "easeOutSine", "y", 1.4, "time", 0.5f));
		iTween.MoveBy(kickNowSprite, iTween.Hash("EaseType", "easeInSine", "y", -1.4, "time", 0.5f, "Delay", 1.5f));
		iTween.FadeTo (kickNowSprite, iTween.Hash ("alpha", 1.0f, "time", 0.5f));
		iTween.FadeTo (kickNowSprite, iTween.Hash ("alpha", 0.0f, "time", 0.5f, "delay", 1.5f));
		/*if((AnimatorController2.nTiro == 1 || AnimatorController2.nTiro == 2 || AnimatorController2.nTiro == 3 || AnimatorController2.nTiro == 4) && !Timer.sceneTraining)
		{
			Invoke ("TakePhoto", 2);
		}*/
		if(AnimatorController2.nTiro == 2 && !Timer.sceneTraining)
		{// (' - ')
			//StartCoroutine(TakePhoto());
		}
	}

	IEnumerator TakePhoto()
	{
		shooterStatus = Status.TAKING;
		yield return new WaitForSeconds(2);

		int consecutive = 0;
		if(PlayerPrefs.HasKey("Picture Consecutive"))
		{
			int oldConsecutive = PlayerPrefs.GetInt("Picture Consecutive");
			consecutive = oldConsecutive + 1;
		}
		else 
		{
			PlayerPrefs.SetInt("Picture Consecutive", consecutive);
		}

		PlayerPrefs.SetInt("Picture Consecutive", consecutive);
		Kinect.TakePicture (consecutive);
		PhotosKeyBoard.photosTaked[0] = System.Environment.MachineName + "_" + consecutive.ToString().PadLeft(8, '0');
		consecutive++;
		yield return new WaitForSeconds(0.7f);

		PlayerPrefs.SetInt("Picture Consecutive", consecutive);
		Kinect.TakePicture (consecutive);
		PhotosKeyBoard.photosTaked[1] = System.Environment.MachineName + "_" + consecutive.ToString().PadLeft(8, '0');
		consecutive++;
		yield return new WaitForSeconds(0.7f);

		PlayerPrefs.SetInt("Picture Consecutive", consecutive);
		Kinect.TakePicture (consecutive);
		PhotosKeyBoard.photosTaked[2] = System.Environment.MachineName + "_" + consecutive.ToString().PadLeft(8, '0');
		consecutive++;
		yield return new WaitForSeconds(0.7f);

		PlayerPrefs.SetInt("Picture Consecutive", consecutive);
		Kinect.TakePicture (consecutive);
		PhotosKeyBoard.photosTaked[3] = System.Environment.MachineName + "_" + consecutive.ToString().PadLeft(8, '0');

		shooterStatus = Status.FINISHED;
	}

	public enum Status
	{
		NO_CALLED,
		TAKING,
		FINISHED
	};

	public GameObject kickNowSprite;
	public GameObject practiceRoundSprite;
	public GameObject gameTimeSprite;

	public static Status shooterStatus;

	private AudioSource[] sounds;
	private AudioSource ambient;
	private AudioSource silb;
	
	private Vector4 color;
}
