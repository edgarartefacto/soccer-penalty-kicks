﻿using UnityEngine;
using System.Collections;

public class Force0 : MonoBehaviour 
{
	private AudioSource[] sounds;
	private AudioSource net;
	private AudioSource post;

	// Use this for initialization
	void Start () 
	{
		sounds = gameObject.GetComponents<AudioSource> ();
		net = sounds [3];
		post = sounds [4];
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.name == "TriggerBack") 
		{
			net.Play();
			Vector3 forceZero;
			forceZero.x = 0;
			forceZero.y = 0;
			forceZero.z = 0;
			rigidbody.velocity = forceZero;
		}
		if (other.gameObject.name == "TriggerAdds") 
		{
			Vector3 forceZero;
			forceZero.x = 0;
			forceZero.y = 0;
			forceZero.z = 0;
			rigidbody.velocity = forceZero;
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		if (collision.gameObject.name == "ColliderPosteIzq")
			post.Play();
		else if (collision.gameObject.name == "ColliderTrabesano")
			post.Play();
		else if (collision.gameObject.name == "ColliderPosteDer")
			post.Play();
	}
}
