﻿using UnityEngine;
using System.Collections;

public class CambiaTextura : MonoBehaviour {

	public Texture2D[] texs;
	public float counter;
	int i;

	// Use this for initialization
	void Start () {
		counter = 0.0f;
		i = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(counter > 0.25f){
			gameObject.renderer.material.mainTexture = texs[i%2];
			counter = 0;
			i++;
		}
		counter += Time.deltaTime;
	}
}
