using UnityEngine;
using System.Collections;

public class AnimatorController2 : MonoBehaviour 
{
	public static int activeAtaj;
	public static int isWin = 1;
	public static int nTiro = 1;
	public static int resultTiro1 = 0;
	public static int resultTiro2 = 0;
	public static int resultTiro3 = 0;
	public static int resultTiro4 = 0;
	public static int resultTiro5 = 0;
	public static float time = 0;
	public static bool nTiroModified = false;
	public static bool canShot = false;
	public static bool alreadyInverse = false;
	//Vars for the public animation
	public static bool publicWin = false;
	public static bool publicLose = false;

	//-----VARIABLES TO USE WITH THE SERVER-----

	public static string idPlayer = "";
	public static string email = "";
//	public static string isAdult = "";
//	public static string acceptPromos = "";
//	public static string acceptDisplay = "";
	public static int mphAverage = 0;
	public static int score = 0;
	//public static string thumb = "";

	//-----END SERVER VARIABLES-----

	public Animator _animPlayer; //Character
	public LimitSpeed[] limitSpeed;

	//Random desitions
	private int randomWalkEnter = 0;
	private int randomProvocation = 0;
	private int randomExitIzDe = 0;
	private int randomExit = 0;
	private float fullSpeed;
	private float exactSpeed;

	Vector3 positionTally;
	Animator anim;

	// Use this for initialization
	void Start () 
	{
		if(nTiro>1)
		{
			positionTally.x = 0;
			positionTally.y = -0.1510029f;
			positionTally.z = 9.7f;
			iTween.RotateTo(gameObject, new Vector3(0,180,0), 0);
			gameObject.transform.position = positionTally;
		}
		anim = GetComponent<Animator>(); 
		//Set random int to decide the enter
		if(nTiro == 1)
		{
			randomWalkEnter = Random.Range(1, 20);	
			acomodaAEsteWey (randomWalkEnter);
		}
		else
			randomWalkEnter = -1;
		//Set random int to decide the provocation
		randomProvocation = Random.Range(1, 22);
		//Set random int to decide the exit Left or Right
		randomExitIzDe = Random.Range(1, 3);	
		//Set random int to decide the exit
		randomExit = Random.Range(1, 12);	
		if(nTiro > 1)
			Invoke("CanShot", 0.2f);
		else
			Invoke("CanShot", 10.1f);
	}
	
	// Update is called once per frame
	void Update () 
	{
        //print (nTiro);
        
        _animPlayer.SetInteger("RandomWalkEnter", randomWalkEnter);
        _animPlayer.SetInteger("RandomProvocation", randomProvocation);
        if (activeAtaj > 0 && time > 0)
        {
            fullSpeed = limitSpeed[activeAtaj - 1]._up - limitSpeed[activeAtaj - 1]._down;
            if (fullSpeed > 0)
            {
                if (time > limitSpeed[activeAtaj - 1]._up)
                    exactSpeed = 0;
                else if (time > limitSpeed[activeAtaj - 1]._down)
                {
                    exactSpeed = time - limitSpeed[activeAtaj - 1]._down;
                    fullSpeed = exactSpeed / fullSpeed;
                    exactSpeed = 1 - fullSpeed;
                }
                else
                    exactSpeed = 1;
            }
            else
                exactSpeed = 0.5f;
            _animPlayer.SetFloat("Speed", exactSpeed);
        }
        _animPlayer.SetInteger("ActiveAtaj", activeAtaj);
        _animPlayer.SetInteger("RandomExitIzDe", randomExitIzDe);
        _animPlayer.SetInteger("RandomExit", randomExit);
        _animPlayer.SetInteger("IsWin", isWin);
        //Evite state idle move the character
        int atakState = Animator.StringToHash("Base Layer.Idle");
        int currState = _animPlayer.GetCurrentAnimatorStateInfo(0).nameHash;
        if (_animPlayer.GetCurrentAnimatorStateInfo(0).nameHash == atakState)
        {
            //Turn rootMotion off in idle state
            _animPlayer.applyRootMotion = false;
            if (gameObject.transform.localEulerAngles.y < 180.0f)
            {
                transform.Rotate(0, 5 * Time.deltaTime, 0, Space.World);

            }
            else if (gameObject.transform.localEulerAngles.y > 180.0f)
            {
                transform.Rotate(0, -5 * Time.deltaTime, 0, Space.World);
            }
            //DELETE ME
            //Debug.Log ("DELETE ME (tally hall rotation) ->" + gameObject.transform.rotation);
        }
        //Turn rootMotion off in any other state
        else
        {
            _animPlayer.applyRootMotion = true;

        }
        
	}

	void acomodaAEsteWey(int entrada){
		
		//Si todo falla, la posicion de Tally Hall es (-8.95, -0.1695002, 10.71652);
		//gameObject.transform.position = new Vector3(-8.95f, -0.1695002f, 10.71652f);
		
		switch (entrada) {
		case 1:
			gameObject.transform.position -= new Vector3(0.08178875f, 0.0f, 0.0f);
			break;
		case 2:
			gameObject.transform.position -= new Vector3(-0.7433462f, 0.0f, 0.0f);
			break;
		case 3:
			gameObject.transform.position -= new Vector3(0.9301359f, 0.0f, 0.0f);
			break;
		case 4:
			gameObject.transform.position -= new Vector3(0.9301359f, 0.0f, 0.0f);
			break;
		case 5:
			gameObject.transform.position -= new Vector3(-0.1523852f + -0.1285023f, 0.0f, 0.0f);
			break;
		case 6:
			gameObject.transform.position -= new Vector3(-0.339962f, 0.0f, 0.0f);
			break;
		case 7:
			gameObject.transform.position -= new Vector3(-0.3646394f + -0.2277023f, 0.0f, 0.0f);
			break;
		case 8:
			gameObject.transform.position -= new Vector3(0.9819607f, 0.0f, 0.0f);
			break;
		case 9:
			gameObject.transform.position -= new Vector3(-0.7143312f, 0.0f, 0.0f);
			break;
		case 10:
			gameObject.transform.position -= new Vector3(1.171898f, 0.0f, 0.0f);
			break;
		case 11:
			gameObject.transform.position -= new Vector3(1.026145f, 0.0f, 0.0f);
			break;
		case 12:
			gameObject.transform.position -= new Vector3(0.4270908f + 0.1313968f, 0.0f, 0.0f);
			break;
		case 13:
			gameObject.transform.position -= new Vector3(0.1001844f, 0.0f, 0.0f);
			break;
		case 14:
			gameObject.transform.position -= new Vector3(0.4502531f + 0.1124119f, 0.0f, 0.0f);
			break;
		case 15:
			gameObject.transform.position -= new Vector3(0.7148777f, 0.0f, 0.0f);
			break;
		case 16:
			gameObject.transform.position -= new Vector3(0.1272239f, 0.0f, 0.0f);
			break;
		case 17:
			gameObject.transform.position -= new Vector3(0.4406356f, 0.0f, 0.0f);
			break;
		case 18:
			gameObject.transform.position -= new Vector3(-0.113697f, 0.0f, 0.0f);
			break;
		case 19:
			gameObject.transform.position -= new Vector3(0.5077786f, 0.0f, 0.0f);
			break;
		default:
			//do nothin';
			break;
		}
	}

	void CanShot()
	{
		canShot = true;
////		if(nTiro == 3 || nTiro == 4)
////		{
////			int consecutive = 0;
////			if(PlayerPrefs.HasKey("Picture Consecutive"))
////			{
////				int oldConsecutive = PlayerPrefs.GetInt("Picture Consecutive");
////				consecutive = oldConsecutive + 1;
////				PlayerPrefs.SetInt("Picture Consecutive", consecutive);
////			}
////			else 
////				PlayerPrefs.SetInt("Picture Consecutive", consecutive);
////			if(nTiro == 3)
////				photo1 = System.Environment.MachineName + "_" + consecutive;
////			else
////				photo4 = System.Environment.MachineName + "_" + consecutive;
////			Kinect.TakePicture (consecutive);
////		}
	}

	[System.Serializable]
	public class LimitSpeed	
	{	
		public float _down;
		public float _up;
	}
}