﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Launcher : MonoBehaviour
{

    public GameObject winCanvas;
    public GameObject loseCanvas;

    public Text winScoreText;
    public Text loseScoreText;

    public KeenTracking _analytics;

	void Start ()
	{
		isWorking = false;
        if (Timer.sceneTraining)
        {
            Debug.Log("Call Execute 0");
            StartCoroutine(Execute(0));
        }
    }

	public IEnumerator Execute(float time = 0.0f)
	{
		if(time > 0.0f)
		{
			yield return new WaitForSeconds(time);
		}

		if(isWorking)
			yield break;

		isWorking = true;

		// Reset Values
		AnimatorController2.time = 0;
		AnimatorController2.activeAtaj = 0;
		AnimatorController2.isWin = 1;
		AnimatorController2.nTiroModified = false;
		AnimatorController2.alreadyInverse = false;
		AnimatorController2.canShot = false;
		
		Kinect.shotDone = false;
		if(!Timer.sceneTraining)
		{	// Game time
			if(Timer.myTimer < 1.0f || AnimatorController2.nTiro == RESET_SHOT_GAME)
			{
				//Reset();
                
                Timer.sceneTraining = true;
				
				if(!Configuration.isAssistedMode)
				{// Change to sign up
					switch(ObjectVisible.shooterStatus)
					{
						case ObjectVisible.Status.NO_CALLED:


                            if (Puntaje.puntaje >= XMLConfiguration.GetSetting<int>("ScoreToWin", 5))
                            {
                                winCanvas.SetActive(true);
                                winScoreText.text = Puntaje.puntaje+"";
                            }
                            else
                            {
                                loseCanvas.SetActive(true);
                                loseScoreText.text = Puntaje.puntaje+"";
                            }

                            SaveAnalytics();

                            Reset();

                            StartCoroutine("GoToVideo");




							//Application.LoadLevel ("VideoLoop");
							yield break;
						break;

						case ObjectVisible.Status.TAKING:
							yield return StartCoroutine("GoToVideo");
                            break;

						case ObjectVisible.Status.FINISHED:
							yield return StartCoroutine("GoToVideo");
                            break;
					}
				}
				else
				{
					AnimatorController2.mphAverage = AnimatorController2.mphAverage / COUNT_BONUS;
//					UploadDataAssisted();
				}
			}
			else
			{	// Reload game
                //print("Reinicio");
                Debug.Log("Other reset");
				Application.LoadLevel (Application.loadedLevel);
			}
		}
		else
		{	// Training time
			Timer.sceneTraining = false;

			AnimatorController2.nTiro = 1;
			AnimatorController2.score = 0;
            Debug.Log("Reset by training");
			Application.LoadLevel (Application.loadedLevel);
		}

		yield return null;
	}

	public void Reset()
	{
		Timer.progress = -0.2f;
		Puntaje.puntaje = 0;
        Puntaje.tiros = 1;
        Bonus.bonus = 1;
		Kinect.numberCometShot = 1;
		Kinect.numberCometOuterShot = 1;
        Kinect.shotDone = false;
        Kinect.shot = false;
        AnimatorController2.nTiro = 1;
        AnimatorController2.score = 0;
        AnimatorController2.isWin = 1;
        AnimatorController2.nTiro = 1;
        AnimatorController2.resultTiro1 = 0;
        AnimatorController2.resultTiro2 = 0;
        AnimatorController2.resultTiro3 = 0;
        AnimatorController2.resultTiro4 = 0;
        AnimatorController2.resultTiro5 = 0;
        AnimatorController2.time = 0;
        AnimatorController2.nTiroModified = false;
        AnimatorController2.activeAtaj = 0;
        AnimatorController2.canShot = false;
        AnimatorController2.alreadyInverse = false;
        AnimatorController2.canShot = false;
        AnimatorController2.publicWin = false;
        AnimatorController2.publicLose = false;
    }

    public void PressedReset()
    {
        Reset();
        Application.LoadLevel(Application.loadedLevel);
    }

    private IEnumerator GoToVideo()
    {
        yield return new WaitForSeconds(XMLConfiguration.GetSetting<float>("TimeShowScore", 8));
        Application.LoadLevel("VideoLoop");
    }

    private void SaveAnalytics()
    {
        _analytics.TrackEvent(new KeenTracking.GameOverEvent()
        {
            pcName = System.Environment.MachineName,
            kicks = "" + (Puntaje.tiros),
            score = Puntaje.puntaje + ""
        });

    }

    public static readonly int MAX_TRAINING_SHOTS	= 1;
	public static readonly int RESET_SHOT_GAME		= 1;
	public static readonly int COUNT_BONUS			= 5;

	private static bool isWorking			= false;
}
