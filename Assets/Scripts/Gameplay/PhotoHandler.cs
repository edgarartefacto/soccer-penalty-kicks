﻿using UnityEngine;
using System;
using System.Collections;
using System.Timers;

public class PhotoHandler
{
	public PhotoHandler(float millis)
	{
		mName = "";
		mTexture = null;

		mTimer = new System.Timers.Timer(millis);
		mTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
		mTimer.Enabled = true;
	}

	public string Name {
		get {
			return this.mName;
		}
		set {
			mName = value;
		}
	}

	public Texture2D Texture {
		get {
			return this.mTexture;
		}
		set {
			mTexture = value;
		}
	}

	public bool IsWorking {
		get {
			return this.isWorking;
		}
		set {
			isWorking = value;
		}
	}

	public bool IsInBackup {
		get {
			return this.isInBackup;
		}
		set {
			isInBackup = value;
		}
	}

	public void StartTimer()
	{
		isWorking = true;
		isInBackup = false;

		mTimer.Start();
	}

	private void OnTimedEvent(object source, ElapsedEventArgs e)
	{
		mTimer.Dispose();
		isWorking = false;
	}

	public override string ToString ()
	{
		return string.Format ("[PhotoHandler: mName={0}, mTexture={1}, mTimer={2}]", mName, mTexture, mTimer);
	}

	private string mName;

	private bool isWorking;
	private bool isInBackup;

	private Texture2D mTexture;

	private System.Timers.Timer mTimer;
}
