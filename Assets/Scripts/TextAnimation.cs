﻿using UnityEngine;
using System.Collections;
using TMPro;

public class TextAnimation : MonoBehaviour
{
	void Awake()
	{
		textMesh = gameObject.GetComponent<TextMeshPro> ();
		textColor = textMesh.color;
	}

	void Start () {}
	
	void Update () {}

	public void ChangeAlpha(float alpha)
	{
		textColor.a = alpha;
		textMesh.color = textColor;
	}

	public void Reset()
	{
		textColor.a = 1.0f;
		textMesh.color = textColor;
	}

	private TextMeshPro textMesh;

	private Color textColor;
}
