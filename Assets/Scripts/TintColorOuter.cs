﻿using UnityEngine;
using System.Collections;

public class TintColorOuter : MonoBehaviour 
{
	private Vector4 color;
	private GameObject cometa;
	
	// Use this for initialization
	void Start () 
	{
		cometa = GameObject.Find("balon");
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		renderer.material.SetColor ("_TintColor", color);
	}
	
	void Update()
	{
		if (Kinect.shotCometOuter) 
		{
			Kinect.shotCometOuter = false;
			Invoke ("TurnOff", 2);
			if(Kinect.numberCometOuterShot == 1)
			{
				color.w = 0;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometOuterShot == 2)
			{
				color.w = 0.25f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometOuterShot == 3)
			{
				color.w = 0.5f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometOuterShot == 4)
			{
				color.w = 0.75f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometOuterShot == 5)
			{
				color.w = 1;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			if(Kinect.numberCometOuterShot == 5)
				Kinect.numberCometOuterShot = 1;
		}
	}

	void TurnOff()
	{
			color.w = 0;
			color.x = 1;
			color.y = 1;
			color.z = 1;
			renderer.material.SetColor ("_TintColor", color);
	}
}
