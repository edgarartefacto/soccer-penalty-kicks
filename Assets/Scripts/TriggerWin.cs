﻿using UnityEngine;
using System.Collections;

public class TriggerWin : MonoBehaviour 
{
	public Timer myTimer;

	public GameObject winSprite1;
	public GameObject winSprite2;

	private AudioSource[] sounds;
	private AudioSource kick;
	private AudioSource gol;
	private AudioSource fail;
	private AudioSource messageAppear;
	private AudioSource messageDisappear;

	float progresstimeMessage = -1.0f;

	Launcher mLauncher;
	
	void Awake()
	{
		mLauncher = GameObject.FindObjectOfType<Launcher>();
	}

	void Start()
	{
		myTimer = GameObject.Find("Timer").GetComponent<Timer>();
		sounds = gameObject.GetComponents<AudioSource> ();
		kick = sounds[0];
		gol = sounds[1];
		fail = sounds[2];
		messageAppear = sounds[6];
		messageDisappear = sounds[7];
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.name == "TriggerGoal") 
		{
			AnimatorController2.isWin = 0;
			gol.Play();
			AnimatorController2.publicWin = true;
			messageAppear.Play();
			Invoke("Disappear", 1.5f);
			
			iTween.MoveBy(winSprite1, iTween.Hash("EaseType", "easeOutSine", "x", -3.9, "time", 1.0f));
			iTween.MoveBy(winSprite1, iTween.Hash("EaseType", "easeInSine", "x", 3.9, "time", 1.0f, "Delay", 1.5f));
			iTween.ValueTo(gameObject, iTween.Hash("from", progresstimeMessage, "to", 2.0f, "time", 1.0f, "delay", 0.7f, "onupdatetarget", gameObject, "onupdate", "TweenedtimeMessageValue"));
			
			Kinect.numberCometShot++;
			Kinect.numberCometOuterShot++;

			if(mLauncher != null)
			{
				StartCoroutine(mLauncher.Execute(7.0f));
			}
			else
			{
				Debug.Log("TriggerWin: Launcher is NULL");
			}

			//ES AQUI
//			Invoke ("Restart", 7);//<---- CHANGE WITH LAUNCHER
		}
		else if (other.name == "TriggerFuera")
		{
			AnimatorController2.activeAtaj = -1;
		}

		if (other.name == "TriggerGoalExtraPointsD" || other.name == "TriggerGoalExtraPointsI") 
		{
			//Puntaje.puntaje += 10000;
		}
	}

	void Disappear()
	{
		messageDisappear.Play();
	}

	void TweenedtimeMessageValue(float val)
	{
		progresstimeMessage = val;
	}

	void Update()
	{
		winSprite1.renderer.material.SetFloat("_Progress", progresstimeMessage);
		winSprite2.renderer.material.SetFloat("_Progress", progresstimeMessage);
	}

}
