﻿using UnityEngine;
using System.Collections;

public class BodyCollider : MonoBehaviour 
{
	private Vector3 inverse;
	private Vector3 inverseAngular;

	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.name == "balon" && !AnimatorController2.alreadyInverse)
		{
			//print ("Body collision velocity = " + collision.gameObject.rigidbody.velocity);
			AnimatorController2.alreadyInverse = true;
			inverse.x = -collision.gameObject.rigidbody.velocity.x;
			inverse.y = collision.gameObject.rigidbody.velocity.y;
			inverse.z = -collision.gameObject.rigidbody.velocity.z;
			collision.gameObject.rigidbody.velocity = inverse;
			//print ("Body collision velocity = " + collision.gameObject.rigidbody.velocity);
			//Debug.Break();
		}
	}
}
