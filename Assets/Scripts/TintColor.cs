﻿using UnityEngine;
using System.Collections;

public class TintColor : MonoBehaviour 
{
	private Vector4 color;
	private GameObject cometa;

	// Use this for initialization
	void Start () 
	{
		cometa = GameObject.Find("balon");
		color.w = 0;
		color.x = 1;
		color.y = 1;
		color.z = 1;
		renderer.material.SetColor ("_TintColor", color);
	}

	void Update()
	{
		if (Kinect.shotComet) 
		{
			Kinect.shotComet = false;
			Invoke ("TurnOff", 2);
			if(Kinect.numberCometShot == 1)
			{
				color.w = 0;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometShot == 2)
			{
				color.w = 0.25f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometShot == 3)
			{
				color.w = 0.5f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometShot == 4)
			{
				color.w = 0.75f;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			else if(Kinect.numberCometShot == 5)
			{
				color.w = 1;
				color.x = 1;
				color.y = 1;
				color.z = 1;
				renderer.material.SetColor ("_TintColor", color);
			}
			if(Kinect.numberCometShot == 5)
				Kinect.numberCometShot = 1;
		}
	}

	void TurnOff()
	{
			color.w = 0;
			color.x = 1;
			color.y = 1;
			color.z = 1;
			renderer.material.SetColor ("_TintColor", color);
	}
}
