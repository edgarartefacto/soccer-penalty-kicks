using UnityEngine;
using System.Collections;

public class CameraSettings : MonoBehaviour {

	public float fov;
	public Vector3 cameraPos;
	public Camera currentCam;
	float mov = 0.02f;

	public GUIText cpy;
	public GUIText fovi;
	public GUIText cpz;
	public GUIText cr;

	// Use this for initialization
	void Start () {
		if(currentCam == null){
			currentCam = Camera.main;
		}
		cameraPos = currentCam.gameObject.transform.position;
		fov = currentCam.fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp ("up"))
			//movemos la c{amara hacia arriba
			cameraPos = cameraPos + new Vector3(0, mov, 0);
		if (Input.GetKeyUp ("down"))
			//movemos la c{amara hacia abajo
			cameraPos = cameraPos - new Vector3(0, mov, 0);
		if (Input.GetKeyUp ("left"))
			fov-=0.3f;
		if (Input.GetKeyUp ("right"))
			fov+=0.3f;
		if (Input.GetKeyUp ("w")){
			//movemos la camara hacia el frente (sobre z) *En esta escena es x porque somos gilipollas*
			cameraPos = cameraPos - new Vector3(mov, 0, 0);
		}
		if (Input.GetKeyUp ("s")){
			//movemos la camara hacia atras (sobre z) *En esta escena es x porque somos gilipollas*
			cameraPos = cameraPos + new Vector3(mov, 0, 0);
		}
		if (Input.GetKeyUp ("a"))
			currentCam.gameObject.transform.RotateAround(currentCam.gameObject.transform.position, transform.forward, -2f);
		if (Input.GetKeyUp ("d"))
			currentCam.gameObject.transform.RotateAround(currentCam.gameObject.transform.position, transform.forward, 2f);

		currentCam.gameObject.transform.position = cameraPos;
		currentCam.fieldOfView = fov;

		cpy.text = "Camera Position (Y) = " + cameraPos.y;
		fovi.text = "Field Of View = " + fov;
		cr.text = "Camera Rotation Angle = " + currentCam.transform.eulerAngles.x;
		cpz.text = "Camera Position (Z) = " + cameraPos.x;
	}
}
