﻿using UnityEngine;
using System.Collections;

public class SoundSpineFloor : MonoBehaviour 
{
	private AudioSource[] sounds;
	private AudioSource fall;

	void Start()
	{
		sounds = gameObject.GetComponents<AudioSource> ();
		fall = sounds [0];
	}

	void OnCollisionEnter(Collision col)
	{
		print ("Colicion:" + col.gameObject.name);
		if(col.gameObject.name == "Plane")
		{
			print (col.gameObject.name);
			fall.Play();
		}
	}
}
