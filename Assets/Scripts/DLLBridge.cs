﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class DLLBridge
{
	[DllImport ("ResorteraKinectPlugin")]
	public static extern void SetTextureId(IntPtr ptr);

	[DllImport ("ResorteraKinectPlugin")]
	public static extern void ReadPixel(int x, int y);

	[DllImport ("ResorteraKinectPlugin")]
	public static extern void ReadAllPixels();

	[DllImport ("ResorteraKinectPlugin")]
	public static extern void WriteAllPixels();
}
