﻿using UnityEngine;
using System.Collections;

public class EndScreenVideo : MonoBehaviour 
{
	void Start () 
	{
		if(Application.loadedLevelName == "EndScreen")
		{
			Invoke ("TimeLeaderOver", 10);
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			ResetValues();
			Application.LoadLevel ("IntroVideo");
		}
	}

	void OnMouseUp()
	{
		ResetValues();
		Application.LoadLevel ("IntroVideo");
	}

	void TimeLeaderOver()
	{
		ResetValues();
		Application.LoadLevel ("IntroVideo");
	}

	public static void ResetValues()
	{
		AnimatorController2.idPlayer = "";
		AnimatorController2.email = "";
		AnimatorController2.mphAverage = 0;
		AnimatorController2.score = 0;

		for(int i = 0; i < PhotosKeyBoard.photosTaked.Length; i++)
		{
			PhotosKeyBoard.photosTaked[i] = "";
		}
	}
}