using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System;
using System.Xml;
using System.IO;

public class Kinect : MonoBehaviour 
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Point3D 
	{
		public float x;
		public float y;
		public float z;
	}   

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int InitializeKinect (); 

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetTextureId(IntPtr ptr);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int UpdateKinect ();

    [DllImport("MVPKinectUnityPlugin")]
    private static extern int Shutdown();

    [DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetReferencePoint (float x, float y);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int GetForce (ref Point3D force);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetWriteTexture (int val);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetWriteDebugData (int val);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void ReadyForNewShot ();

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int GetNoisePointsNum ();

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetXAngle (float angle);
	
	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetZAngle (float angle);
	
	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetYOffset (float offset);
	
	[DllImport ("MVPKinectUnityPlugin")]
	public static extern void TakePicture (int pictureId);

	[DllImport("MVPKinectUnityPlugin")]
	private static extern void SetPhotoDefaults(int photoX, int photoy, int photoWidth, int photoHeight);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void GetRawFloorNormal (ref Point3D normal);
	
	[DllImport ("MVPKinectUnityPlugin")]
	private static extern void SetRawFloorNormal (ref Point3D normal);

	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int Get3DReferencePoint (ref Point3D refPoint);
	
	[DllImport ("MVPKinectUnityPlugin")]
	private static extern int Set3DReferencePoint (ref Point3D refPoint);

	private Texture2D texture;
	private Vector2 kinectReferencePoint;
	//private float timeSinceShot;
	private bool doShot = false;

	public GameObject soccerBall;
	public TextMesh debug;
	public TextMesh noiseText;

	Point3D pReferencePoint;
	Point3D pNormal;
	Point3D pPoint;
	Vector3 vReferencePoint;
	Vector3 vForce;
	private bool lastCanShot = false;
	private bool saveDebugData = false;

//	private int photoX;
//	private int photoy;
//	private int photoWidth;
//	private int photoHeight;
//	public bool serverMode = false;

	static public bool shotDone = false;
	static public bool shot = false;
	static public bool shotComet = false;
	static public bool shotCometOuter = false;
	static public Point3D pForce;
	static public int numberCometShot = 1;
	static public int numberCometOuterShot = 1;

    public static Kinect singleton;

    float ballForce;
    float groundAngle;
    float sideAngle;

	// Use this for initialization
	void Start () 
	{
		//print ("Entra Kinect");
        if (Kinect.singleton == null)
        {
            Kinect.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        ballForce = Mathf.Clamp(XMLConfiguration.GetSetting<float>("BallForce", 1), 0, 12);
        groundAngle = Mathf.Clamp(XMLConfiguration.GetSetting<float>("GroundAngle", 1), 0, 12);
        sideAngle = Mathf.Clamp(XMLConfiguration.GetSetting<float>("SideAngle", 1), 0, 12);

        Screen.showCursor = true;
		debug = GameObject.Find("Debug").GetComponent<TextMesh>();
		noiseText = GameObject.Find("Noise").GetComponent<TextMesh>();
		//#if UNITY_EDITOR
		InitializeKinect ();
		if(PlayerPrefs.HasKey("kinectReferencePoint_x") && PlayerPrefs.HasKey("kinectReferencePoint_y"))
		{
			kinectReferencePoint.x = PlayerPrefs.GetFloat("kinectReferencePoint_x");
			kinectReferencePoint.y = PlayerPrefs.GetFloat("kinectReferencePoint_y");
			SetReferencePoint (kinectReferencePoint.x,kinectReferencePoint.y);
		}
		
		//#endif

		texture = new Texture2D (512, 512, TextureFormat.ARGB32, false);
		texture.filterMode = FilterMode.Point;

		//#if UNITY_EDITOR
		SetTextureId (texture.GetNativeTexturePtr ());
		//#endif

		//SetPhotoDefaults(Configuration.defaultPhotoX, Configuration.defaultPhotoY, Configuration.defaultPhotoWidth, Configuration.defaultPhotoHeight);

		renderer.material.SetTexture ("_MainTex", texture);
		pReferencePoint = new Point3D ();
		vReferencePoint = new Vector3 ();
		pNormal = new Point3D ();
		pPoint = new Point3D ();
		Invoke ("AsignValues", 5);
		gameObject.renderer.enabled = false;
		if (!Configuration.isTestServer) {// serverMode
			Screen.showCursor = gameObject.renderer.enabled;
		}
	}

	void AsignValues()
	{
        print("Value Assign");
		pNormal.x = PlayerPrefs.GetFloat("RawFloorNormalX", 0);
		pNormal.y = PlayerPrefs.GetFloat("RawFloorNormalY", 0);
		pNormal.z = PlayerPrefs.GetFloat("RawFloorNormalZ", 0);
        print("pNormal " + pNormal.x + "," + pNormal.y + "," + pNormal.z);
		SetRawFloorNormal (ref pNormal);

		pPoint.x = PlayerPrefs.GetFloat("PointReferenceX", 0);
		pPoint.y = PlayerPrefs.GetFloat("PointReferenceY", 0);
		pPoint.z = PlayerPrefs.GetFloat("PointReferenceZ", 0);
		Set3DReferencePoint (ref pPoint);
        print("pPoint " + pPoint.x + "," + pPoint.y + "," + pPoint.z);

        float valueConfKinect;
		valueConfKinect = PlayerPrefs.GetFloat("YOffset", 0);
        print("YOffset : " + valueConfKinect);
		SetYOffset (valueConfKinect);
		valueConfKinect = PlayerPrefs.GetFloat("ZAngle", 0);
        print("ZAngle : " + valueConfKinect);
        SetZAngle (valueConfKinect);
		valueConfKinect = PlayerPrefs.GetFloat("XAngle", 0);
        print("XAngle : " + valueConfKinect);
        SetXAngle (valueConfKinect);
	}
	
	// Update is called once per frame
	void Update () 
	{

		//#if UNITY_EDITOR
		UpdateKinect ();
		
		if( AnimatorController2.canShot && !lastCanShot )
		{
			ReadyForNewShot();
		}
		
		lastCanShot = AnimatorController2.canShot;
		
		if( GetForce(ref pForce)==1 )
		{
			if(AnimatorController2.canShot == true)
			{
				doShot = true;
				AnimatorController2.canShot = false;
                //print("By Kinect: " + pForce.x + "," + pForce.y + "," + pForce.z);
            }
		}
        //#endif
		if (Input.GetKeyUp (KeyCode.Space)) 
		{
			if(AnimatorController2.canShot == true)
			{
				AnimatorController2.canShot = false;
                pForce.x = UnityEngine.Random.Range (-360, 360);
				pForce.y = UnityEngine.Random.Range (40, 300);
				pForce.z = UnityEngine.Random.Range (270, 2000);
                /*pForce.x = 50;
                pForce.y = 150;
                pForce.z = 300;*/
				doShot = true;
			}
		}
		if(gameObject.renderer.enabled)
		{
			int noise = GetNoisePointsNum ();
			noiseText.text = "Noise: " + noise.ToString();
		}
		else
			noiseText.text = "";
		if ( Input.GetKeyUp (KeyCode.Tab) ) 
		{
			if(Application.loadedLevelName != "Gdame")
			{
				gameObject.renderer.enabled = !gameObject.renderer.enabled;
				if(Configuration.isTestServer)//serverMode
				{
					Screen.showCursor = true;
				}
				else
				{
					Screen.showCursor = gameObject.renderer.enabled;
				}
				gameObject.collider.enabled = gameObject.renderer.enabled;
				SetWriteTexture( gameObject.renderer.enabled==true ? 1 : 0 );
			}
		}
		/*if (Input.GetKeyUp (KeyCode.D)) 
		{
			if(Application.loadedLevelName != "KeyBoard")
			{
				saveDebugData = !saveDebugData;
				SetWriteDebugData( saveDebugData ? 1 : 0 );
				if(saveDebugData)
					debug.text = "Debug ON";
				else
					debug.text = "";
			}
		}*/


		if (!shotDone && doShot) 
		{
			doShot = false;
			shotDone = true;
            //timeSinceShot = Time.time;
            //print (pForce.x + "," + pForce.y + "," + pForce.z);
            //print("Antes: " + pForce.x + "," + pForce.y + "," + pForce.z);
            pForce.x *= sideAngle;
            pForce.y *= groundAngle;
            pForce.z *= ballForce;

            //print(pForce.x + "," + pForce.y + "," + pForce.z);

            if (pForce.y < 40)
				pForce.y = 40;
			if(pForce.z < 270)
				pForce.z = 270;
            if (pForce.z > 1200)
                pForce.z = 1200;

			soccerBall.rigidbody.AddForce(pForce.x,pForce.y,pForce.z);
			shotComet = true;
			shotCometOuter = true;
			shot = true;
		}

		if(gameObject.renderer.enabled)
		{
			if (Input.GetKeyUp (KeyCode.W)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float offset;
				offset = PlayerPrefs.GetFloat("YOffset", 0);
                if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    offset += 20.0f;
                else
                    offset += 1.0f;
                SetYOffset (offset);
				PlayerPrefs.SetFloat("YOffset", offset);
			}
            

            if (Input.GetKeyUp (KeyCode.S)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float offset;
				offset = PlayerPrefs.GetFloat("YOffset", 0);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    offset -= 20.0f;
                else
                    offset -= 1.0f;
                SetYOffset (offset);
				PlayerPrefs.SetFloat("YOffset", offset);
			}

			if (Input.GetKeyUp (KeyCode.LeftArrow)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float zAngle;
				zAngle = PlayerPrefs.GetFloat("ZAngle", 0);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    zAngle -= 20.0f;
                else
                    zAngle -= 1.0f;

                SetZAngle (zAngle);
				PlayerPrefs.SetFloat("ZAngle", zAngle);
			}

			if (Input.GetKeyUp (KeyCode.RightArrow)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float zAngle;
				zAngle = PlayerPrefs.GetFloat("ZAngle", 0);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    zAngle += 20.0f;
                else
                    zAngle += 1.0f;
                SetZAngle (zAngle);
				PlayerPrefs.SetFloat("ZAngle", zAngle);
			}

			if (Input.GetKeyUp (KeyCode.UpArrow)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float xAngle;
				xAngle = PlayerPrefs.GetFloat("XAngle", 0);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    xAngle += 20.0f;
                else
                    xAngle += 1.0f;
				SetXAngle (xAngle);
				PlayerPrefs.SetFloat("XAngle", xAngle);
			}

			if (Input.GetKeyUp (KeyCode.DownArrow)) 
			{
				GetRawFloorNormal (ref pNormal);
				PlayerPrefs.SetFloat("RawFloorNormalX", pNormal.x);
				PlayerPrefs.SetFloat("RawFloorNormalY", pNormal.y);
				PlayerPrefs.SetFloat("RawFloorNormalZ", pNormal.z);
				Get3DReferencePoint (ref pPoint);
				PlayerPrefs.SetFloat("PointReferenceX", pPoint.x);
				PlayerPrefs.SetFloat("PointReferenceY", pPoint.y);
				PlayerPrefs.SetFloat("PointReferenceZ", pPoint.z);
				float xAngle;
				xAngle = PlayerPrefs.GetFloat("XAngle", 0);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                    xAngle -= 20.0f;
                else
                    xAngle -= 1.0f;
                SetXAngle (xAngle);
				PlayerPrefs.SetFloat("XAngle", xAngle);
			}
		}
        else
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Input.GetKey(KeyCode.F))
                {
                    ballForce += 0.1f;
                    ballForce = Mathf.Clamp(ballForce, 0, 12);
                    XMLConfiguration.SaveSetting(true, "BallForce", ballForce+"");
                }
                else if(Input.GetKey(KeyCode.G))
                {
                    groundAngle += 0.1f;
                    groundAngle = Mathf.Clamp(groundAngle, 0, 12);
                    XMLConfiguration.SaveSetting(true, "GroundAngle", groundAngle+"");
                }
                else if(Input.GetKey(KeyCode.S))
                {
                    sideAngle += 0.1f;
                    groundAngle = Mathf.Clamp(sideAngle, 0, 12);
                    XMLConfiguration.SaveSetting(true, "SideAngle", sideAngle+"");
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (Input.GetKey(KeyCode.F))
                {
                    ballForce -= 0.1f;
                    ballForce = Mathf.Clamp(ballForce, 0, 12);
                    XMLConfiguration.SaveSetting(true, "BallForce", ballForce + "");
                }
                else if (Input.GetKey(KeyCode.G))
                {
                    groundAngle -= 0.1f;
                    groundAngle = Mathf.Clamp(groundAngle, 0, 12);
                    XMLConfiguration.SaveSetting(true, "GroundAngle", groundAngle + "");
                }
                else if(Input.GetKey(KeyCode.S))
                {
                    sideAngle -= 0.1f;
                    groundAngle = Mathf.Clamp(sideAngle, 0, 12);
                    XMLConfiguration.SaveSetting(true, "SideAngle", sideAngle + "");
                }
            }
        }
	}

	void OnMouseUp()
	{
		if(gameObject.renderer.enabled)
		{
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) 
			{
				MeshCollider mcollider = hit.collider.gameObject.GetComponent<MeshCollider>();
				Vector3 barycentricCoord = hit.barycentricCoordinate;
				Mesh mesh = mcollider.sharedMesh;
				Vector2[] uvs = mesh.uv;
				int[] triangles = mesh.triangles;
				Vector2 uv1,uv2,uv3;
				uv1 = uvs[triangles[hit.triangleIndex * 3 + 0]];
				uv2 = uvs[triangles[hit.triangleIndex * 3 + 1]];
				uv3 = uvs[triangles[hit.triangleIndex * 3 + 2]];
				kinectReferencePoint = uv1*barycentricCoord.x + uv2*barycentricCoord.y + uv3*barycentricCoord.z;
				PlayerPrefs.SetFloat("kinectReferencePoint_x",kinectReferencePoint.x);
				PlayerPrefs.SetFloat("kinectReferencePoint_y",kinectReferencePoint.y);
				PlayerPrefs.SetFloat("YOffset", 0);
				PlayerPrefs.SetFloat("ZAngle", 0);
				PlayerPrefs.SetFloat("XAngle", 0);
				//print("Saved");

				//#if UNITY_EDITOR
				SetReferencePoint (kinectReferencePoint.x,kinectReferencePoint.y);
				//#endif
				Debug.Log("Da clic");
			}
		}
	}

    void OnApplicationQuit()
    {
        Shutdown();
    }
}
