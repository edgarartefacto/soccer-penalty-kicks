﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;

public class Configuration : MonoBehaviour
{
    public static Configuration singleton;
	void Awake()
	{
		Load();

		if(!PlayerPrefs.HasKey(PREFS_XML_PLAYER))
		{
			PlayerPrefs.SetInt(PREFS_XML_PLAYER, 0);
		}
	}

	void Start () {

        if (Configuration.singleton == null)
        {
            Configuration.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	void Update () {}

	public static void Load()
	{
		XmlDocument xmlDoc = new XmlDocument();
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("/") );
		string filepath = path + "/configuration.xml";
		
		if (File.Exists (filepath))
		{
			xmlDoc.Load (filepath);

			int nTesting = 0;
			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/TestServer").InnerText, out nTesting);
			isTestServer = (nTesting == 1) ? true : false;

			liveServer = xmlDoc.DocumentElement.SelectSingleNode("/Config/ProductionServerURL").InnerText;
			testServer = xmlDoc.DocumentElement.SelectSingleNode("/Config/TestServerURL").InnerText;
			currentServer = (isTestServer) ? testServer : liveServer;

			photoDir = xmlDoc.DocumentElement.SelectSingleNode("/Config/PhotoDir").InnerText;
			photoService = xmlDoc.DocumentElement.SelectSingleNode("/Config/PhotoService").InnerText;

			int nAssistedMode = 0;
			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/AssistedMode").InnerText, out nAssistedMode);
			isAssistedMode = (nAssistedMode == 1) ? true : false;

			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/DefaultPhotoX").InnerText, out defaultPhotoX);
			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/DefaultPhotoY").InnerText, out defaultPhotoY);
			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/DefaultPhotoWidth").InnerText, out defaultPhotoWidth);
			int.TryParse(xmlDoc.DocumentElement.SelectSingleNode("/Config/DefaultPhotoHeight").InnerText, out defaultPhotoHeight);
			
			/*print ("config.xml loaded");
			print ("liveServer= " + liveServer);
			print ("testServer= " + testServer);
			print ("isTestServer= " + isTestServer);
			print ("photoDir= " + photoDir);
			print ("photoService= " + photoService);
			print ("isAssistedMode= " + isAssistedMode);
			print ("defaultPhotoX= " + defaultPhotoX);
			print ("defaultPhotoY= " + defaultPhotoY);
			print ("defaultWidth= " + defaultPhotoWidth);
			print ("defaultHeight= " + defaultPhotoHeight);*/
		}
		else
		{
			print ("The file config.xml does not exist.");
		}
	}

	public static int defaultPhotoX;
	public static int defaultPhotoY;
	public static int defaultPhotoWidth;
	public static int defaultPhotoHeight;

	public static bool isTestServer;
	public static bool isAssistedMode;

	public static string currentServer;
	public static string liveServer;
	public static string testServer;
	public static string photoDir;
	public static string photoService;

	public static string PREFS_XML_PLAYER	= "AUDI_PLAYER";
	public static string PREFS_XML_NAME		= "Audi_";
}
