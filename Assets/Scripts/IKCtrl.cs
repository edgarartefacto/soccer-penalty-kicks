﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))]  

public class IKCtrl : MonoBehaviour 
{
	protected Animator animator;
	
	public bool ikActive = false;
	public Transform planeObj = null;
	public Transform rightFoot;
	public Transform leftFoot;
	public Transform rightToe;
	
	private Vector3 rightRigntPosition;
	private Vector3 leftRigntPosition;
	private Vector3 positionToe;
	
	void Start () 
	{
		animator = GetComponent<Animator>();
	}
	
	//a callback for calculating IK
	void OnAnimatorIK(int layer)
	{
		if(animator) 
		{
			//if the IK is active, set the position and rotation directly to the goal. 
			if(ikActive) 
			{
				//weight = 1.0 for the right and left feet means position and rotation will be at the IK goal (the place the character wants to grab)
				animator.SetIKPositionWeight(AvatarIKGoal.RightFoot,1.0f);				
				animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot,1.0f);	
				animator.SetIKRotationWeight(AvatarIKGoal.RightFoot,0.5f);				
				animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot,1.0f);
				//set the position and the rotation of the right and left feet where the external object is
				if(planeObj != null) 
				{
					Debug.Log ("rifht foot: " + rightFoot.position.y);
					Debug.Log ("position:" + rightRigntPosition.y);
					//limit if right position of the right foot
					if(rightFoot.position.y<0.1f)
					{
						//set right position and rotation of the right foot
						rightRigntPosition.x = rightFoot.position.x;
						rightRigntPosition.y = 0.1f;
						rightRigntPosition.z = rightFoot.position.z;
						positionToe.x = rightToe.position.x;
						positionToe.y = 0.1f;
						positionToe.z = rightToe.position.z;
						animator.SetIKPosition(AvatarIKGoal.RightFoot,rightRigntPosition);
						animator.SetIKRotation(AvatarIKGoal.RightFoot,Quaternion.LookRotation(positionToe - rightRigntPosition,Vector3.up));
					}
					else
					{
						rightRigntPosition.x = rightFoot.position.x;
						rightRigntPosition.y = rightFoot.position.y;
						rightRigntPosition.z = rightFoot.position.z;
					}
					//limit if right position of the left foot
					if(leftFoot.position.y<0.1f)
					{
						//set right position and rotation of the right foot
						leftRigntPosition.x = leftFoot.position.x;
						leftRigntPosition.y = 0.1f;
						leftRigntPosition.z = leftFoot.position.z;
						animator.SetIKPosition(AvatarIKGoal.LeftFoot,leftRigntPosition);
					}
					else
					{
						leftRigntPosition.x = leftFoot.position.x;
						leftRigntPosition.y = leftFoot.position.y;
						leftRigntPosition.z = leftFoot.position.z;
					}
				}
			}			
			//if the IK is not active, set the position and rotation of the right and left feet back to the original position
			else 
			{			
				animator.SetIKPositionWeight(AvatarIKGoal.RightFoot,0);
				animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot,0);
			}
		}
	}
}