﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System;
using System.IO;
using System.Runtime.InteropServices;

public class PassMain : MonoBehaviour 
{
	[DllImport ("LaunchApplication")]
	private static extern void LaunchAppIfNotOpened (string appName);
	
	[DllImport("user32.dll")]
	static extern uint GetActiveWindow();
	
	[DllImport("user32.dll")]
	static extern bool SetForegroundWindow(IntPtr hWnd);
	
	[DllImport("user32.dll")]
	static extern bool ShowWindow(IntPtr hWnd,int show);

	public string[] apps;

	private IntPtr w;

	void Start()
	{
		print ("servidor tipo pruebas: " +  Configuration.isTestServer.ToString());

		foreach (string appName in apps) 
		{
			LaunchAppIfNotOpened (appName);
		}

		Application.LoadLevel ("IdleScreen");
	}
}
