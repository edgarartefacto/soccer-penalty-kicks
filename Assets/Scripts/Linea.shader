Shader "Linea" {
	Properties {
		_MainTex ("Texture (RGBA)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags {"Queue" = "Geometry+1" }
		Pass {
           	Blend SrcAlpha OneMinusSrcAlpha 
            Cull Off 
            
            SetTexture [_MainTex] {
            	constantColor [_Color]
                combine texture * constant,  texture * constant
            }
		}
	} 
}
