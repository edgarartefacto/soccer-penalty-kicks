using UnityEngine;
using System.Collections;

public class ExampleTexture : MonoBehaviour
{
	void Start ()
	{
//		texture = new Texture2D (2048, 2048, TextureFormat.ARGB32, false);
//		texture.filterMode = FilterMode.Bilinear;

		DLLBridge.SetTextureId(renderer.material.mainTexture.GetNativeTexturePtr());
//		renderer.material.SetTexture ("_MainTex", texture);
	}
	
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			DLLBridge.WriteAllPixels();
		}
	}

	private Texture2D texture;
}
