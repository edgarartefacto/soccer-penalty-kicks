﻿using UnityEngine;
using System.Collections;

public class TestWWWTexture : MonoBehaviour 
{
	public string url = "";
	//public string url = "http://newsite.mvp-interactive.com/upload/ARTEFACTOW8_78.png";

	// Use this for initialization
	IEnumerator Start() 
	{
		WWW www = new WWW(url);
		yield return www;
		renderer.material.mainTexture = www.texture;
	}
}
