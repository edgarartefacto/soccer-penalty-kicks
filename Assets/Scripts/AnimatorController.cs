﻿using UnityEngine;
using System.Collections;

public class AnimatorController : MonoBehaviour 
{
	public static int activeAtaj;

	public Animator _animPlayer; //Character
	public AnimationClip Test;

	//Random desitions
	private int randomWalkEnter = 0;
	private int randomEnter = 0;
	private float randomFloat = 0.0f;
	private float randomFloatAtaj = 0.0f;

	void Start()
	{
		//Set random int to decide the walk enter
		randomWalkEnter = Random.Range(1, 5);
		_animPlayer.SetInteger("RandomWalkEnter",randomWalkEnter);
		//Set random int to decide the enter
		if (randomWalkEnter > 1) 
		{
			randomEnter = Random.Range (1, 2);
			_animPlayer.SetInteger ("RandomEnter", randomEnter);
		}
		//set random float value to decide final animation
		while (randomFloat == 0.0f)		
			randomFloat = Random.Range (-1.0f, 1.0f);
		while (randomFloatAtaj == 0.0f)		
			randomFloatAtaj = Random.Range (-1.0f, 1.0f);
		//Restart every 30 seconds
		Invoke("RestarLevel", 30);
	}
		
	// Update is called once per frame
	void Update () 
	{
		//decide final animation
		if(randomFloat > 0.0f)		   
			_animPlayer.SetBool("RandomIzqDer",true);			
		else if(randomFloat < 0.0f)
			_animPlayer.SetBool("RandomIzqDer",false);
		_animPlayer.SetInteger("ActiveAtaj",activeAtaj);
	}

	//Called when want to restar the levels
	void RestarLevel()
	{
		Application.LoadLevel(Application.loadedLevel); //Current level
	}
}
