﻿Shader "Artefacto/Alpha Mask" {
Properties {    

    _MainTex ("Texture", 2D) = "white" { }
    _Mask ("Mask", 2D) = "white" { }
  
}
SubShader {
	Tags {"Queue" = "Transparent" }
    Pass {
    Blend SrcAlpha OneMinusSrcAlpha     
	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag	
	#include "UnityCG.cginc"	

	sampler2D _MainTex,_Mask;
	
	
	struct v2f {
	    float4  pos : SV_POSITION;
	    float2  uv : TEXCOORD0;
	};	
	float4 _MainTex_ST;	

	v2f vert (appdata_base v){
	    v2f o;
	    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	    return o;
	}	
	half4 frag (v2f i) : COLOR{		
	
		half4 finalcol = tex2D (_MainTex, i.uv);
		finalcol.a *= tex2D (_Mask, i.uv).a;
		 
		
	    return finalcol;
	}
	ENDCG

    }
}
Fallback "VertexLit"
} 