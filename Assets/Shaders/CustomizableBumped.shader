﻿Shader "Custom/CustomizableBumped" {
 Properties
 {
 _Color ("Main Color", Color) = (1,1,1,1)
 _MainTex ("Base (RGB)", 2D) = "white" {}
 _BumpMap ("Normalmap", 2D) = "bump" {}
 //1 means do nothing. 0 means max power, 2 means low power
 _BumpPower ("Bump Power", Range (3, 0.01)) = 1
 }
 SubShader {
 	Tags { "RenderType"="Opaque" }
 	LOD 350

CGPROGRAM
#pragma surface surf Lambert

 sampler2D _MainTex;
 sampler2D _BumpMap;
 fixed4 _Color;
 fixed _BumpPower;

 struct Input
 {
 float2 uv_MainTex;
 float2 uv_BumpMap;
 };

 void surf (Input IN, inout SurfaceOutput o)
 {
 fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
 o.Albedo = c.rgb;
 o.Alpha = c.a;
 fixed3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
 normal.z = normal.z * _BumpPower;
 o.Normal = normalize(normal);
 }

 ENDCG  
 }
 FallBack "Bumped Diffuse"
}
