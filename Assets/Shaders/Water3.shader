﻿Shader "Custom/Water3" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_Cube ("Cubemap", CUBE) = "" {}
		_reflect ("Reflect", Range (0.0, 1)) = 0.5
		_refract ("Refract", Range (0.0, 1)) = 0.5
	}
	SubShader {
		Tags { "Queue"="Transparent+1" }
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			
			sampler2D _MainTex;
			sampler2D _BumpMap;
			samplerCUBE _Cube;
			float _reflect;
			float _refract;
			
			struct vertexInput {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord0 : TEXCOORD0;
				float4 tangent : TANGENT;
			};

			struct fragmentInput{
				float4 position : SV_POSITION;
				float4 texcoord0 : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
				float3 binormalWorld : TEXCOORD4;
			};

			fragmentInput vert(vertexInput i){
				fragmentInput o;
				o.position = mul (UNITY_MATRIX_MVP, i.vertex);
				o.normalWorld = normalize ( mul( float4(i.normal,0.0), _World2Object ).xyz );
				o.tangentWorld = normalize ( mul( _Object2World , i.tangent ).xyz );
				o.binormalWorld = normalize ( cross(o.normalWorld,o.tangentWorld ) * i.tangent.w );
				
				o.texcoord0 = i.texcoord0;
				return o;
			}
			float4 frag(fragmentInput i) : COLOR {
				half4 c;
				half4 n;
				half3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xzy );
				
				c.rgb = tex2D(_MainTex, i.texcoord0.xy).rgb;
				half3 localCoords = tex2D(_BumpMap, i.texcoord0.xy).rgb;
				
				localCoords.xyz = localCoords.xyz * 2.0; 
				localCoords.xyz -= half3(1.0,1.0,1.0); 
				
				float3x3 m = float3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				n.xyz = normalize( mul(localCoords,m) );
				
				float d = dot(i.normalWorld , viewDirection.xyz );
				half3 rl = pow( texCUBE ( _Cube, reflect( -viewDirection,n.xyz) ) * _reflect * (d) , 2.3)*3.0;
				
				c.rgb = half3(d,d,d)*( 1.0 - _refract )*0.5;
				
				c.rgb += rl;
				half l = rl.r*0.33 + rl.g*0.33 + rl.b*0.33;
				
				//c.rgb += pow( texCUBE (_Cube, refract( viewDirection,n.xyz,1.33) ) *_refract * d , 1.5);
				
				//c.rgb = half3(d,d,d);
				//c.rgb = n.rgb;
				
				c.a = l;
				return c;
			}

			ENDCG
		}
	}
}