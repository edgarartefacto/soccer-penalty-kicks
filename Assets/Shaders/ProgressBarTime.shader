// Upgrade NOTE: replaced 'glstate.matrix.mvp' with 'UNITY_MATRIX_MVP'

Shader "ProgressBarTimeText" {

Properties {
    _Color ("Color", Color) = (1,1,1,1)
    _MainTex ("Main Tex (RGBA)", 2D) = "white" {}
    _Progress ("Progress", Range(0.0,1.0)) = 0.0
    _MinU ("Min U",Float) = 0.0
    _MaxU ("Max U",Float) = 0.0
}

SubShader {
    	Tags { "Queue"="Transparent" }
    	ZTest always
    	Blend SrcAlpha OneMinusSrcAlpha
        Pass {

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

uniform sampler2D _MainTex;
uniform float4 _Color;
uniform float _Progress;
uniform float _MinU;
uniform float _MaxU;

struct v2f {
    float4 pos : POSITION;
    float2 uv : TEXCOORD0;
};

v2f vert (appdata_base v)
{
    v2f o;
    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
    o.uv = TRANSFORM_UV(0);

    return o;
}

half4 frag( v2f i ) : COLOR
{
	float range = 1.0;
    half4 color = tex2D( _MainTex, i.uv);
    float u = _MinU + (_MaxU-_MinU)*_Progress;
    if(   u < i.uv.x )
    {
    	if( u > i.uv.x - range  )
    	{
    		half3 c1, c2;
    		c1 = half3(0.99,0.94,0.86);
    		c2 = tex2D( _MainTex, i.uv);
    		float a = (u - i.uv.x + range)/range;
//    		color.a *= a;
    		color.rgb = a*c1 + (1.0-a)*c2;
    		//color.rgb = c1;
    	}
    	else
    	{
    		color.a *= 1.0;
    	}
    }else
    {
    	if( u < i.uv.x + range  )
    	{
    		half3 c1, c2;
    		c1 = half3(0.99,0.94,0.86);
    		c2 = tex2D( _MainTex, i.uv);
    		float a = (i.uv.x + range - u)/range;
//    		color.a *= a;
    		color.rgb = a*c1 + (1.0-a)*c2;
    		//color.rgb = c1;
    	}
    	else
    	{
    	color.a *= 1.0;
    	color.rgb = tex2D( _MainTex, i.uv);
    	}
    }
    
    //return color*_Color;
    
    return color;
}

ENDCG

    }
}

}
