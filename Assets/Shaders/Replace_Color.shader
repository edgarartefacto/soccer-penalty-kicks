Shader "Replace - Color" {
	Properties {
		_MainTex ("Texture (RGBA)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags {"Queue" = "Transparent" }
		Pass {
           	Blend SrcAlpha OneMinusSrcAlpha 
            Cull Off 
            SetTexture [_MainTex] {
            	constantColor [_Color]
                combine texture * constant,  texture * constant
            }
		}
	} 
}
