﻿Shader "Artefacto/Cerdas" {
	Properties {
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_AmbColor ("Ambient Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (2, 300)) = 0.078125
		_Diffuse ("Diffuse", Range (0, 1)) = 1.0
		_Ambient ("Ambient", Range (0, 1)) = 1.0
		_Specular ("Specular", Range (0, 1)) = 1.0
		_MainTex ("Base (RGB)", 2D) = "white" {}
	    _BumpMap ("Normalmap", 2D) = "bump" {}
	}
	SubShader {
			Tags { "RenderType"="Transparent" }
			LOD 200
			//Blend SrcAlpha OneMinusSrcAlpha
			AlphaTest Greater 0.9
			Cull Off
		
			
			CGPROGRAM
			#pragma surface surf SimpleSpecular

			sampler2D _MainTex,_BumpMap;
			half _Shininess,_Diffuse,_Ambient,_Specular;
			half4 _AmbColor;

			half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			          half3 h = normalize (lightDir + viewDir);

			          half diff = max (0, dot (s.Normal, lightDir));

			          float nh = max (0, dot (s.Normal, h));
			          float spec = pow (nh, s.Specular);

			          half4 c;
			          c.rgb = (s.Albedo * _LightColor0.rgb * diff * _Diffuse + _LightColor0.rgb * spec * _SpecColor * _Specular) * (atten * 2) + _AmbColor * s.Albedo * _Ambient;
			          c.a = s.Alpha;
			          return c;
			      }


			 
			struct Input {
				float2 uv_MainTex;
				float2 uv_BumpMap;
			};

			void surf (Input IN, inout SurfaceOutput o) {

				fixed4 finalcol;
				float2 uv = IN.uv_MainTex;

				finalcol = tex2D(_MainTex, uv);
				
				o.Gloss = finalcol.a;
				o.Specular = _Shininess;
				o.Albedo = finalcol.rgb;
				//o.Albedo = float3(0.0,0.0,1.0);
				o.Alpha = finalcol.a;
				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			}
			ENDCG
			
			ZWrite Off
			AlphaTest LEqual 0.95
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Less
			Cull Front
			
		
			CGPROGRAM
			#pragma surface surf SimpleSpecular

			sampler2D _MainTex,_BumpMap;
			half _Shininess,_Diffuse,_Ambient,_Specular;
			half4 _AmbColor;

			half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			          half3 h = normalize (lightDir + viewDir);

			          half diff = max (0, dot (s.Normal, lightDir));

			          float nh = max (0, dot (s.Normal, h));
			          float spec = pow (nh, s.Specular);

			          half4 c;
			          c.rgb = (s.Albedo * _LightColor0.rgb * diff * _Diffuse + _LightColor0.rgb * spec * _SpecColor * _Specular) * (atten * 2) + _AmbColor * s.Albedo * _Ambient;
			          c.a = s.Alpha;
			          return c;
			      }


			 
			struct Input {
				float2 uv_MainTex;
				float2 uv_BumpMap;
			};

			void surf (Input IN, inout SurfaceOutput o) {

				fixed4 finalcol;
				float2 uv = IN.uv_MainTex;

				finalcol = tex2D(_MainTex, uv);
				
				o.Gloss = finalcol.a;
				o.Specular = _Shininess;
				o.Albedo = finalcol.rgb;
				//o.Albedo = float3(1.0,0.0,0.0);
				o.Alpha = finalcol.a;
				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			}
			ENDCG
			
			ZWrite Off
			AlphaTest LEqual 0.95
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Less
			Cull Back
			
		
			CGPROGRAM
			#pragma surface surf SimpleSpecular

			sampler2D _MainTex,_BumpMap;
			half _Shininess,_Diffuse,_Ambient,_Specular;
			half4 _AmbColor;

			half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
			          half3 h = normalize (lightDir + viewDir);

			          half diff = max (0, dot (s.Normal, lightDir));

			          float nh = max (0, dot (s.Normal, h));
			          float spec = pow (nh, s.Specular);

			          half4 c;
			          c.rgb = (s.Albedo * _LightColor0.rgb * diff * _Diffuse + _LightColor0.rgb * spec * _SpecColor * _Specular) * (atten * 2) + _AmbColor * s.Albedo * _Ambient;
			          c.a = s.Alpha;
			          return c;
			      }


			 
			struct Input {
				float2 uv_MainTex;
				float2 uv_BumpMap;
			};

			void surf (Input IN, inout SurfaceOutput o) {

				fixed4 finalcol;
				float2 uv = IN.uv_MainTex;

				finalcol = tex2D(_MainTex, uv);
				
				o.Gloss = finalcol.a;
				o.Specular = _Shininess;
				o.Albedo = finalcol.rgb;
				//o.Albedo = float3(1.0,0.0,0.0);
				o.Alpha = finalcol.a;
				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			}
			ENDCG
			
		
	}
	
} 
