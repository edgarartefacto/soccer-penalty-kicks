﻿Shader "Artefacto/Hair5" 
{
	Properties 
	{
		_DiffuseColor ("Diffuse Color", Color) = (0.5, 0.5, 0.5, 1)
		_SpecularColor1 ("Specular Color 1", Color) = (0.5, 0.5, 0.5, 1)
		_SpecularColor2 ("Specular Color 2", Color) = (0.5, 0.5, 0.5, 1)
		_LightColor ("Light Color", Color) = (0.5, 0.5, 0.5, 1)
		_PrimaryShift ("Primary Shift", Range (0, 1)) = 1.0
		_SecondaryShift ("Secondary Shift", Range (0, 1)) = 1.0
		_SpecExp1 ("Spec Exp 1", Range (0, 500)) = 1.0
		_SpecExp2 ("Spec Exp 2", Range (0, 500)) = 1.0
		_tSpecShift ("Shift (RGB)", 2D) = "white" {}
		_tSpecMask ("Noise (RGB)", 2D) = "white" {}
		_tBase ("Base (RGB)", 2D) = "white" {}
		_tAlpha ("Alpha (RGB)", 2D) = "white" {}
		_VectorLight1 ("Vector Light 1", Vector) = (0,0,0,0) 
		_VectorLight2 ("Vector Light 2", Vector) = (0,0,0,0) 
		_VectorLight3 ("Vector Light 3", Vector) = (0,0,0,0) 
		_VectorLight4 ("Vector Light 4", Vector) = (0,0,0,0)
		_ColorLight1 ("Color Light 1", Color) = (0,0,0,0) 
		_ColorLight2 ("Color Light 2", Color) = (0,0,0,0) 
		_ColorLight3 ("Color Light 3", Color) = (0,0,0,0) 
		_ColorLight4 ("Color Light 4", Color) = (0,0,0,0)
		_nLights ("N Lights", int) = 0
	}	
	
	SubShader 
	{
		Tags { "Queue"="Transparent" "LightMode" = "ForwardBase" }		
		LOD 200
			
		AlphaTest GEqual 0.99
		Cull Off	
		ZWrite On
		ZTest Less
		//Blend SrcAlpha OneMinusSrcAlpha
		
		Pass 
		{
			CGPROGRAM
			#pragma multi_compile_fwdbase 
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			
			#include "UnityCG.cginc" 
			
			uniform float4 _LightColor0; 
			
			float3 vertexLighting;
			float3 vertexSpec;	
            // color of light source (from "Lighting.cginc")
			
			sampler2D _tSpecShift,_tSpecMask,_tBase,_tAlpha;
			half _PrimaryShift,_SecondaryShift,_SpecExp1,_SpecExp2;
			half4 _DiffuseColor,_SpecularColor1,_SpecularColor2,_LightColor,_ColorLight1, _ColorLight2, _ColorLight3, _ColorLight4;
			float4 _VectorLight1, _VectorLight2, _VectorLight3, _VectorLight4;
			uniform float4 VectorLight[4];
			uniform float4 ColorLight[4];
			int _nLights;
			
			struct vertexInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				float4 tangent : TANGENT;
			};

			struct fragmentInput
			{
				float4 position : SV_POSITION;
				float4 col : COLOR;
				float4 uv : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
			};			
			
			float3 ShiftTangent(float3 T, float3 N, float shift)
			{	
				float3 shiftedT = T + shift * N;
				return normalize(shiftedT);
			}
			
			float StrandSpecular(float3 T, float3 V, float3 L, float exponent)
			{
				float3 H = normalize(L + V);
				float dotTH = dot(T, H);
				float sinTH = sqrt(1.0 - dotTH * dotTH);
				float dirAtten = smoothstep(-1.0, 0.0, dot(T, H));
				return dirAtten * pow(sinTH, exponent);
			}
			
			float3 FinalColor(int index, float4 posWorld, float3 normalDirection, float3 t1, float3 t2, float3 viewDirection, float specMask)
			{
				float4 lightColor;
				float3 vertexToLightSource;
				float3 lightDirection;
				float3 diffuseReflection;
				float3 specularReflection = float3(0.0, 0.0, 0.0);
				float attenuation = 1.0;
	            fixed isPoint = 0.0;
				if(VectorLight[index].w == 1.0)
	    		{	            			
	        		vertexToLightSource = float3(VectorLight[index] - posWorld);	                		
	        		isPoint = 1.0;
	        	}
	        	else
	        	{
	        		lightDirection = VectorLight[index].xyz;
	        	}
	        	lightColor = ColorLight[index];
	        	if(isPoint == 1.0)//PointLight?
                {
	                lightDirection = normalize(vertexToLightSource);
	                float squaredDistance = dot(vertexToLightSource, vertexToLightSource);
	                attenuation = 1.0 / (1.0 + unity_4LightAtten0[index] * squaredDistance);
	            }
	            diffuseReflection = attenuation * float3(lightColor) * float3(_DiffuseColor) * saturate(lerp(0.25, 1.0, dot(normalDirection, lightDirection)));
	            // light source on the wrong side?
	            if (dot(normalDirection, lightDirection) >= 0.0) 	               
	            {		            	
	            	//specular lighting
	                specularReflection = attenuation * float3(lightColor) * float3(_SpecularColor1) * StrandSpecular(t1, viewDirection, lightDirection, _SpecExp1);
					//add 2nd specular term, modulated with noise texture		                																											  
					specularReflection += attenuation * float3(lightColor) * float3(_SpecularColor2) * specMask * StrandSpecular(t2, viewDirection, lightDirection, 
																																 _SpecExp2);
	            }
	            vertexLighting = vertexLighting + diffuseReflection;
	            vertexSpec = vertexSpec + specularReflection;
	            float3 ambientLighting = float3(UNITY_LIGHTMODEL_AMBIENT) * float3(_DiffuseColor);
	            return ambientLighting + vertexSpec + vertexLighting;
	            
			}
			
			fragmentInput vert(vertexInput i)
			{
				fragmentInput o;				
				float4x4 modelMatrix = _Object2World;
				float4x4 modelMatrixInverse = _World2Object; 
	            // unity_Scale.w is unnecessary here 
	            o.posWorld = mul(modelMatrix, i.vertex);
	            o.normalWorld = normalize(float3(mul(float4(i.normal, 0.0), modelMatrixInverse)));
				o.position = mul(UNITY_MATRIX_MVP, i.vertex);							
				o.tangentWorld = normalize(mul(_Object2World, i.tangent).xyz);			
				o.uv = float4( i.uv.xy, 0, 0 );
				return o;
			}
			
			float4 frag(fragmentInput i) : COLOR 
			{
				float4 c;
				ColorLight[0] = _ColorLight1;
				ColorLight[1] = _ColorLight2;
				ColorLight[2] = _ColorLight3;
				ColorLight[3] = _ColorLight4;
				VectorLight[0] = _VectorLight1;
				VectorLight[1] = _VectorLight2;
				VectorLight[2] = _VectorLight3;
				VectorLight[3] = _VectorLight4;
				float3 normalDirection = normalize(i.normalWorld);
				float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xzy );
				//shift tangents
				float shiftTex = tex2D(_tSpecShift, i.uv.xy) - 0.5;
				float3 t1 = ShiftTangent(i.tangentWorld, i.normalWorld, _PrimaryShift + shiftTex);
				float3 t2 = ShiftTangent(i.tangentWorld, i.normalWorld, _SecondaryShift + shiftTex);
				float specMask = tex2D(_tSpecMask, i.uv.xy); //approximate sparkles using texture
	            // Diffuse and specular reflection by four "vertex lights"
				vertexLighting = float3(0.0, 0.0, 0.0);
				vertexSpec = float3(0.0, 0.0, 0.0);				
				int index = 0;
				float3 sumaColores = float3(0.0, 0.0, 0.0);
				if(index < _nLights)
				{
					sumaColores = FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
					index++;
					if(index < _nLights)
					{
						sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
						index++;
						if(index < _nLights)
						{
							sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
							index++;
							if(index < _nLights)
							{
								sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
								index++;
							}
						}
					}										
				}	            
	            //Final color assembly
	            //VectorLight[0] = _VectorLight1;
	            //c.rgb = VectorLight[0].xyz;
	            c.rgb = sumaColores * tex2D(_tBase, i.uv.xy) * _LightColor;
				//o.rgb = specular;
				//o.rgb = diffuse;
				c.a = tex2D(_tAlpha, i.uv.xy); //read alpha texture
				return c;				
			}

			ENDCG
		}		
		
		AlphaTest Less 0.99	
		Cull Front
		ZWrite Off
		ZTest Less
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass 
		{
			CGPROGRAM
			#pragma multi_compile_fwdbase 
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			
			#include "UnityCG.cginc" 
			
			uniform float4 _LightColor0; 
			
			float3 vertexLighting;
			float3 vertexSpec;	
            // color of light source (from "Lighting.cginc")
			
			sampler2D _tSpecShift,_tSpecMask,_tBase,_tAlpha;
			half _PrimaryShift,_SecondaryShift,_SpecExp1,_SpecExp2;
			half4 _DiffuseColor,_SpecularColor1,_SpecularColor2,_LightColor,_ColorLight1, _ColorLight2, _ColorLight3, _ColorLight4;
			float4 _VectorLight1, _VectorLight2, _VectorLight3, _VectorLight4;
			uniform float4 VectorLight[4];
			uniform float4 ColorLight[4];
			int _nLights;
			
			struct vertexInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				float4 tangent : TANGENT;
			};

			struct fragmentInput
			{
				float4 position : SV_POSITION;
				float4 col : COLOR;
				float4 uv : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
			};			
			
			float3 ShiftTangent(float3 T, float3 N, float shift)
			{	
				float3 shiftedT = T + shift * N;
				return normalize(shiftedT);
			}
			
			float StrandSpecular(float3 T, float3 V, float3 L, float exponent)
			{
				float3 H = normalize(L + V);
				float dotTH = dot(T, H);
				float sinTH = sqrt(1.0 - dotTH * dotTH);
				float dirAtten = smoothstep(-1.0, 0.0, dot(T, H));
				return dirAtten * pow(sinTH, exponent);
			}
			
			float3 FinalColor(int index, float4 posWorld, float3 normalDirection, float3 t1, float3 t2, float3 viewDirection, float specMask)
			{
				float4 lightColor;
				float3 vertexToLightSource;
				float3 lightDirection;
				float3 diffuseReflection;
				float3 specularReflection = float3(0.0, 0.0, 0.0);
				float attenuation = 1.0;
	            fixed isPoint = 0.0;
				if(VectorLight[index].w == 1.0)
	    		{	            			
	        		vertexToLightSource = float3(VectorLight[index] - posWorld);	                		
	        		isPoint = 1.0;
	        	}
	        	else
	        	{
	        		lightDirection = VectorLight[index].xyz;
	        	}
	        	lightColor = ColorLight[index];
	        	if(isPoint == 1.0)//PointLight?
                {
	                lightDirection = normalize(vertexToLightSource);
	                float squaredDistance = dot(vertexToLightSource, vertexToLightSource);
	                attenuation = 1.0 / (1.0 + unity_4LightAtten0[index] * squaredDistance);
	            }
	            diffuseReflection = attenuation * float3(lightColor) * float3(_DiffuseColor) * saturate(lerp(0.25, 1.0, dot(normalDirection, lightDirection)));
	            // light source on the wrong side?
	            if (dot(normalDirection, lightDirection) >= 0.0) 	               
	            {		            	
	            	//specular lighting
	                specularReflection = attenuation * float3(lightColor) * float3(_SpecularColor1) * StrandSpecular(t1, viewDirection, lightDirection, _SpecExp1);
					//add 2nd specular term, modulated with noise texture		                																											  
					specularReflection += attenuation * float3(lightColor) * float3(_SpecularColor2) * specMask * StrandSpecular(t2, viewDirection, lightDirection, 
																																 _SpecExp2);
	            }
	            vertexLighting = vertexLighting + diffuseReflection;
	            vertexSpec = vertexSpec + specularReflection;
	            float3 ambientLighting = float3(UNITY_LIGHTMODEL_AMBIENT) * float3(_DiffuseColor);
	            return ambientLighting + vertexSpec + vertexLighting;
	            
			}
			
			fragmentInput vert(vertexInput i)
			{
				fragmentInput o;				
				float4x4 modelMatrix = _Object2World;
				float4x4 modelMatrixInverse = _World2Object; 
	            // unity_Scale.w is unnecessary here 
	            o.posWorld = mul(modelMatrix, i.vertex);
	            o.normalWorld = normalize(float3(mul(float4(i.normal, 0.0), modelMatrixInverse)));
				o.position = mul(UNITY_MATRIX_MVP, i.vertex);							
				o.tangentWorld = normalize(mul(_Object2World, i.tangent).xyz);			
				o.uv = float4( i.uv.xy, 0, 0 );
				return o;
			}
			
			float4 frag(fragmentInput i) : COLOR 
			{
				float4 c;
				ColorLight[0] = _ColorLight1;
				ColorLight[1] = _ColorLight2;
				ColorLight[2] = _ColorLight3;
				ColorLight[3] = _ColorLight4;
				VectorLight[0] = _VectorLight1;
				VectorLight[1] = _VectorLight2;
				VectorLight[2] = _VectorLight3;
				VectorLight[3] = _VectorLight4;
				float3 normalDirection = normalize(i.normalWorld);
				float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xzy );
				//shift tangents
				float shiftTex = tex2D(_tSpecShift, i.uv.xy) - 0.5;
				float3 t1 = ShiftTangent(i.tangentWorld, i.normalWorld, _PrimaryShift + shiftTex);
				float3 t2 = ShiftTangent(i.tangentWorld, i.normalWorld, _SecondaryShift + shiftTex);
				float specMask = tex2D(_tSpecMask, i.uv.xy); //approximate sparkles using texture
	            // Diffuse and specular reflection by four "vertex lights"
				vertexLighting = float3(0.0, 0.0, 0.0);
				vertexSpec = float3(0.0, 0.0, 0.0);				
				int index = 0;
				float3 sumaColores = float3(0.0, 0.0, 0.0);
				if(index < _nLights)
				{
					sumaColores = FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
					index++;
					if(index < _nLights)
					{
						sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
						index++;
						if(index < _nLights)
						{
							sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
							index++;
							if(index < _nLights)
							{
								sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
								index++;
							}
						}
					}										
				}	            
	            //Final color assembly
	            //VectorLight[0] = _VectorLight1;
	            //c.rgb = VectorLight[0].xyz;
	            c.rgb = sumaColores * tex2D(_tBase, i.uv.xy) * _LightColor;
				//o.rgb = specular;
				//o.rgb = diffuse;
				c.a = tex2D(_tAlpha, i.uv.xy); //read alpha texture
				return c;				
			}

			ENDCG
		}
		
		AlphaTest Less 0.99
		Cull Back
		ZWrite On
		ZTest Less
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass 
		{
			CGPROGRAM
			#pragma multi_compile_fwdbase 
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			
			#include "UnityCG.cginc" 
			
			uniform float4 _LightColor0; 
			
			float3 vertexLighting;
			float3 vertexSpec;	
            // color of light source (from "Lighting.cginc")
			
			sampler2D _tSpecShift,_tSpecMask,_tBase,_tAlpha;
			half _PrimaryShift,_SecondaryShift,_SpecExp1,_SpecExp2;
			half4 _DiffuseColor,_SpecularColor1,_SpecularColor2,_LightColor,_ColorLight1, _ColorLight2, _ColorLight3, _ColorLight4;
			float4 _VectorLight1, _VectorLight2, _VectorLight3, _VectorLight4;
			uniform float4 VectorLight[4];
			uniform float4 ColorLight[4];
			int _nLights;
			
			struct vertexInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				float4 tangent : TANGENT;
			};

			struct fragmentInput
			{
				float4 position : SV_POSITION;
				float4 col : COLOR;
				float4 uv : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalWorld : TEXCOORD2;
				float3 tangentWorld : TEXCOORD3;
			};			
			
			float3 ShiftTangent(float3 T, float3 N, float shift)
			{	
				float3 shiftedT = T + shift * N;
				return normalize(shiftedT);
			}
			
			float StrandSpecular(float3 T, float3 V, float3 L, float exponent)
			{
				float3 H = normalize(L + V);
				float dotTH = dot(T, H);
				float sinTH = sqrt(1.0 - dotTH * dotTH);
				float dirAtten = smoothstep(-1.0, 0.0, dot(T, H));
				return dirAtten * pow(sinTH, exponent);
			}
			
			float3 FinalColor(int index, float4 posWorld, float3 normalDirection, float3 t1, float3 t2, float3 viewDirection, float specMask)
			{
				float4 lightColor;
				float3 vertexToLightSource;
				float3 lightDirection;
				float3 diffuseReflection;
				float3 specularReflection = float3(0.0, 0.0, 0.0);
				float attenuation = 1.0;
	            fixed isPoint = 0.0;
				if(VectorLight[index].w == 1.0)
	    		{	            			
	        		vertexToLightSource = float3(VectorLight[index] - posWorld);	                		
	        		isPoint = 1.0;
	        	}
	        	else
	        	{
	        		lightDirection = VectorLight[index].xyz;
	        	}
	        	lightColor = ColorLight[index];
	        	if(isPoint == 1.0)//PointLight?
                {
	                lightDirection = normalize(vertexToLightSource);
	                float squaredDistance = dot(vertexToLightSource, vertexToLightSource);
	                attenuation = 1.0 / (1.0 + unity_4LightAtten0[index] * squaredDistance);
	            }
	            diffuseReflection = attenuation * float3(lightColor) * float3(_DiffuseColor) * saturate(lerp(0.25, 1.0, dot(normalDirection, lightDirection)));
	            // light source on the wrong side?
	            if (dot(normalDirection, lightDirection) >= 0.0) 	               
	            {		            	
	            	//specular lighting
	                specularReflection = attenuation * float3(lightColor) * float3(_SpecularColor1) * StrandSpecular(t1, viewDirection, lightDirection, _SpecExp1);
					//add 2nd specular term, modulated with noise texture		                																											  
					specularReflection += attenuation * float3(lightColor) * float3(_SpecularColor2) * specMask * StrandSpecular(t2, viewDirection, lightDirection, 
																																 _SpecExp2);
	            }
	            vertexLighting = vertexLighting + diffuseReflection;
	            vertexSpec = vertexSpec + specularReflection;
	            float3 ambientLighting = float3(UNITY_LIGHTMODEL_AMBIENT) * float3(_DiffuseColor);
	            return ambientLighting + vertexSpec + vertexLighting;
	            
			}
			
			fragmentInput vert(vertexInput i)
			{
				fragmentInput o;				
				float4x4 modelMatrix = _Object2World;
				float4x4 modelMatrixInverse = _World2Object; 
	            // unity_Scale.w is unnecessary here 
	            o.posWorld = mul(modelMatrix, i.vertex);
	            o.normalWorld = normalize(float3(mul(float4(i.normal, 0.0), modelMatrixInverse)));
				o.position = mul(UNITY_MATRIX_MVP, i.vertex);							
				o.tangentWorld = normalize(mul(_Object2World, i.tangent).xyz);			
				o.uv = float4( i.uv.xy, 0, 0 );
				return o;
			}
			
			float4 frag(fragmentInput i) : COLOR 
			{
				float4 c;
				ColorLight[0] = _ColorLight1;
				ColorLight[1] = _ColorLight2;
				ColorLight[2] = _ColorLight3;
				ColorLight[3] = _ColorLight4;
				VectorLight[0] = _VectorLight1;
				VectorLight[1] = _VectorLight2;
				VectorLight[2] = _VectorLight3;
				VectorLight[3] = _VectorLight4;
				float3 normalDirection = normalize(i.normalWorld);
				float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - i.posWorld.xzy );
				//shift tangents
				float shiftTex = tex2D(_tSpecShift, i.uv.xy) - 0.5;
				float3 t1 = ShiftTangent(i.tangentWorld, i.normalWorld, _PrimaryShift + shiftTex);
				float3 t2 = ShiftTangent(i.tangentWorld, i.normalWorld, _SecondaryShift + shiftTex);
				float specMask = tex2D(_tSpecMask, i.uv.xy); //approximate sparkles using texture
	            // Diffuse and specular reflection by four "vertex lights"
				vertexLighting = float3(0.0, 0.0, 0.0);
				vertexSpec = float3(0.0, 0.0, 0.0);				
				int index = 0;
				float3 sumaColores = float3(0.0, 0.0, 0.0);
				if(index < _nLights)
				{
					sumaColores = FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
					index++;
					if(index < _nLights)
					{
						sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
						index++;
						if(index < _nLights)
						{
							sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
							index++;
							if(index < _nLights)
							{
								sumaColores += FinalColor(index, i.posWorld, normalDirection, t1, t2, viewDirection, specMask);
								index++;
							}
						}
					}										
				}	            
	            //Final color assembly
	            //VectorLight[0] = _VectorLight1;
	            //c.rgb = VectorLight[0].xyz;
	            c.rgb = sumaColores * tex2D(_tBase, i.uv.xy) * _LightColor;
				//o.rgb = specular;
				//o.rgb = diffuse;
				c.a = tex2D(_tAlpha, i.uv.xy); //read alpha texture
				return c;				
			}

			ENDCG
		}
	}
}