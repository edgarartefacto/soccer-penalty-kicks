﻿Shader "MVP/BumpedDiffuseSpecularMap" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalMap ("Normal Map (RGB)", 2D) = "white" {}
		_SpecularMap ("Specular Map (RGB)", 2D) = "white" {}
		_Gloss ("Gloss",Range(0.01,200)) = 0.05
		_Shininess ("Shiness", Range(0.0,3)) = 0.5
	}
	SubShader {
		Tags {"Queue"="Geometry"}
		LOD 200
		
		CGPROGRAM
		#pragma surface surf SimpleSpecular 

		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _SpecularMap;
		half _Gloss;
		half _Shininess;
		half3 _SpecMap;
		
		half4 LightingSimpleSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			half3 h = normalize (lightDir + viewDir);

			half diff = max (0, dot (s.Normal, lightDir));

			float nh = max (0, dot (s.Normal, h));
			float spec = pow (nh, s.Gloss);

			half4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec * _SpecMap * s.Specular) * (atten * 2);
			c.a = s.Alpha;
			return c;
		}

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Gloss = _Gloss;
			o.Specular = _Shininess;
			o.Normal = UnpackNormal( tex2D(_NormalMap,IN.uv_MainTex));
			_SpecMap = tex2D (_SpecularMap, IN.uv_MainTex).rgb;
		}
		ENDCG 
	}  
	FallBack "Diffuse"
}
