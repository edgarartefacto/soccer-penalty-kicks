﻿Shader "Custom/CustomizableBumpedSpecular" {
 Properties
 {
 _Color ("Main Color", Color) = (1,1,1,1)
 _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
 	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
 _MainTex ("Base (RGB)", 2D) = "white" {}
 _BumpMap ("Normalmap", 2D) = "bump" {}
 //1 means do nothing. 0 means max power, 2 means low power
 _BumpPower ("Bump Power", Range (3, 0.01)) = 1
 _SpecTex ("Spec (RGB)", 2D) = "white" {}
 }
 SubShader {
 	Tags { "RenderType"="Opaque" }
 	LOD 350

CGPROGRAM
#pragma surface surf Lambert

 sampler2D _MainTex;
 sampler2D _BumpMap;
 fixed4 _Color;
 fixed _BumpPower;
 sampler2D _SpecTex;
 float _Shininess;

 struct Input
 {
 float2 uv_MainTex;
 float2 uv_BumpMap;
 };

 void surf (Input IN, inout SurfaceOutput o)
 {
 fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
 half4 spectex = tex2D(_SpecTex, IN.uv_MainTex);
    _SpecColor = spectex;
 o.Albedo = c.rgb;
 o.Specular = _Shininess;
 o.Alpha = c.a;
 fixed3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
 normal.z = normal.z * _BumpPower;
 o.Normal = normalize(normal);
 }

 ENDCG  
 }
 FallBack "Bumped Diffuse"
}
