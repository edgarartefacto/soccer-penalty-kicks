﻿Shader "Custom/Effects/Circle" {
Properties {    
	_Color ("Color", Color) = (1,1,1,1)	 	
    _Angle ("SliderValue", Range(0.0,360.0)) = 40.0
    _MainTex ("Texture", 2D) = "white" { }      
    _Offset ("Float", float) = 0.0
}
SubShader {
	Tags {"Queue" = "Transparent" }
    Pass {
    Blend SrcAlpha OneMinusSrcAlpha     
	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag	
	#include "UnityCG.cginc"	
	#pragma target 3.0
	float4 _Color;
	float _Angle;
	sampler2D _MainTex,_DissolveTex;
	
	struct v2f {
	    float4  pos : SV_POSITION;
	    float2  uv : TEXCOORD0;
	};	
	float4 _MainTex_ST;	
	float _Offset;
	//float DissolveValue;
	v2f vert (appdata_base v){
	    v2f o;
	    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	    return o;
	}	
	half4 frag (v2f i) : COLOR{		
		half4 texcol;		
		texcol = tex2D (_MainTex, i.uv);	
		float x=i.uv.x;	
		float y=i.uv.y;		
		float deltaY = x - 0.5f;
		float deltaX = y - 0.5f;
		float newAngle =(atan2(deltaY, deltaX) * 180.0f / 3.1416)+_Offset;	
		if(newAngle<0.0f)
				newAngle+=360;	
		if(newAngle>_Angle)
			texcol.a=0.0f;						
	    return texcol*_Color;
	}
	ENDCG
    }
}
Fallback "VertexLit"
} 