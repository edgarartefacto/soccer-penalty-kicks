Shader "Custom/Effects/DissolveDiffuse" {
Properties {
	_Color ("Color", Color) = (1, 1, 1, 1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_GlossValue ("GlossValue", Range(0,1)) = 0.01   
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	//_BumpMap ("Normalmap", 2D) = "bump" {}
	_MainTex ("Base (RGB)", 2D) = "white" {}       
    //Dissolve stuff
     _InnerColor ("DissolveInnerColor", Color) = (1,0,0,1)
	 _OuterColor ("DissolveOuterColor", Color) = (0,0,1,1)
	_DissolveOuterLength("DissolveOuterLength",Float)=0.025
  	_DissolveInnerLength("DissolveInnerLength",Float)=0.05    
    _DissolveValue ("DissolveValue", Range(0,1)) = 0.01   
    _DissolveTex ("Dissolve Texture", 2D) = "white" { }
    
    
}
SubShader {
	Tags { "RenderType"="Opaque" }
	Cull off
	LOD 200
	AlphaTest Greater 0.5
	
CGPROGRAM
#pragma surface surf BlinnPhong
float4 _InnerColor,_OuterColor,_Color;
sampler2D _MainTex,_DissolveTex;//,_BumpMap;
float _DissolveValue,_DissolveInnerLength,_DissolveOuterLength,_GlossValue;
half _Shininess;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
};

void surf (Input IN, inout SurfaceOutput o) {

	float4 diffusecol,dissolvecol,finalcol;
	dissolvecol = tex2D (_DissolveTex, IN.uv_MainTex);
	diffusecol = tex2D(_MainTex, IN.uv_MainTex);	
	
	finalcol=diffusecol;
	
	
	if(dissolvecol.r-_DissolveInnerLength<_DissolveValue){
		finalcol=_InnerColor*diffusecol;
	}
	if(dissolvecol.r<_DissolveValue+_DissolveOuterLength){
		finalcol=_OuterColor*diffusecol;
	}
	if(dissolvecol.r<_DissolveValue){
		finalcol.a=0.0;
	}
	
	o.Albedo.rgb = finalcol.rgb *_Color.rgb;
	o.Alpha = finalcol.a*_Color.a;
	o.Gloss =_GlossValue;
	o.Specular = _Shininess;
	//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
ENDCG
}

Fallback "VertexLit"
}



//o.Gloss = skincol.a;
//_SpecColor *= transparencycol.r + 0.3f;
//o.Specular = _Shininess;//*transparencycol.r + 0.1f;

//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));