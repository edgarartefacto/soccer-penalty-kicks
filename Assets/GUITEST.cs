﻿using UnityEngine;
using System.Collections;

public class GUITEST : MonoBehaviour {
	private TextMesh t;

	public GUIStyle characterStyle;

	// Use this for initialization
	void Start () {
		t = GameObject.Find("Email").GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		if (GUI.Button (new Rect (0, 0, 100f, 100f), "A",characterStyle)) {
			t.text+="A";
		}
	}
}
