﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeenTracking : MonoBehaviour 
{
    public const string KEEN_PROJECT_ID = "5c6f0ce8c9e77c0001edd483";
    public const string KEEN_WRITE_KEY = "D77E87F00C461B33C3E6FFF9CC22355BEA0C230C48F142AD41A0E9DDE67356BE4C5F4C626764018F99B214C4B6DF9D5D270EB0F4DACB9EE24C5A8D1F85B01CCE58738B4C21ED62F9435F81F33D24B69C95BBB484FC288EA05FD2A4D92FB7B67C";

    public const string APPLICATION_TRACKING_ID = "mvp.live.penaltykick";
    public const string APP_NAME = "UTC Penalty Kick";
    public const string APP_VERSION = "2.4.1";
    public class TrackedEvent
    {
        // ADD ADDITIONAL TRACKABLE PARAMETERES HERE
        public string appId = APPLICATION_TRACKING_ID;
        public string name;
        public string pcName;
        public string appName = APP_NAME;
        public string appVersion = APP_VERSION;
        public string kicks;
        public string score;
    }

    /// <summary>
    /// GameStartedEvent is a shortcut to tracking game starts.
    /// Autosets the TrackedEvent.name property and still caries
    /// other params we'd like to track.
    /// </summary>
    public class GameStartedEvent : TrackedEvent
	{
		public GameStartedEvent()
		{
			name = "Start";
			// SET REGISTERED BEFORE SENDING
		}
	}
    public class GameOverEvent : TrackedEvent
    {
        public GameOverEvent()
        {
            name = "Game Over";
            // SET REGISTERED BEFORE SENDING
        }
    }


    private Helios.Keen.Client _keenClient;

	private static KeenTracking _instance;
	public static KeenTracking Instance
	{
		get { return _instance; }
	}

	void Awake () 
	{
		_instance = this;
	}

	void Start()
    {
        // This line assigns project settings AND starts
        // client instance's cache service if everything is OK.
		_keenClient = new Helios.Keen.Client();
        _keenClient.Settings = new Helios.Keen.Client.Config
        {
            /* [REQUIRED] Keen.IO project id, Get this from Keen dashboard */
            ProjectId           = KEEN_PROJECT_ID,
            /* [REQUIRED] Keen.IO write key, Get this from Keen dashboard */
            WriteKey            = KEEN_WRITE_KEY,
            /* [OPTIONAL] Attempt to sweep the cache every 2 minutes */
            CacheSweepInterval  = 120.0f,
            /* [OPTIONAL] In every sweep attempt pop 10 cache entries */
            CacheSweepCount     = 10,
            /* [OPTIONAL] This is the callback per Client's event emission */
            EventCallback       = OnKeenClientEvent,
            /* [OPTIONAL] A cache implementation instance. If not provided, cache is turned off */
            CacheInstance       = new Helios.Keen.Cache.Sqlite(Path.Combine(Application.persistentDataPath, "keen.sqlite3"))
        };
    }
	
    void OnKeenClientEvent(Helios.Keen.Client.CallbackData metric_event)
    {
        Debug.Log("Keen event with name " + metric_event.evdata.Name + " and value "+ metric_event.evdata.Data +" ended with status: "+ metric_event.status.ToString());
    }

    public void TrackEvent(TrackedEvent evt)
    {
        // This is an example of using custom data types
        _keenClient.SendEvent(evt.name, evt);
    }
}
