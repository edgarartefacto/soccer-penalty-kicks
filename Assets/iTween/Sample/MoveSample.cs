using UnityEngine;
using System.Collections;

public class MoveSample : MonoBehaviour
{	
	void Start(){
		iTween.MoveBy(gameObject, iTween.Hash("x", 2, "time", 3.0f, "delay", .1));
		iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0.0f, "time", 3.0f));
	}
}

