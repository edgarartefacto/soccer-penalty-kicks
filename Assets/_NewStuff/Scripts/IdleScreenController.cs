﻿using UnityEngine;
using System.Collections;

public class IdleScreenController : MonoBehaviour {

    public float timeToReturn;
    public GameObject loading;
    float startTime;

    public KeenTracking _analytics;

    public void Start()
    {
        startTime = Time.time;
        timeToReturn = XMLConfiguration.GetSetting<float>("ReturnToVideoTime", 10);
    }
	
    public void Update()
    {
        if (Time.time > startTime + timeToReturn)
        {
            Application.LoadLevel("VideoLoop");
        }
    }

    public void StartGame()
    {
        loading.SetActive(true);
        SaveAnalytics();
        Application.LoadLevel("Game");
    }

    private void SaveAnalytics()
    {
        _analytics.TrackEvent(new KeenTracking.GameStartedEvent()
        {
            pcName = System.Environment.MachineName,
            kicks = "0",
            score = "0"
        });

    }

}
