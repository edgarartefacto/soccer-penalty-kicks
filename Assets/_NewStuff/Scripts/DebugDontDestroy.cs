﻿using UnityEngine;
using System.Collections;

public class DebugDontDestroy : MonoBehaviour {

    public static DebugDontDestroy singleton;

	// Use this for initialization
	void Start () {
        if (DebugDontDestroy.singleton == null)
        {
            DebugDontDestroy.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (transform.parent != null)
                Destroy(transform.parent.gameObject);
            else
                Destroy(gameObject);
        }
    }
	
}
