﻿using UnityEngine;
using System.Collections;

public class RunResolution : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        int width = 1080; // or something else
        int height = 1920; // or something else
        bool isFullScreen = true; // should be windowed to run in arbitrary resolution
        int desiredFPS = 60; // or something else

        Screen.SetResolution(width, height, isFullScreen, desiredFPS);
    }

}
