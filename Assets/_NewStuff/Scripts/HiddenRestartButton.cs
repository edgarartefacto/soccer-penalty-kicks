﻿using UnityEngine;
using System.Collections;

public class HiddenRestartButton : MonoBehaviour {

    public GameObject resetButton;

	// Use this for initialization
	void Start () {
        resetButton.SetActive(XMLConfiguration.GetSetting<bool>("ShowResetButton", false));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
