﻿using UnityEngine;
using System.Collections;

public class VideoPlayerMVP : MonoBehaviour {

    MovieTexture movie;

	// Use this for initialization
	void Start () {
        Renderer r = GetComponent<Renderer>();
        movie = (MovieTexture)r.material.mainTexture;
        movie.Play();
        movie.loop = true;
    }
	
    public void OnDestroy()
    {
        movie.Stop();
    }

    public void NextScreen()
    {
        Application.LoadLevel("IdleScreen");
    }

}
