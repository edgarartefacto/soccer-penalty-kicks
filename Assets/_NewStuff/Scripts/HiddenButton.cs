﻿using UnityEngine;
using System.Collections;

public class HiddenButton : MonoBehaviour {

    public static HiddenButton singleton;

	// Use this for initialization
	void Start () {
	    if (HiddenButton.singleton == null)
        {
            HiddenButton.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
	}
	
    public void Quit()
    {
        Application.Quit();
    }
}
