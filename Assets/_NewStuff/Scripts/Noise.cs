﻿using UnityEngine;
using System.Collections;

public class Noise : MonoBehaviour {

    public static Noise singleton;

	// Use this for initialization
	void Start () {
        if (Noise.singleton == null)
        {
            Noise.singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
