﻿#define USE_DICTIONARY
//#define DEBUG_FLOW

using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System;
using System.Linq;

/// <summary>
/// XML configuration. V_1.0.0
/// </summary>
public class XMLConfiguration : MonoBehaviour
{
    public string rootNodeString = "Settings"; //The parent node of the xml file.
    public string fileNameString = "Settings";
    static XMLConfiguration myslf;

    Dictionary<string, string> mySettings;

    static string filepath;
    static XmlDocument xmlDoc;

    void Awake()
    {
        if (myslf == null)
        {
            myslf = this;
            if (transform.parent != null)
                DontDestroyOnLoad(transform.parent.gameObject);
            else
                DontDestroyOnLoad(this.gameObject);

            //string newFile = @"/Resources/Settings.xml";
            string newFile = @"/../" + fileNameString + ".xml";
            filepath = Application.dataPath + newFile;
            xmlDoc = new XmlDocument();


            mySettings = XMLConfiguration.LoadSettings();
        }
        else
        {
            if (transform.parent != null)
                Destroy(transform.parent.gameObject);
            else
                Destroy(this.gameObject);
        }
    }

    public static T GetSetting<T>(string key, T defaultValue)
    {
        if (myslf == null)
        {
            string error = "You need to add XMLConfiguration script to your scene!!!!";
            Debug.LogError(error);
            return defaultValue;
        }
        if (myslf.mySettings.ContainsKey(key))
        {
            return (T)Convert.ChangeType(myslf.mySettings[key], typeof(T));
        }

        string error2 = "That parameter '" + key + "' is missing from the Settings.xml file. Please check your settings.";
        Debug.LogError(error2);
        return defaultValue;
        //return default(T);
    }
    public static T[] GetSettingAsArray<T>(string key, T[] defaultValue)
    {
        if(myslf == null)
        {
            string error = "You need to add XMLConfiguration script to your scene!!!!";
            Debug.LogError(error);
            return defaultValue;
        }
        if (myslf.mySettings.ContainsKey(key))
        {
            //return (T[])Convert.ChangeType(myslf.mySettings[key].Split(','), typeof(T));
            return myslf.mySettings[key].Split(',')
                    .Select(s => (T)Convert.ChangeType(s, typeof(T)))
                    .ToArray();
        }

        string error2 = "That parameter '"+ key + "' is missing from the Settings.xml file. Please check your settings.";
        Debug.LogError(error2);

        return defaultValue;
    }

    public static Dictionary<string, string> GetSettings()
    {
        if (myslf != null)
        {
            if (myslf.mySettings.ContainsKey("IsDebug") && bool.Parse(myslf.mySettings["IsDebug"]))
            {
                myslf.mySettings = LoadSettings();
            }
            return myslf.mySettings;
        }
        else
        {
            Debug.LogError("You need to add XMLConfiguration script to the scene");
            return null;
        }
    }
    public static void GetSettingsAllAgain()
    {
        myslf.mySettings = LoadSettings();
    }
    public static bool IsDataLoaded()
    {
        if (myslf.mySettings != null)
            return true;

        return false;
    }
    
    /// <summary>
    /// Loads the settings. With this function, the settings are stored in a dictionary<string, string>. Is can read single nodes or the attributes for a node.
    /// Currently this read only the next level of the root parent.
    /// </summary>
    /// <returns>The settings.</returns>
    public static Dictionary<string, string> LoadSettings()
    {
        Dictionary<string, string> settingsDictionary = new Dictionary<string, string>();

        if (FileExist(false))
        {
            xmlDoc.Load(filepath);
            XmlNode nodoRaizDoc = xmlDoc.FirstChild;

            if (nodoRaizDoc.HasChildNodes)
            {
                for (int i = 0; i < nodoRaizDoc.ChildNodes.Count; i++)
                {
                    string newNode = nodoRaizDoc.ChildNodes[i].Name;
                    if (newNode != "#comment")
                    {
                        if (nodoRaizDoc.ChildNodes[i].Attributes.Count > 0)
                        {
                            for (int j = 0; j < nodoRaizDoc.ChildNodes[i].Attributes.Count; j++)
                            {
                                string newKey = newNode + "_" + nodoRaizDoc.ChildNodes[i].Attributes[j].Name;


                                if (!settingsDictionary.ContainsKey(newKey))
                                    settingsDictionary.Add(newKey, nodoRaizDoc.ChildNodes[i].Attributes[j].Value);
                                else
                                    Debug.LogError("That key is already on the dictionary=" + newKey);

                            }
                        }
                        //else{
                        if (nodoRaizDoc.ChildNodes[i].InnerText != "")
                        {
                            string newKey = newNode;

                            if (!settingsDictionary.ContainsKey(newKey))
                                settingsDictionary.Add(newKey, nodoRaizDoc.ChildNodes[i].InnerText);
                            else
                                Debug.LogError("That key is already on the dictionary=" + newKey);

                        }
                        //}
                    }
                }
            }
            else
                Debug.LogError("File does not have tags. Add fields to configurate");

        }

        return settingsDictionary;
    }

    public static void SaveSetting(bool overwriteValue, string _parentNode, string _valueToSave, string _attributeName = "")
    {
        if (!overwriteValue)
        {
            if (_attributeName == "")
            {
                //It saves the value to the inner text of the node.

                if (FileExist(true))
                {
                    XmlElement newNodeXML;
                    newNodeXML = xmlDoc.CreateElement(_parentNode);
                    newNodeXML.InnerText = _valueToSave;

                    XmlElement nodoRaizDoc = xmlDoc.DocumentElement;
                    nodoRaizDoc.AppendChild(newNodeXML);
                    xmlDoc.Save(filepath);
                    Debug.LogWarning("Element added to the file correctly");
                }
            }
            else
            {
                //It saves the value to the corresponding attribute.
                XmlElement newNodeXML;
                newNodeXML = xmlDoc.CreateElement(_parentNode);
                newNodeXML.SetAttribute(_attributeName, _valueToSave);

                XmlElement nodoRaizDoc = xmlDoc.DocumentElement;
                nodoRaizDoc.AppendChild(newNodeXML);
                xmlDoc.Save(filepath);
                Debug.LogWarning("Element added to the file correctly");
            }
        }
        else
        {
            //Overwrite the value.
            XmlElement nodoRaizDoc = xmlDoc.DocumentElement;
            XmlNodeList AllElements = nodoRaizDoc.GetElementsByTagName(_parentNode);

            if (AllElements.Count > 0)
            {
                //Only modifies the first settings that it finds. If there are more than 1, means the settings is duplicated

                if (AllElements.Count > 1)
                {
                    Debug.Log("The setting is duplicated");
                }
                else
                {
                    if (_attributeName != "")
                    {
                        bool attributeWasAdded = false;

                        for (int j = 0; j < AllElements[0].Attributes.Count; j++)
                        {
                            if (AllElements[0].Attributes[j].Name == _attributeName)
                            {
                                AllElements[0].Attributes[j].Value = _valueToSave;
                                attributeWasAdded = true;
                            }
                        }

                        if (!attributeWasAdded)
                        {

                            XmlAttribute newAttribute = xmlDoc.CreateAttribute(_attributeName);
                            newAttribute.Value = _valueToSave;
                            AllElements[0].Attributes.Append(newAttribute);
                            attributeWasAdded = true;
                        }
                    }
                    else
                    {
                        AllElements[0].InnerText = _valueToSave;
                    }

                    xmlDoc.Save(filepath);
                }
            }
            else
            {
                SaveSetting(false, _parentNode, _valueToSave, _attributeName);
            }
        }

        GetSettingsAllAgain();
    }

    public static bool FileExist(bool createFile)
    {


        if (File.Exists(filepath))
        {
            return true;
        }
        else
        {
            if (createFile)
            {
                CreateFile();
                return true;
            }

            Debug.LogError("The " + myslf.fileNameString + " file does not exist:" + filepath);
            return false;
        }
    }

    public static void CreateFile()
    {
        XmlElement rootNode = xmlDoc.CreateElement(myslf.rootNodeString);

        //To save the xml file correctly:
        xmlDoc.AppendChild(rootNode);

        xmlDoc.Save(filepath);
    }

    public static void SaveCroppingSettings(Vector4 _leftCropping, Vector4 _rightCropping)
    {
        //Overwrite the value.
        XmlElement nodoRaizDoc = xmlDoc.DocumentElement;
        XmlNode nodeCropping = nodoRaizDoc.SelectSingleNode("ImageCroppedLeft");

        if (nodeCropping != null)
            nodeCropping.InnerText = _leftCropping.x + "," + _leftCropping.y + "," + _leftCropping.z + "," + _leftCropping.w;
        else
            Debug.LogError("Cant find ImageCroppedLeft tag");

        nodeCropping = nodoRaizDoc.SelectSingleNode("ImageCroppedRight");

        if (nodeCropping != null)
            nodeCropping.InnerText = _rightCropping.x + "," + _rightCropping.y + "," + _rightCropping.z + "," + _rightCropping.w;
        else
            Debug.LogError("Cant find ImageCroppedRight tag");

        xmlDoc.Save(filepath);

    }

    public static void SaveSetting(bool overwriteValue, string _parentNode, Vector3 _valueToSave, string _attributeName = "")
    {
        SaveSetting(overwriteValue, _parentNode, _valueToSave.ToString().TrimStart('(').TrimEnd(')'), _attributeName);
    }
}