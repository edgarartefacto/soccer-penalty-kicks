﻿using UnityEngine;
using System.Collections;

public class RandomAnimate : MonoBehaviour {

	const int WAIT = 0;
	const int LOSE = 1;
	const int WIN = 2;

	int ESTADO = 0;
	//AnimationState[] animClip;
	int[] Wait;
	int[] Lose;
	int[] Win;

	// Use this for initialization
	void Start ()
	{
		Wait = new int[]{6,8,11,/*14,*/0,2,5,13};
		Win = new int[]{0,1,3,4,5,9,12,17};
		Lose = new int[]{2,13,/*15,*/6,14,/**/8,11,14};
		Random.seed = (int)System.DateTime.Now.Ticks;
		animation.wrapMode = WrapMode.Loop;
		//Empezamos en espera
		int x = (int)(Random.value*Wait.Length);
		//Debug.Log ("Legacy: " + Wait[x]);
		StartCoroutine(randomAnimate (Wait[x]));
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(AnimatorController2.publicWin)
		{
			//WIN
			int x = (int)(Random.value*Win.Length);
			//Debug.Log ("Win Anim: " + Win[x]);
			StartCoroutine(randomAnimate (Win[x]));
		}
		else if(AnimatorController2.publicLose)
		{
			//LOSE
			int x = (int)(Random.value*Lose.Length);
			//Debug.Log ("Win Anim: " + Lose[x]);
			StartCoroutine(randomAnimate (Lose[x]));
		}			
		//		else
		//		{
		//			//WAIT
		//			int x = (int)(Random.value*Wait.Length);
		//			Debug.Log ("Win Anim: " + Wait[x]);
		//			StartCoroutine(randomAnimate (Wait[x]));
		//		}
	}

	IEnumerator randomAnimate(int i){
		yield return new WaitForSeconds (Random.value*0.5f);
		animation.CrossFade("Anim" + i);
		AnimatorController2.publicWin = false;
		AnimatorController2.publicLose = false;
		//Debug.Log ("Again: " + i + " RandomSeed: " + Random.seed);
	}
}
