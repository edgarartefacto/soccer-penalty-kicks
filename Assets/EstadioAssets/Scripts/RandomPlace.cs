using UnityEngine;
using System.Collections;

public class RandomPlace : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		printRandomPosition (8, 26);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	void printRandomPosition(int filas, int columnas){

		string aux = "";
		int prevInt = 0;

		for (int i = 0; i < filas; i++) {
			Random.seed = (int)System.DateTime.Now.Ticks;
			for (int j = 0; j < columnas; j++) {
				//1-10 hombres
				//11-19 mujeres
				int auxInt = Random.Range(1, 19);
				if (auxInt != prevInt){
					aux += auxInt + " ";
					prevInt = auxInt;
				}
				else {
					j--;
				}
			}
			print(aux + "\n");
			aux = "";
		}
		
	}
}

