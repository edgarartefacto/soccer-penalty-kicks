﻿using UnityEngine;
using System.Collections;

public class EditorScript : MonoBehaviour 
{
	public AnimationClip initial;
	public AnimationClip idle;
	public AnimationClip[] walk;
	public AnimationClip[] enter;
	public AnimationClip[] provocation;
	public AnimationClip[] ataj;
	public int[] atajIzqDerCen;
	public AnimationClip[] walkExit;
	public AnimationClip[] exit;
	public int[] exitIzqDer;
	public int[] isWin;
	public UnityEditorInternal.AnimatorController ac;
	public Row[] test;
}

[System.Serializable]
public class Row	
{	
	public float _offset;
	public float _duration;
	public float _exitTime;
}