﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

public class MyMenu : MonoBehaviour 
{
	// Add a menu item named "Build State Machine" to MyMenu in the menu bar.
	[MenuItem ("MyMenu/Build State Machine")]
	static void BuildStateMachine () 
	{
		if(Selection.gameObjects.Length > 0)
			foreach (GameObject g in Selection.gameObjects) 
			{
				EditorScript ES = g.GetComponent<EditorScript> ();
				if (ES)
				{
					//Declare that we go to use
					AnimatorControllerLayer layer;
					StateMachine m_stateMachine;
					State _State;
					//State2 and State3 used when create transitions
					State _State2;
					State _State3;
					//state used for jump idle to exit
					State _State4;
					Motion _Motion;
					BlendTree _BlendTree;
					Transition tout;
					AnimatorCondition condition;
					AnimatorCondition timeExit;
					//Count the states added (is used to the index)
					int indexState = 0;
					//Add the parameters
					ES.ac.AddParameter ("RandomWalkEnter", AnimatorControllerParameterType.Int);
					ES.ac.AddParameter ("RandomProvocation", AnimatorControllerParameterType.Int);
					ES.ac.AddParameter ("ActiveAtaj", AnimatorControllerParameterType.Int);
					ES.ac.AddParameter ("RandomExitIzDe", AnimatorControllerParameterType.Int);
					ES.ac.AddParameter ("RandomExit", AnimatorControllerParameterType.Int);
					ES.ac.AddParameter ("IsWin", AnimatorControllerParameterType.Int);
					//Get the layer
					layer = ES.ac.GetLayer(0);
					//Get the state machine
					m_stateMachine = layer.stateMachine;
					//Create initial state
					m_stateMachine.AddState ("Initial");
					//Set the clip to the initial state
					_State = m_stateMachine.GetState(indexState);
					//Increase the index of the states
					indexState++;
					_State.SetAnimationClip (ES.initial);
					//Enter
					for (int i = 0; i<ES.enter.Length; i++) 
					{
						//Create walking states
						m_stateMachine.AddState ("Walk" + i);
						//Set the clip to the walking state
						_State2 = m_stateMachine.GetState(indexState);
						//Increase the index of the states
						indexState++;
						_State2.SetAnimationClip (ES.walk[i]);
						//Create enter states
						m_stateMachine.AddState ("Enter" + i);
						//Set the clip to the enter state
						_State3 = m_stateMachine.GetState(indexState);
						//Increase the index of the states
						indexState++;
						_State3.SetAnimationClip (ES.enter[i]);
						//Create the transition
						tout = m_stateMachine.AddTransition(_State, _State2);
						//Create a condition a configure it
						tout.AddCondition();
						condition = tout.GetCondition (1);
						condition.parameter = "RandomWalkEnter";
						condition.mode = TransitionConditionMode.Equals;
						condition.threshold = i + 1;						
						//Create the transition
						tout = m_stateMachine.AddTransition(_State2, _State3);
						tout.offset = ES.test[i]._offset;
						tout.duration = ES.test[i]._duration;
						timeExit = tout.GetCondition (0);
						timeExit.exitTime = ES.test[i]._exitTime;
					}
					//the transitions to the provocation state
					int nTransition = 1;
					//The value for the while of the maximun value of the enter states
					int maxValueIndexEnter = indexState;
					for (int i = 0; i < ES.provocation.Length; i++) 
					{
						//Create provocation states
						m_stateMachine.AddState ("Provocation" + i);
						//Set the clip to the provocation state
						_State2 = m_stateMachine.GetState(indexState);
						//Increase the index of the states
						indexState++;
						_State2.SetAnimationClip (ES.provocation[i]);
						//The first enter state is in the inex 2
						int indexEnter = 2;
						//we know the first enter state has the index 2 and every 2 indexes we have another
						while(indexEnter <= maxValueIndexEnter)
						{
							//Set the clip to the Enter state
							_State = m_stateMachine.GetState(indexEnter);
							//Create the transition
							tout = m_stateMachine.AddTransition(_State, _State2);
							//Create a condition a configure it
							tout.AddCondition();
							condition = tout.GetCondition (1);
							condition.parameter = "RandomProvocation";
							condition.mode = TransitionConditionMode.Equals;
							condition.threshold = nTransition;
							//The next enter sate is 2 indexes next
							indexEnter += 2;
						}
						//Increase the value to activate this transition
						nTransition++;
					}
					//Create idle state
					m_stateMachine.AddState ("Idle");
					//Set the clip to the idle state
					_State = m_stateMachine.GetState(indexState);
					//Increase the index of the states
					indexState++;
					_State.SetAnimationClip (ES.idle);
					//Transitions Provoc-Idle
					for(int i = 0; i < ES.provocation.Length; i++)
					{
						//Get the state of the provocations tha start at maxValuendexEnter
						_State2 = m_stateMachine.GetState(maxValueIndexEnter);
						//Create the transition
						tout = m_stateMachine.AddTransition(_State2, _State);
						maxValueIndexEnter++;
					}
					//Ataj
					for (int i = 0; i < ES.ataj.Length; i++) 
					{
						//Create ataj states
						m_stateMachine.AddState ("Ataj" + i);

						//Set the clip to the ataj state
						_State2 = m_stateMachine.GetState(indexState);
//						_BlendTree = _State2.CreateBlendTree();
						//Increase the index of the states
						indexState++;
//						_BlendTree.AddAnimationClip(ES.ataj[i]);
//						_BlendTree.AddAnimationClip(ES.ataj[i]);
//						_Motion = _BlendTree.GetMotion(0);	
//					_Motion
						_State2.SetAnimationClip (ES.ataj[i]);
						//Create the transition
						tout = m_stateMachine.AddTransition(_State, _State2);
						//Remove the default condition
						tout.RemoveCondition(0);
						//Create a condition a configure it
						tout.AddCondition();
						condition = tout.GetCondition (0);
						condition.parameter = "ActiveAtaj";
						condition.mode = TransitionConditionMode.Equals;
						condition.threshold = i + 1;
					}
					//maxValueIndexEnter has the idle state index at this moment, add 1 to have the first ataja index
					maxValueIndexEnter++;
					int minValueIndexAtaj = maxValueIndexEnter;
					//The value for the while of the maximun value of the enter states
					int maxValueIndexAtaj = indexState-1;
					//Variables to the random exit in LeftWin, LeftLose, RightWin and RightLose
					int rW = 0;
					int rL = 0;
					int lW = 0;
					int lL = 0;
					//Exit
					//Save idle state
					_State4 = _State;
					for (int i = 0; i<ES.exit.Length; i++) 
					{
						if(ES.exitIzqDer[i] == 1 && ES.isWin[i] == 1)
						{
							rW++;
						}
						else if (ES.exitIzqDer[i] == 1 && ES.isWin[i] == 0)
						{
							rL++;
						}
						else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 1)
						{
							lW++;
						}
						else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 0)
						{
							lL++;
						}
						//Create exit states
						m_stateMachine.AddState ("Exit" + i);
						//Set the clip to the exit state
						_State2 = m_stateMachine.GetState(indexState);
						//Increase the index of the states
						indexState++;
						_State2.SetAnimationClip (ES.exit[i]);
						//Create walk exit states
						m_stateMachine.AddState ("WalkExit" + i);
						//Set the clip to the walk exit state
						_State3 = m_stateMachine.GetState(indexState);
						//Increase the index of the states
						indexState++;
						_State3.SetAnimationClip (ES.walkExit[i]);
						int x = 0;		
						//transition from idle to exit						
						tout = m_stateMachine.AddTransition(_State4, _State2);
						//Remove the default condition
						tout.RemoveCondition(0);
						//Create a condition a configure it
						tout.AddCondition();
						condition = tout.GetCondition (0);
						condition.parameter = "ActiveAtaj";
						condition.mode = TransitionConditionMode.Equals;
						condition.threshold = -1;
						//Create a condition and configure it
						tout.AddCondition();
						condition = tout.GetCondition (1);
						condition.parameter = "RandomExitIzDe";
						condition.mode = TransitionConditionMode.Equals;
						if(ES.exitIzqDer[i] == 1)
							condition.threshold = 1;
						else
							condition.threshold = 2;
						//Create a condition and configure it
						tout.AddCondition();
						condition = tout.GetCondition (2);
						condition.parameter = "IsWin";
						condition.mode = TransitionConditionMode.Equals;
						if(ES.isWin[i] == 1)
							condition.threshold = 1;
						else if (ES.isWin[i] == 0)
							condition.threshold = 0;
						//Create a condition for RandomExit and configure it
						tout.AddCondition();
						condition = tout.GetCondition (3);
						condition.parameter = "RandomExit";
						condition.mode = TransitionConditionMode.Equals;
						if(ES.exitIzqDer[i] == 1 && ES.isWin[i] == 1)
						{
							condition.threshold = rW;
						}
						else if (ES.exitIzqDer[i] == 1 && ES.isWin[i] == 0)
						{
							condition.threshold = rL;
						}
						else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 1)
						{
							condition.threshold = lW;
						}
						else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 0)
						{
							condition.threshold = lL;
						}
						//Create the transitions
						while(minValueIndexAtaj <= maxValueIndexAtaj)
						{
							//Set the clip to the ataj state
							_State = m_stateMachine.GetState(minValueIndexAtaj);
							//Create the transition
							if(ES.atajIzqDerCen[x] == ES.exitIzqDer[i])
							{
								tout = m_stateMachine.AddTransition(_State, _State2);
								//Create a condition for IsWin (1 = true, 0 = false) and configure it
								tout.AddCondition();
								condition = tout.GetCondition (1);
								condition.parameter = "IsWin";
								condition.mode = TransitionConditionMode.Equals;
								if(ES.isWin[i] == 1)
									condition.threshold = 1;
								else if (ES.isWin[i] == 0)
									condition.threshold = 0;
								//Create a condition for RandomExit and configure it
								tout.AddCondition();
								condition = tout.GetCondition (2);
								condition.parameter = "RandomExit";
								condition.mode = TransitionConditionMode.Equals;
								if(ES.exitIzqDer[i] == 1 && ES.isWin[i] == 1)
								{
									condition.threshold = rW;
								}
								else if (ES.exitIzqDer[i] == 1 && ES.isWin[i] == 0)
								{
									condition.threshold = rL;
								}
								else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 1)
								{
									condition.threshold = lW;
								}
								else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 0)
								{
									condition.threshold = lL;
								}
							}
							else if(ES.atajIzqDerCen[x] == 2)
							{								
								tout = m_stateMachine.AddTransition(_State, _State2);
								//Create a condition and configure it
								tout.AddCondition();
								condition = tout.GetCondition (1);
								condition.parameter = "RandomExitIzDe";
								condition.mode = TransitionConditionMode.Equals;
								if(ES.exitIzqDer[i] == 1)
									condition.threshold = 1;
								else
									condition.threshold = 2;
								//Create a condition and configure it
								tout.AddCondition();
								condition = tout.GetCondition (2);
								condition.parameter = "IsWin";
								condition.mode = TransitionConditionMode.Equals;
								if(ES.isWin[i] == 1)
									condition.threshold = 1;
								else if (ES.isWin[i] == 0)
									condition.threshold = 0;
								//Create a condition for RandomExit and configure it
								tout.AddCondition();
								condition = tout.GetCondition (3);
								condition.parameter = "RandomExit";
								condition.mode = TransitionConditionMode.Equals;
								if(ES.exitIzqDer[i] == 1 && ES.isWin[i] == 1)
								{
									condition.threshold = rW;
								}
								else if (ES.exitIzqDer[i] == 1 && ES.isWin[i] == 0)
								{
									condition.threshold = rL;
								}
								else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 1)
								{
									condition.threshold = lW;
								}
								else if (ES.exitIzqDer[i] == 3 && ES.isWin[i] == 0)
								{
									condition.threshold = lL;
								}
							}						
							//The next exit sate is 1 index next
							minValueIndexAtaj++;
							x++;
						}
						tout = m_stateMachine.AddTransition(_State2, _State3);
						minValueIndexAtaj = maxValueIndexEnter;
					}					
				}
			}
		else
			print ("Select the object with the EditorScript.");
	}

	[MenuItem ("MyMenu/Delete State Machine")]
	static void DeleteStateMachine () 
	{
		if(Selection.gameObjects.Length > 0)
			foreach (GameObject g in Selection.gameObjects) 
			{
				EditorScript ES = g.GetComponent<EditorScript> ();
				if (ES)
				{
					//Declare that we go to use
					AnimatorControllerLayer layer;
					StateMachine m_stateMachine;
					//Get the layer
					layer = ES.ac.GetLayer(0);
					//Get the state machine
					m_stateMachine = layer.stateMachine;
					//Remove states
					State stateToRemove;
					for(int i = m_stateMachine.stateCount-1; i >= 0; i--)
					{
						stateToRemove = m_stateMachine.GetState(i);
						m_stateMachine.RemoveState(stateToRemove);
					}
					//Remove the parameters
					for(int i = ES.ac.parameterCount-1; i >= 0; i--)
					{
						ES.ac.RemoveParameter(i);
					}
				}
			}
		else
			print ("Select the object with the EditorScript.");
	}

	[MenuItem ("MyMenu/View Transition Data")]
	static void ViewTransitionData () 
	{
		if(Selection.gameObjects.Length > 0)
			foreach (GameObject g in Selection.gameObjects) 
			{
				EditorScript ES = g.GetComponent<EditorScript> ();
				if (ES)
				{
					//Declare that we go to use
					AnimatorControllerLayer layer;
					StateMachine m_stateMachine;
					//Get the layer
					layer = ES.ac.GetLayer(0);
					//Get the state machine
					m_stateMachine = layer.stateMachine;
					//Remove states
					State state1;
					State state2;
					state1 = m_stateMachine.GetState(37);
					state2 = m_stateMachine.GetState(38);
					AnimatorCondition condition;
					foreach(Transition tout in m_stateMachine.GetTransitionsFromState(state1))
					{
						condition = tout.GetCondition (0);
						print("offset: " + tout.offset + ", duration: " + tout.duration + ", timeExit: " + condition.exitTime);
					}
				}
			}
		else
			print ("Select the object with the EditorScript.");
	}
}
